package it.polimi.provaFinale2013.communication.marshaling;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParcel;
import it.polimi.provaFinale2013.communication.marshalling.parcel.ISymmetricParceler;
import it.polimi.provaFinale2013.utils.parcel.ParcelUtils;

import java.io.IOException;

import junit.framework.Assert;

public class ParcelTestObject {
	public static final ParcelTestObjectFactory CREATOR = new ParcelTestObjectFactory();
	public int a;
	public long b;
	public short c;
	public boolean d;
	public String e;
	public char f;
	public ParcelTestObject g;

	public boolean equals(Object obj) {
		if(obj instanceof ParcelTestObject) {
			ParcelTestObject oc = (ParcelTestObject) obj;
			return this.a == oc.a &&
					this.b == oc.b &&
					this.c == oc.c &&
					this.d == oc.d &&
					(this.e == oc.e || (this.e != null && oc.e != null && this.e.equals(oc.e) )) &&
					this.f == oc.f &&
					(this.g == oc.g || (this.g != null && oc.g != null && this.g.equals(oc.g) ));
		}
		else
			return false;
	}

	public static class ParcelTestObjectFactory implements ISymmetricParceler<ParcelTestObject> {

		public ParcelTestObjectFactory() {
			ParcelUtils.registerParceller(this);
		}

		@Override
		public void writeToParcel(ParcelTestObject contenuto, IParcel pacchetto) {
			try {
				pacchetto.writeInt(contenuto.a);
				pacchetto.writeLong(contenuto.b);
				pacchetto.writeShort(contenuto.c);
				pacchetto.writeBoolean(contenuto.d);
				pacchetto.writeUTF(contenuto.e);
				pacchetto.writeChar(contenuto.f);
				pacchetto.writeObject(contenuto.g, this);
			} catch (IOException e) {
				Assert.fail("IO exception");
			}
		}

		@Override
		public ParcelTestObject createFromParcel(IParcel pacchetto) {
			try {
				ParcelTestObject o = new ParcelTestObject();
				o.a = pacchetto.readInt();
				o.b = pacchetto.readLong();
				o.c = pacchetto.readShort();
				o.d = pacchetto.readBoolean();
				o.e = pacchetto.readUTF();
				o.f = pacchetto.readChar();
				o.g = pacchetto.readObject(this);
				return o;
			} catch (IOException e) {
				Assert.fail("IO exception");
				throw new IllegalStateException();
			}
		}

		@Override
		public String getUniqueClassID() {
			return "OggettoCasuale";
		}
		
	}
}
