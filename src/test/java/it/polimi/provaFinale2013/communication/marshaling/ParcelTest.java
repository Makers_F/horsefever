package it.polimi.provaFinale2013.communication.marshaling;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParcel;
import it.polimi.provaFinale2013.communication.marshalling.parcel.Parcel;
import it.polimi.provaFinale2013.communication.marshalling.parcel.ParcelFactory;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.utils.Log.DebugLevel;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ParcelTest {

	int randomNumber = 0x0000;
	ParcelTestObject pt1;
	ParcelTestObject pt2;

	@Before
	public void setupObject() {
		Log.setLogOptions(DebugLevel.LOG_VERBOSE, true);
		pt1 = new ParcelTestObject();
		pt2 = new ParcelTestObject();

		pt1.a = 6678595;
		pt1.b = 202034820375L;
		pt1.c = 6790;
		pt1.d = true;
		pt1.e = null;
		pt1.f = 'z';
		pt1.g = pt2;

		pt2.a = 9998881;
		pt2.b = 302304020304030L;
		pt2.c = 2234;
		pt2.d = false;
		pt2.e = "Links 2";
		pt2.f = 'f';
		pt2.g = null;
	}

	@Test(expected = Parcel.WrongParcelReadAccess.class)
	public void testWrongReading() throws IOException {
		IParcel parcerl = ParcelFactory.getParcel();
		parcerl.writeInt(randomNumber);
		parcerl.writeObject(pt1, ParcelTestObject.CREATOR);
		parcerl.readBoolean();
	}

	@Test(expected = Parcel.WrongParcelWriteAccess.class)
	public void testWrongWriting() throws IOException {
		IParcel p1 = ParcelFactory.getParcel();
		p1.writeInt(randomNumber);
		p1.writeObject(pt1, ParcelTestObject.CREATOR);

		p1.apriParcel();
		p1.writeBoolean(false);
	}

	@Test
	public void nullArryTest() {
		try {
			IParcel p1 = ParcelFactory.getParcel();
			p1.writeArray(null, ParcelTestObject.CREATOR);
			p1.apriParcel();
			ParcelTestObject[] obj = p1.readArray(ParcelTestObject.class, ParcelTestObject.CREATOR);
			Assert.assertNull("The objects are equal", obj);
		} catch (IOException e) {
			Assert.fail("IOException : " + e.getMessage());
		}
	}

	@Test
	public void testCorrectDataPassed() {
		try {
			IParcel p1 = ParcelFactory.getParcel();
			p1.writeObject(pt1, ParcelTestObject.CREATOR);
			p1.apriParcel();
			ParcelTestObject obj = p1.readObject(ParcelTestObject.CREATOR);
			Assert.assertEquals("The objects are equal", pt1, obj);
		} catch (IOException e) {
			Assert.fail("IOException : " + e.getMessage());
		}
	}

	@Test
	public void testArrayCorrectness() {
		try {
			ParcelTestObject array[] = {pt1,pt2,null,pt2,pt1};
			IParcel p1 = ParcelFactory.getParcel();
			p1.writeArray(array, ParcelTestObject.CREATOR);
	
			p1.apriParcel();
			ParcelTestObject newArray[] = p1.readArray(ParcelTestObject.class, ParcelTestObject.CREATOR);
			boolean bool = true;
			for(int i = 0; i< array.length; i++) {
				if(array[i] != null)
					bool &= array[i].equals(newArray[i]);
				else
					bool &= (newArray[i] == null);
			}
			Assert.assertTrue(bool);
		} catch (IOException e) {
			Assert.fail("IOException : " + e.getMessage());
		}
	}

	@Test(expected = RuntimeException.class)
	public void testCirculareReferenceDetected() {
		try {
			IParcel p3 = ParcelFactory.getParcel();
			pt2.g = pt1;
			p3.writeObject(pt1, ParcelTestObject.CREATOR);
		} catch (IOException e) {
			Assert.fail("IOException : " + e.getMessage());
		}
	}

	@Test
	public void testCirculareReferenceHandling() {
		try {
			IParcel p1 = ParcelFactory.getParcel();
			pt2.g = pt1;
			try {
			p1.writeObject(pt1, ParcelTestObject.CREATOR);
			} catch (RuntimeException e) {
				
			}

			p1.apriParcel();
			ParcelTestObject read = p1.readObject(ParcelTestObject.CREATOR);
			pt2.g = null;
			Assert.assertEquals("The circular reference was handled correctly.", pt1, read);
		} catch (IOException e) {
			Assert.fail("IOException : " + e.getMessage());
		}
	}
}
