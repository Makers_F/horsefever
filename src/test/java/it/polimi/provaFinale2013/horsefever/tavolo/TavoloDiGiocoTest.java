package it.polimi.provaFinale2013.horsefever.tavolo;

import static org.junit.Assert.assertTrue;
import junit.framework.Assert;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.AlreadyRegisteredPlayerNameException;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.NotEnoughPlayerSlotsException;

import org.junit.Test;

public class TavoloDiGiocoTest {

	@Test
	public void testRegisterPlayer() {
		TavoloDiGioco mTavoloDiGioco = new TavoloDiGioco();
		try {
			mTavoloDiGioco.registerPlayer("Alpha");
			mTavoloDiGioco.registerPlayer("Bravo");
			mTavoloDiGioco.registerPlayer("Charlie");
			mTavoloDiGioco.registerPlayer("Alpha");
			Assert.fail();
		} catch (AlreadyRegisteredPlayerNameException e) {
			Assert.assertTrue(true);
		} catch (NotEnoughPlayerSlotsException e) {
			Assert.fail();
		}
		try {
			mTavoloDiGioco.registerPlayer("Delta");
			mTavoloDiGioco.registerPlayer("Echo");
			mTavoloDiGioco.registerPlayer("Foxtrot");
			mTavoloDiGioco.registerPlayer("Golf");
			Assert.fail();
		} catch (AlreadyRegisteredPlayerNameException e) {
			Assert.fail();
		} catch (NotEnoughPlayerSlotsException e) {
			Assert.assertTrue(true);
		}
		assertTrue(mTavoloDiGioco.getNumberOfPlayers() == 6);
	}
}
