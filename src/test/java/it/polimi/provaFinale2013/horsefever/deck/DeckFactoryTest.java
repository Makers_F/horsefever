package it.polimi.provaFinale2013.horsefever.deck;

import static org.junit.Assert.*;
import it.polimi.provaFinale2013.horsefever.carte.DeckFactory;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Test;

public class DeckFactoryTest {

	@Test
	public void testGetMovementCardsDeck() {
		try {
			assertTrue("Il file XML e' letto correttamente", DeckFactory.class.getClassLoader().getResourceAsStream("xml/carteMovimento.xml").read() != -1);
			DeckFactory.getMovementCardsDeck(false);
			DeckFactory.getMovementCardsDeck(true);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void testGetActionCardsDeck() {
		try {
			assertTrue("Il file XML e' letto correttamente", DeckFactory.class.getClassLoader().getResourceAsStream("xml/carteAzione.xml").read() != -1);
			DeckFactory.getActionCardsDeck(false);
			DeckFactory.getActionCardsDeck(true);
		} catch (IOException e) {
			Assert.fail();
		}
	}
}
