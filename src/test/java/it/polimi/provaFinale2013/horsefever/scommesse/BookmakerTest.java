package it.polimi.provaFinale2013.horsefever.scommesse;

import static org.junit.Assert.assertTrue;

import java.util.Random;

import junit.framework.Assert;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.giocatore.Player;
import it.polimi.provaFinale2013.horsefever.giocatore.PlayerCard;
import it.polimi.provaFinale2013.horsefever.scommesse.Bookmaker.AlreadyPresentBetException;

import org.junit.Before;
import org.junit.Test;

public class BookmakerTest {

	private final static String[] mPlayersNames = {"Ann","Bob","Carl","Doug","Earl","Frank"};

	private Bookmaker mBookmaker = new Bookmaker();
	private Quotations mQuotations = new Quotations();
	private Player mPlayer;
	private PlayerCard mPlayerCard;
	private Horse mHorse;
	private KindOfBet mKindOfBet;

	@Before
	public void beforeExecution() {
		Random rnd = new Random();
		int cardIndex = rnd.nextInt(mPlayersNames.length);
		mPlayerCard = PlayerCard.getPlayerCards(mQuotations)[cardIndex];

		int playerNameIndex = rnd.nextInt(mPlayersNames.length);
		mPlayer = new Player(mPlayersNames[playerNameIndex], mPlayerCard);

		int horseIndex = rnd.nextInt(Horse.values().length);
		mHorse = Horse.values()[horseIndex];

		mKindOfBet = rnd.nextBoolean() ? KindOfBet.PLACED : KindOfBet.WINNER;
	}

	@Test
	public void testRegisterBet() {
		try {
			mBookmaker.registerBet(mPlayer, mHorse, 1000, mKindOfBet);
		} catch (AlreadyPresentBetException e) {
			Assert.fail();
		}
		assertTrue(mBookmaker.isBetPending());
	}

	@Test
	public void testGiveMoneyAndPv() {
		try {
			mBookmaker.registerBet(mPlayer, mHorse, 1000, mKindOfBet);
		} catch (AlreadyPresentBetException e) {
			Assert.fail();
		}
		mBookmaker.giveMoneyAndPv(mPlayer);
		assertTrue(!mBookmaker.isPaymentPending());
	}
}