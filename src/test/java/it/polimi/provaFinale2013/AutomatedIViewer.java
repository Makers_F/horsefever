package it.polimi.provaFinale2013;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.horsefever.corse.Races;
import it.polimi.provaFinale2013.horsefever.scommesse.Bookmaker.AlreadyPresentBetException;
import it.polimi.provaFinale2013.horsefever.scommesse.KindOfBet;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.AlreadyRegisteredPlayerNameException;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.NotEnoughPlayerSlotsException;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.PlayerDoNotOwnSpecifiedCardException;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.view.IViewer;

public class AutomatedIViewer implements IViewer {

	private static final int PRIMO = 0;
	private static final int SECONDO = PRIMO + 1;
	private static final int TERZO = SECONDO + 1;

	private static final String A_player = "Alpha";
	private static final String B_player = "Bravo";
	private static final String C_player = "Charlie";

	private final ITavoloDiGioco<IViewer> mTavoloDiGioco;
	private List<Horse> mHorseArrived = new LinkedList<Horse>();
	private int mTurn;
	private int mNumberOfTimesBetCalled;
	private int mNumberOfTimesCardPlacedCalled;
	private boolean mFirstTimeFullQuotationCalled = true;
	private int mRightBets;
	private int mRightStables;

	public AutomatedIViewer(ITavoloDiGioco<IViewer> pTavolo) {
		mTavoloDiGioco = pTavolo;
	}

	@Override
	public void onSingleQuotationChanged(Stable pStable, int pOldQuotation,
			int pNewQuotation) {
		if(mTurn == PRIMO) {
			Assert.assertEquals(pStable, Stable.STABLE_WHITE);
			Assert.assertTrue(pOldQuotation == 6 && pNewQuotation == 7);
		} else if (mTurn == SECONDO) {
			Assert.assertEquals(pStable, Stable.STABLE_GREEN);
			Assert.assertTrue(pOldQuotation == 6 && pNewQuotation == 4);
		} else if (mTurn == TERZO) {
			Assert.fail("No quotation shoul change");
		}
	}

	@Override
	public void onFullQuotationUpdate(Map<Stable, Integer> pHorseToQuotationMap) {
		if(mFirstTimeFullQuotationCalled) {
			mFirstTimeFullQuotationCalled = false;
			return;
		}

		if(mTurn == PRIMO) {
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_YELLOW) == 3);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_BLUE) == 4);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_BLACK) == 4);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_WHITE) == 6);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_GREEN) == 6);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_RED) == 6);
		} else if(mTurn == SECONDO) {
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_YELLOW) == 4);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_BLUE) == 5);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_BLACK) == 5);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_WHITE) == 5);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_GREEN) == 4);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_RED) == 5);
		} else if (mTurn == TERZO) {
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_YELLOW) == 3);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_BLACK) == 4);
			Assert.assertTrue(pHorseToQuotationMap.get(Stable.STABLE_GREEN) == 3);
		}
	}

	@Override
	public void onRaceStarted() {}

	@Override
	public void onMovementCardRevealed(int[] pMovementCardValues) {
		Log.user("Movement: " + Arrays.toString(pMovementCardValues));
	}

	@Override
	public void onSprint(Horse pHorse, int pSprintNumberInThisTurn) {}

	@Override
	public void onHorsePositionsChanged(Map<Horse, Integer> pHorseToPositionMap) {
		for (Map.Entry<Horse, Integer> horsePosition : pHorseToPositionMap.entrySet()) {
			Horse horse = horsePosition.getKey();
			int position = horsePosition.getValue();
			if (!mHorseArrived.contains(horse)) {
				StringBuilder asterisks = new StringBuilder();
				// correcting the offset in order to have all the asterisisks start at the same location
				for(int i = horse.toString().length(); i < Horse.HORSE_YELLOW.toString().length(); i++) {
					asterisks.append(" ");
				}
				for (int i = 0; i < position && i < Races.RACE_LENGHT; i++) {
					asterisks.append("*");
				}
				// adding an indicator to shows how much to go
				for(int i = position; i < Races.RACE_LENGHT; i++) {
					asterisks.append("+");
				}
				asterisks.append("|");
				for (int i = Races.RACE_LENGHT; i < position; i++) {
					asterisks.append("*");
				}
				Log.user(horse + " " + asterisks);
			}
		}
	}

	@Override
	public void onHorseArrived(Horse pHorse, int pHorsePosition) {
		String position = null;
		switch (pHorsePosition) {
			case 1: position = "1st";
					break;
			case 2: position = "2nd";
					break;
			case 3: position = "3rd";
					break;
			default: position = String.valueOf(pHorsePosition).concat("th");
					break;
		}
		Log.user(pHorse + " has arrived " + position);
		mHorseArrived.add(pHorse);
	}

	@Override
	public void onRaceFinished(Horse[] pArrivalOrderedHorses) {
		mHorseArrived.clear();
		if(mTurn == PRIMO) {
			Horse[] expectedArrival = new Horse[] { Horse.HORSE_RED, Horse.HORSE_YELLOW, Horse.HORSE_BLACK, Horse.HORSE_WHITE, Horse.HORSE_BLUE, Horse.HORSE_GREEN};
			Assert.assertTrue("They shoudl arrive like this",Arrays.equals(pArrivalOrderedHorses, expectedArrival));
		} else if (mTurn == SECONDO) {
			Horse[] expectedArrival = new Horse[] { Horse.HORSE_WHITE, Horse.HORSE_RED, Horse.HORSE_GREEN, Horse.HORSE_BLACK, Horse.HORSE_BLUE, Horse.HORSE_YELLOW};
			Assert.assertTrue("They shoudl arrive like this",Arrays.equals(pArrivalOrderedHorses, expectedArrival));
		} else if (mTurn == TERZO) {
			Assert.assertTrue(pArrivalOrderedHorses[0] == Horse.HORSE_YELLOW);
			Assert.assertTrue(pArrivalOrderedHorses[1] == Horse.HORSE_GREEN);
			//Assert.assertTrue(pArrivalOrderedHorses[2] == Horse.HORSE_BLUE);
			Assert.assertTrue(pArrivalOrderedHorses[2] == Horse.HORSE_BLACK);
			//Assert.assertTrue((pArrivalOrderedHorses[4] == Horse.HORSE_WHITE) || (pArrivalOrderedHorses[5] == Horse.HORSE_WHITE));
			//Assert.assertTrue((pArrivalOrderedHorses[4] == Horse.HORSE_RED) || (pArrivalOrderedHorses[5] == Horse.HORSE_RED));
		}
	}

	@Override
	public void onPlacedBet(String pPlayerName, Horse pHorse, int pValue, KindOfBet pKindOfBet) {}

	@Override
	public void onWonBet(String pPlayerName, int pAmount, int pPVs,
			KindOfBet pKindOfBet) {
		if(mTurn == PRIMO) {
			if(pPlayerName.equals(A_player)) {
				Assert.assertTrue(pAmount == 1200 && pPVs == 1 && pKindOfBet == KindOfBet.PLACED);
				mRightBets++;
			} else if(pPlayerName.equals(B_player)) {
				Assert.assertTrue(pAmount == 1000 && pPVs == 1 && pKindOfBet == KindOfBet.PLACED);
				mRightBets++;
			} else if(pPlayerName.equals(C_player)) {
				Assert.assertTrue(pAmount == 700 && pPVs == 3 && pKindOfBet == KindOfBet.WINNER);
				mRightBets++;
			}
		} else if (mTurn == SECONDO) {
			Assert.fail("No win bet");
		} else if (mTurn == TERZO) {
			if(pPlayerName.equals(A_player)) {
				Assert.assertTrue(pAmount == 2800 && pPVs == 3 && pKindOfBet == KindOfBet.WINNER);
				mRightBets++;
			} else if(pPlayerName.equals(B_player)) {
				Assert.assertTrue(pAmount == 1000 && pPVs == 1 && pKindOfBet == KindOfBet.PLACED);
				mRightBets++;
			}
		}
	}

	@Override
	public void onStablePaid(Stable pStable, int pAmount) {
		if(mTurn == PRIMO) {
			if(pStable == Stable.STABLE_YELLOW) {
				Assert.assertTrue(pAmount == 400);
				mRightStables++;
			} else if(pStable == Stable.STABLE_BLACK) {
				Assert.assertTrue(pAmount == 200);
				mRightStables++;
			}
		} else if (mTurn == SECONDO) {
			Assert.fail("No payment");
		} else if (mTurn == TERZO ) {
			if(pStable == Stable.STABLE_YELLOW) {
				Assert.assertTrue(pAmount == 600);
				mRightStables++;
			}
			if(pStable == Stable.STABLE_BLACK) {
				Assert.assertTrue(pAmount == 200);
				mRightStables++;
			}
		}
	}

	@Override
	public void onActionCardGivenToPlayer(String pPlayerName, String pCardName) {}

	@Override
	public void onActionCardAssignedToHorse(String pPlayerName, String pCardName, Horse pHorse) {}

	@Override
	public boolean onExpectBet(String pPlayerName, int pMinBet, int pMaxBet,
			Map<Horse, Integer> pHorseToQuotationMap, boolean isMandatory) {
		if(mTurn == PRIMO) {
			try {
				if(isMandatory) {
					if(mNumberOfTimesBetCalled == 0) {
						mTavoloDiGioco.registerBet(A_player, Horse.HORSE_YELLOW, 400, KindOfBet.WINNER);
						mNumberOfTimesBetCalled++;
						return true;
					} else if(mNumberOfTimesBetCalled == 1) {
						mTavoloDiGioco.registerBet(B_player, Horse.HORSE_BLACK, 500, KindOfBet.PLACED);
						mNumberOfTimesBetCalled++;
						return true;
					} else {
						mTavoloDiGioco.registerBet(C_player, Horse.HORSE_RED, 100, KindOfBet.WINNER);
						mNumberOfTimesBetCalled = 0;
						return true;
					}
				} else {
					if(mNumberOfTimesBetCalled == 0) {
						mNumberOfTimesBetCalled++;
						return false;
					} else if(mNumberOfTimesBetCalled == 1) {
						mNumberOfTimesBetCalled++;
						return false;
					} else {
						mNumberOfTimesBetCalled = 0;
						mTavoloDiGioco.registerBet(A_player, Horse.HORSE_YELLOW, 600, KindOfBet.PLACED);
						return true;
					}
				}
			} catch (AlreadyPresentBetException e) {
				Assert.fail("Should not raise alredy present exception. " + e.getMessage());
				return false;
			}
		} else if(mTurn == SECONDO) {
			try {
				if(isMandatory) {
					if(mNumberOfTimesBetCalled == 0) {
						mTavoloDiGioco.registerBet(B_player, Horse.HORSE_BLUE, 3000, KindOfBet.PLACED);
						mNumberOfTimesBetCalled++;
						return true;
					} else if(mNumberOfTimesBetCalled == 1) {
						mTavoloDiGioco.registerBet(C_player, Horse.HORSE_BLACK, 4300, KindOfBet.PLACED);
						mNumberOfTimesBetCalled++;
						return true;
					} else {
						mTavoloDiGioco.registerBet(A_player, Horse.HORSE_YELLOW, 1000, KindOfBet.WINNER);
						mNumberOfTimesBetCalled = 0;
						return true;
					}
				} else {
					mNumberOfTimesBetCalled = 0;
					return false;
				}
			} catch (AlreadyPresentBetException e) {
				Assert.fail("Should not raise alredy present exception. " + e.getMessage());
				return false;
			}
		} else if(mTurn == TERZO) {
			if(isMandatory) {
				if(mNumberOfTimesBetCalled == 0) {
					try {
						mTavoloDiGioco.registerBet(A_player, Horse.HORSE_YELLOW, 700, KindOfBet.WINNER);
						mNumberOfTimesBetCalled++;
						return true;
					} catch (AlreadyPresentBetException e) {
						Assert.fail("Should not raise alredy present exception. " + e.getMessage());
						return false;
					}
				} else {
					try {
						mTavoloDiGioco.registerBet(B_player, Horse.HORSE_GREEN, 500, KindOfBet.PLACED);
						mNumberOfTimesBetCalled++;
						return true;
					} catch (AlreadyPresentBetException e) {
						Assert.fail("Should not raise alredy present exception. " + e.getMessage());
						return false;
					}
				}
			} else {
				if(mNumberOfTimesBetCalled == 0) {
					try {
						mTavoloDiGioco.registerBet(A_player, Horse.HORSE_YELLOW, 700, KindOfBet.WINNER);
						Assert.fail("Should raise exception.");
						return false;
					} catch (AlreadyPresentBetException e) {
						try {
							mTavoloDiGioco.registerBet(A_player, Horse.HORSE_RED, 200, KindOfBet.WINNER);
							mNumberOfTimesBetCalled++;
							return true;
						} catch (AlreadyPresentBetException e1) {
							Assert.fail("Should not raise alredy present exception. " + e.getMessage());
							return false;
						}
					}
				} else {
					mNumberOfTimesBetCalled = 0;
					return false;
				}
			}
		} else {
			Assert.fail("Should be just 3 turns");
			return false;
		}
	}

	@Override
	public void onExpectPlacedCard(String pPlayerName,
			List<String> pAvailableCards) {
		if(mTurn == PRIMO) {
			if(mNumberOfTimesCardPlacedCalled == 0) {
				try {
					mTavoloDiGioco.assignCardToHorse(A_player, "Serum Maleficum", Horse.HORSE_YELLOW);
					Assert.fail("Non � stata sollevata l'eccezione");
				} catch (PlayerDoNotOwnSpecifiedCardException e) {
					try {
						mTavoloDiGioco.assignCardToHorse(A_player, "Flagellum Fulguris", Horse.HORSE_YELLOW);
						mNumberOfTimesCardPlacedCalled++;
					} catch (PlayerDoNotOwnSpecifiedCardException e1) {
						Assert.fail("Should not except. " + e1.getMessage());
					}
				}
			} else if(mNumberOfTimesCardPlacedCalled == 1) {
				try {
					mTavoloDiGioco.assignCardToHorse(B_player, "Venenum Veneficum", Horse.HORSE_BLUE);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 2) {
				try {
					mTavoloDiGioco.assignCardToHorse(C_player, "Magna Velocitas", Horse.HORSE_BLACK);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 3) {
				try {
					mTavoloDiGioco.assignCardToHorse(A_player, "Felix Infernalis", Horse.HORSE_GREEN);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 4) {
				try {
					mTavoloDiGioco.assignCardToHorse(B_player, "Steven Sting", Horse.HORSE_WHITE);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 5) {
				try {
					mTavoloDiGioco.assignCardToHorse(C_player, "In Igni Veritas", Horse.HORSE_RED);
					mNumberOfTimesCardPlacedCalled = 0;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			}
		} else if (mTurn == SECONDO) { // Remember, the player rotated
			if(mNumberOfTimesCardPlacedCalled == 0) {
				try {
					mTavoloDiGioco.assignCardToHorse(B_player, "Serum Maleficum", Horse.HORSE_YELLOW);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 1) {
				try {
					mTavoloDiGioco.assignCardToHorse(C_player, "Aqua Putrida", Horse.HORSE_BLUE);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 2) {
				try {
					mTavoloDiGioco.assignCardToHorse(A_player, "Mala Tempora", Horse.HORSE_BLACK);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 3) {
				try {
					mTavoloDiGioco.assignCardToHorse(B_player, "Fustis et Radix", Horse.HORSE_WHITE);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 4) {
				try {
					mTavoloDiGioco.assignCardToHorse(C_player, "Alfio Allibratore", Horse.HORSE_GREEN);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 5) {
				try {
					mTavoloDiGioco.assignCardToHorse(A_player, "XIII", Horse.HORSE_RED);
					mNumberOfTimesCardPlacedCalled = 0;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			}
		} else if (mTurn == TERZO) { // Remember, the player rotated
			if(mNumberOfTimesCardPlacedCalled == 0) {
				try {
					mTavoloDiGioco.assignCardToHorse(C_player, "Serum Maleficum", Horse.HORSE_BLUE);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 1) {
				try {
					mTavoloDiGioco.assignCardToHorse(A_player, "Fritz Finden", Horse.HORSE_BLACK);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 2) {
				try {
					mTavoloDiGioco.assignCardToHorse(B_player, "Vigor Ferreum", Horse.HORSE_WHITE);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 3) {
				try {
					mTavoloDiGioco.assignCardToHorse(C_player, "Herba Magica", Horse.HORSE_BLUE);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 4) {
				try {
					mTavoloDiGioco.assignCardToHorse(A_player, "Aqua Putrida", Horse.HORSE_BLACK);
					mNumberOfTimesCardPlacedCalled++;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			} else if(mNumberOfTimesCardPlacedCalled == 5) {
				try {
					mTavoloDiGioco.assignCardToHorse(B_player, "Felix Infernalis", Horse.HORSE_WHITE);
					mNumberOfTimesCardPlacedCalled = 0;
				} catch (PlayerDoNotOwnSpecifiedCardException e1) {
					Assert.fail("Should not except. " + e1.getMessage());
				}
			}
		}
		
	}

	@Override
	public void onTurnFinished() {
		if(mTurn == PRIMO) {
			Assert.assertTrue(mRightBets == 3);
			Assert.assertTrue(mRightStables == 2);
			mRightBets = 0;
			mRightStables = 0;
		} else if (mTurn == SECONDO) {
			Assert.assertTrue(mRightBets == 0);
			Assert.assertTrue(mRightStables == 0);
			mRightBets = 0;
			mRightStables = 0;
		} else if (mTurn == TERZO) {
			Assert.assertTrue(mRightBets == 2);
			Assert.assertTrue(mRightStables == 2);
			mRightBets = 0;
			mRightStables = 0;
		}
		mTurn++;
		mNumberOfTimesBetCalled = 0;
		mNumberOfTimesCardPlacedCalled = 0;
		Log.user("======================================================================");
	}

	@Override
	public void onRemovedPVInsteadOfBet(String pPlayerName) {
		Assert.assertTrue(pPlayerName.equals(C_player) && mTurn == TERZO);
		Log.user(pPlayerName + " perde 2 PV invece che scommettere");
	}

	@Override
	public void onPlayerEliminated(String pPlayerName) {
		Assert.fail("No player shoudl be eliminated");
	}

	@Override
	public void onGameStarted(Map<String, String> pPlayerNamePlayerCardMap,
			int pTotalNumberOfTurns) {
		mTurn = PRIMO;
		Assert.assertEquals(pPlayerNamePlayerCardMap.get(A_player), "Cranio Mercanti");
		Assert.assertEquals(pPlayerNamePlayerCardMap.get(B_player), "Steve Mc Skull");
		Assert.assertEquals(pPlayerNamePlayerCardMap.get(C_player), "Viktor Von Schadel");
	}

	@Override
	public void onEndOfGame(String pWinnerPlayerName) {
		Assert.assertEquals(A_player, pWinnerPlayerName);
	}

	@Override
	public void startPlayerRegistration() {
		try {
			mTavoloDiGioco.registerPlayer(A_player);
			mTavoloDiGioco.registerPlayer(B_player);
			mTavoloDiGioco.registerPlayer(C_player);
			mTavoloDiGioco.registerPlayer(C_player);
			Assert.fail("Missed the exception");
		} catch (AlreadyRegisteredPlayerNameException e) {
			// should be reached
		} catch (NotEnoughPlayerSlotsException e) {
			Assert.fail("Should not be reached in this game.");
		}
		mTavoloDiGioco.startMatch();
	}

	@Override
	public void onActionCardRevealed(Horse pHorse,
			List<String> pActionCardsAssigned, List<String> pActionCardsRemoved) {}
}
