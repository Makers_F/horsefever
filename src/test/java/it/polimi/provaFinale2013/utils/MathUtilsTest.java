package it.polimi.provaFinale2013.utils;

import static org.junit.Assert.*;
import it.polimi.provaFinale2013.utils.MathUtils;

import org.junit.Test;

public class MathUtilsTest {

	@Test
	public void testClamp() {
		for(int i = 0; i < 1000; i++) {
			int random = (int) (Math.random() * 25);
			int check = MathUtils.clamp(random, 2, 7);
			assertTrue(check >= 2 && check <= 7);
		}
	}
}