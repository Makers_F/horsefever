package it.polimi.provaFinale2013.utils;

import static org.junit.Assert.*;
import it.polimi.provaFinale2013.horsefever.cavalli.Colour;
import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.utils.EnumUtils;

import org.junit.Test;

public class EnumUtilsTest {

	@Test
	public void testIndexing() {
		for(Horse horse : Horse.values()) {
			int index = EnumUtils.getIndex(horse);
			Horse indexedHorse = EnumUtils.getEnumFromIndex(index, Horse.class);
			assertEquals("The enum got from the index should be equal to the original enum", horse, indexedHorse);
		}
	}

	@Test
	public void testGetHorsefromStable() {
		for(Horse horse : Horse.values()) {
			Colour color = horse.getColor();
			assertEquals("Il cavallo deve essere dello stesso colore della scuderia",
				horse,
				EnumUtils.getHorsefromStable(Stable.getStableFromColor(color)));
		}
	}

	@Test
	public void testGetStableFromHorse() {
		for(Stable stable : Stable.values()) {
			Colour color = stable.getColor();
			assertEquals("La scuderia deve essere dello stesso colore del cavallo",
				stable,
				EnumUtils.getStableFromHorse(Horse.getHorseFromColor(color)));
		}
	}
}