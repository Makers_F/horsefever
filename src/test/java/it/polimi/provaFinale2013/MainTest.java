package it.polimi.provaFinale2013;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Test;

import it.polimi.provaFinale2013.communication.ClientSideITavolo;
import it.polimi.provaFinale2013.communication.IncomingConnectionHandler;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco;
import it.polimi.provaFinale2013.horsefever.tavolo.TavoloDiGioco;
import it.polimi.provaFinale2013.utils.DebugRandom;
import it.polimi.provaFinale2013.utils.DebugUtils;
import it.polimi.provaFinale2013.utils.RandomUtils;
import it.polimi.provaFinale2013.view.IViewer;

public class MainTest {


	@Test
	public void testStandardGame() {
		DebugUtils.setDebug(true);
		((DebugRandom) RandomUtils.getRandom()).reset();
		ITavoloDiGioco<IViewer> tavolo = new TavoloDiGioco();
		IViewer viewer = new AutomatedIViewer(tavolo);
		tavolo.registerListener(viewer);
		viewer.startPlayerRegistration();
		((DebugRandom) RandomUtils.getRandom()).reset();
		DebugUtils.setDebug(false);
	}

	@Test
	public void testInternetStandardGame() {
		DebugUtils.setDebug(true);
		final int port = 6000;
		((DebugRandom) RandomUtils.getRandom()).reset();
		ITavoloDiGioco<IViewer> tavolo = new TavoloDiGioco();
		IncomingConnectionHandler ich = null;
		try {
			ich = new IncomingConnectionHandler(tavolo, port);
		} catch (IOException e1) {
			Assert.fail();
		}
		ich.start();
		ClientSideITavolo serverProxy = new ClientSideITavolo("localhost", port);
		IViewer mViewer = new AutomatedIViewer(serverProxy);
		serverProxy.registerListener(mViewer);
		try {
			serverProxy.startListening();
		} catch (IOException e) {
			Assert.fail("Should have no connection problems");
		}
		((DebugRandom) RandomUtils.getRandom()).reset();
		DebugUtils.setDebug(false);
	}
}
