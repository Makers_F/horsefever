package it.polimi.provaFinale2013.graphic;

import java.awt.Color;
import java.awt.Container;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;

import it.polimi.provaFinale2013.horsefever.cavalli.Stable;

public class QuotationsManager {

	public static final int X_GENERIC_COLUMN = 298;
	private static final int X_QUOTATIONS_COLUMN = 309;
	private static final int Y_FIRST_QUOTATION = 187;
	private static final int QUOTATION_VERTICAL_DISTANCE = 72;
	private static final int QUOTATION_WIDTH_AND_HEIGHT = 50;

	private JLabel mWhiteQuotation = new JLabel();
	private JLabel mYellowQuotation = new JLabel();
	private JLabel mRedQuotation = new JLabel();
	private JLabel mGreenQuotation = new JLabel();
	private JLabel mBlueQuotation = new JLabel();
	private JLabel mBlackQuotation = new JLabel();
	private Container mContainer;

	private Map<Stable, JLabel> mStableToJLabelMap = new HashMap<Stable, JLabel>();

	public QuotationsManager(Container pContainer) {
		mContainer = pContainer;

		mStableToJLabelMap.put(Stable.STABLE_WHITE, mWhiteQuotation);
		mStableToJLabelMap.put(Stable.STABLE_YELLOW, mYellowQuotation);
		mStableToJLabelMap.put(Stable.STABLE_RED, mRedQuotation);
		mStableToJLabelMap.put(Stable.STABLE_GREEN, mGreenQuotation);
		mStableToJLabelMap.put(Stable.STABLE_BLUE, mBlueQuotation);
		mStableToJLabelMap.put(Stable.STABLE_BLACK, mBlackQuotation);
	}

	/**
	 * Set the position of the "1:" labels next to the horse starting square
	 * @param pJFrame The JFrame in which the labels will be shown
	 */
	public void setQuotationPosition(JFrame pJFrame) {
		List<JLabel> quotations = loadQuotations();
		int heightOffset = 0;
		for(JLabel thisQuotation : quotations) {
			thisQuotation.setBounds(X_QUOTATIONS_COLUMN, Y_FIRST_QUOTATION + heightOffset, QUOTATION_WIDTH_AND_HEIGHT, QUOTATION_WIDTH_AND_HEIGHT);
			thisQuotation.setForeground(Color.LIGHT_GRAY);
			pJFrame.add(thisQuotation);
			JLabel generic = new JLabel("1:");
			generic.setBounds(X_GENERIC_COLUMN, Y_FIRST_QUOTATION + heightOffset, QUOTATION_WIDTH_AND_HEIGHT, QUOTATION_WIDTH_AND_HEIGHT);
			generic.setForeground(Color.LIGHT_GRAY);
			pJFrame.add(generic);
			heightOffset += QUOTATION_VERTICAL_DISTANCE;
		}
	}

	/**
	 * Called to update stable quotation
	 * @param pStable The stable to update
	 * @param pQuotation The number X of 1:X quotation
	 */
	public void updateQuotation(Stable pStable, int pQuotation) {
		for(Map.Entry<Stable, JLabel> stableQuotation : mStableToJLabelMap.entrySet()) {
			if(stableQuotation.getKey().equals(pStable)) {
				stableQuotation.getValue().setText(String.valueOf(pQuotation));
				mContainer.add(stableQuotation.getValue());
			}
		}
	}

	private List<JLabel> loadQuotations() {
		List<JLabel> quotations = new LinkedList<JLabel>();
		quotations.add(mWhiteQuotation);
		quotations.add(mYellowQuotation);
		quotations.add(mRedQuotation);
		quotations.add(mGreenQuotation);
		quotations.add(mBlueQuotation);
		quotations.add(mBlackQuotation);
		return quotations;
	}
}