package it.polimi.provaFinale2013.graphic;

import java.awt.Container;
import java.awt.Image;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.utils.ResourcesUtils;

public class CardsManager {

	private static final JLabel MOVEMENT_DECK = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CMRETRO));
	private static final int FIRST_CARD_OFFSET = 95;
	private static final int SECOND_CARD_OFFSET = 110;
	private static final int CARD_ASSIGNED_TO_PLAYER_OFFSET = 45;
	private static final int CARD_ASSIGNED_TO_HORSE_OFFSET = 10;
	private static final int INITIAL_NUMBER_OF_CARDS_ASSIGNED = 0;
	private static final int CARD_TO_CARD_OFFSET = 6;

	private Container mContainer;
	private JLabel mMovementCard = new JLabel();
	private Map<String, JLabel> mJLabelToCardNameMap = new HashMap<String, JLabel>();
	private Map<Horse, Integer> mNumberOfCardsAssignedToHorse = new HashMap<Horse, Integer>();
	private List<String> mPlayersThatAlreadyOwnACard = new LinkedList<String>();

	public CardsManager(Container pContainer) {
		mContainer = pContainer;
		resetNumberOfCardsAssigned();
	}

	/**
	 * Shows the last Movement Card revealed
	 * @param pImageNumbers The numbers contained in the card, in order of appearance
	 */
	public void placeMovementCardOnTheBoard(String pImageNumbers) {
		mMovementCard.setIcon(null);
		mMovementCard.setIcon(ResourcesUtils.loadImageIcon("Carta Movimento " + pImageNumbers));
		mMovementCard.setBounds(905, 5, mMovementCard.getPreferredSize().width, mMovementCard.getPreferredSize().height);
		mContainer.add(mMovementCard);
		mMovementCard.repaint();
	}

	/**
	 * Hides the MovementCards that have been revealed before in the game
	 */
	public void resetMovementCards() {
		MOVEMENT_DECK.setBounds(800, 5, MOVEMENT_DECK.getPreferredSize().width, MOVEMENT_DECK.getPreferredSize().height);
		mContainer.add(MOVEMENT_DECK);
		// Nascondo l'ultima carta movimento girata
		mMovementCard.setIcon(null);
	}

	/**
	 * Places a card next to a player
	 * @param pPlayerName The name of the player
	 * @param pCardName The name of the card
	 * @param pPlayerManager The PlayerManager that is being used
	 */
	public void placeHiddenActionCardNextToPlayer(String pPlayerName, String pCardName, PlayerManager pPlayerManager) {
		int playerCount = pPlayerManager.getPlayerToCounterMap().get(pPlayerName);
		JLabel hiddenCard = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.RETRO_CARTA_AZIONE));
		int offset = !mPlayersThatAlreadyOwnACard.contains(pPlayerName) ? FIRST_CARD_OFFSET : SECOND_CARD_OFFSET;
		hiddenCard.setBounds(pPlayerManager.getXValuesColumn() + offset, pPlayerManager.getYValuesColum(playerCount), hiddenCard.getPreferredSize().width, hiddenCard.getPreferredSize().height);
		mContainer.add(hiddenCard);
		hiddenCard.repaint();
		mJLabelToCardNameMap.put(pCardName, hiddenCard);
		mPlayersThatAlreadyOwnACard.add(pPlayerName);
	}

	/**
	 * Places a card next to a horse
	 * @param pPlayerName The name of player who is playing the card
	 * @param pCardName The name of card that is being assigned
	 * @param pHorse The horse that is receiving the card
	 * @param pHorseManager The HorseManager that is being used
	 */
	public void assignActionCardToHorse(String pPlayerName, String pCardName, Horse pHorse, HorseManager pHorseManager) {
		Log.d(pHorse + " " + pCardName);
		JLabel card = mJLabelToCardNameMap.get(pCardName);
		int numberOfCards = mNumberOfCardsAssignedToHorse.get(pHorse);
		Log.d("Number of cards assigned to " + pHorse + " : " + numberOfCards);
		int offset = CARD_TO_CARD_OFFSET * numberOfCards;
		Log.d("offset = " + offset);
		card.setLocation(QuotationsManager.X_GENERIC_COLUMN - CARD_ASSIGNED_TO_PLAYER_OFFSET - offset, pHorseManager.getHorseOrdinate(pHorse) - CARD_ASSIGNED_TO_HORSE_OFFSET);
		numberOfCards += 1;
		mNumberOfCardsAssignedToHorse.put(pHorse, numberOfCards);
	}

	/**
	 * Flips the cards on the screen
	 * @param pActionCardsNames The list of the names of the cards
	 */
	public void revealActionCards(List<String> pActionCardsNames) {
		for(String card : pActionCardsNames) {
			JLabel label = mJLabelToCardNameMap.get(card);
			label.setIcon(resizeImageIcon(ResourcesUtils.loadImageIcon(card), 41, 70));
		}
	}

	/**
	 * Hides the removed (due to conflicts) action cards
	 * @param pActionCardsNames The list of the names of the cards
	 */
	public void removeActionCard(List<String> pActionCardsNames) {
		for(String card : pActionCardsNames) {
			mJLabelToCardNameMap.get(card).setVisible(false);
		}
	}

	/**
	 * Removes all the ActionCards that are placed on the screen
	 */
	public void removeAllActionCardsOnTheScreen() {
		for(Map.Entry<String, JLabel> entryset : mJLabelToCardNameMap.entrySet()) {
			entryset.getValue().setVisible(false);
		}
		mPlayersThatAlreadyOwnACard.clear();
		mJLabelToCardNameMap.clear();
		resetNumberOfCardsAssigned();
		resetMovementCards();
	}

	private ImageIcon resizeImageIcon(ImageIcon pImageIcon, int newWidth, int newHeight) {
		return new ImageIcon(pImageIcon.getImage().getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH));
	}

	private void resetNumberOfCardsAssigned() {
		mNumberOfCardsAssignedToHorse.put(Horse.HORSE_BLACK, INITIAL_NUMBER_OF_CARDS_ASSIGNED);
		mNumberOfCardsAssignedToHorse.put(Horse.HORSE_BLUE, INITIAL_NUMBER_OF_CARDS_ASSIGNED);
		mNumberOfCardsAssignedToHorse.put(Horse.HORSE_GREEN, INITIAL_NUMBER_OF_CARDS_ASSIGNED);
		mNumberOfCardsAssignedToHorse.put(Horse.HORSE_RED, INITIAL_NUMBER_OF_CARDS_ASSIGNED);
		mNumberOfCardsAssignedToHorse.put(Horse.HORSE_WHITE, INITIAL_NUMBER_OF_CARDS_ASSIGNED);
		mNumberOfCardsAssignedToHorse.put(Horse.HORSE_YELLOW, INITIAL_NUMBER_OF_CARDS_ASSIGNED);
	}
}
