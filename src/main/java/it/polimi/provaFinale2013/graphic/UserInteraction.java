package it.polimi.provaFinale2013.graphic;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.scommesse.KindOfBet;
import it.polimi.provaFinale2013.utils.ResourcesUtils;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class UserInteraction {

	private static final int MAX_PLAYER_NAME_LENGHT = 10;
	private static final String EMPTY_STRING = "";

	/**
	 * Called when a player is registered in the game
	 * @param pJFrame The JFrame that will contain the dialog
	 * @return The name that the player has written
	 */
	public String registerPlayerDialog(JFrame pJFrame) {
		String name = JOptionPane.showInputDialog(pJFrame, "Write the player name. Leave blank and press 'Enter' if you don't want to add another player.", "Player Registration", JOptionPane.DEFAULT_OPTION);
		if(name == null) {
			return EMPTY_STRING;
		}
		if(name.trim().length() > MAX_PLAYER_NAME_LENGHT) {
			return name.trim().substring(0, MAX_PLAYER_NAME_LENGHT);
		}
		return name.trim();
	}

	/**
	 * Called when a player has to bet
	 * @param pJFrame The JFrame that will contain the dialog
	 * @return The amount of money the player wants to bet
	 */
	public int amountOfTheBetDialog(JFrame pJFrame, int pMinBet, int pMaxBet, String pTitle) {
		String moneyAmount = JOptionPane.showInputDialog(pJFrame, "Write how much money you want to bet [" + pMinBet + "," + pMaxBet + "]. It must be a value divisible by 100.", pTitle, JOptionPane.QUESTION_MESSAGE);
		if(moneyAmount != null && isInteger(moneyAmount.trim())) {
			int value = Integer.valueOf(moneyAmount.trim());
			if(value < pMinBet || value > pMaxBet) {
				generalMessage("Please write a number in the range [" + pMinBet + "," + pMaxBet + "]");
				return amountOfTheBetDialog(pJFrame, pMinBet, pMaxBet, pTitle);
			} else if(!(value % 100 == 0)) {
				generalMessage("Please write a number divisible by 100.");
				return amountOfTheBetDialog(pJFrame, pMinBet, pMaxBet, pTitle);
			}
			return value;
		} else {
			return amountOfTheBetDialog(pJFrame, pMinBet, pMaxBet, pTitle);
		}
	}

	/**
	 * Used to select a horse from a list of horses
	 * @param pJFrame The JFrame that will contain the dialog
	 * @param pMessage The message that will be shown
	 * @return The horse selected
	 */
	public Horse horseOfTheBetDialog(JFrame pJFrame, String pMessage) {
		Horse horse = (Horse) JOptionPane.showInputDialog(pJFrame, pMessage, "Horse Selection", JOptionPane.INFORMATION_MESSAGE, null, Horse.values(), null);
		if(horse == null) {
			return horseOfTheBetDialog(pJFrame, pMessage);
		}
	    return horse;
	}

	/**
	 * Used to select a KindOfBet from a list of KindOfBets
	 * @param pJFrame The JFrame that will contain the dialog
	 * @return The KindOfBet selected
	 */
	public KindOfBet kindOfBetDialog(JFrame pJFrame) {
		KindOfBet kindOfBet = (KindOfBet) JOptionPane.showInputDialog(pJFrame, "Select which kind of bet you want to do.", "Kind Of Bet Selection", JOptionPane.INFORMATION_MESSAGE, null, KindOfBet.values(), null);
		if(kindOfBet == null) {
			return kindOfBetDialog(pJFrame);
		}
		return kindOfBet;
	}

	/**
	 * Called when the user has to place a card on a horse
	 * @param pJFrame The JFrame that will contain the dialog
	 * @param pTitle The title of the dialog
	 * @param pMessage The message that will be shown
	 * @param pCardsAvailable The name(s) of the card(s) available
	 * @return The card selected
	 */
	public String cardSelectedDialog(JFrame pJFrame, String pTitle, String pMessage, String[] pCardsAvailable) {
		JLabel cardImages = new JLabel();
		cardImages.setLayout(new BorderLayout());
		cardImages.add(new JLabel(pMessage), BorderLayout.NORTH);
		if(pCardsAvailable.length == 1) {
			cardImages.setIcon(ResourcesUtils.loadImageIcon(pCardsAvailable[0]));
		} else if(pCardsAvailable.length == 2) {
			CompoundIcon coumpoundIcon = new CompoundIcon(ResourcesUtils.loadImageIcon(pCardsAvailable[0]), ResourcesUtils.loadImageIcon(pCardsAvailable[1]));
			cardImages.setIcon(coumpoundIcon);
		}
		String card = (String) JOptionPane.showInputDialog(pJFrame, cardImages, pTitle, JOptionPane.INFORMATION_MESSAGE, null, pCardsAvailable, null);
		if(card == null) {
			return cardSelectedDialog(pJFrame, pTitle, pMessage, pCardsAvailable);
		}
		return card;
	}

	/**
	 * Used to get a "yes or no" will of the user
	 * @param pJFrame The JFrame that will contain the dialog
	 * @param pMessage The message that will be shown
	 * @param pTitle The title of the dialog
	 * @return true if yes, false otherwise
	 */
	public boolean yesOrNoDialog(JFrame pJFrame, String pMessage, String pTitle) {
		return JOptionPane.showConfirmDialog(pJFrame, pMessage, pTitle, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION ? true : false;
	}

	/**
	 * Used to display a personalized message
	 * @param pTitle The title of the dialog
	 * @param pMessage The message that will be shown
	 */
	public void message(String pTitle, String pMessage) {
		JOptionPane.showMessageDialog(null, pMessage, pTitle, JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Used to display a message
	 * @param pMessage The message that will be shown
	 */
	public static void generalMessage(String pMessage) {
		JOptionPane.showMessageDialog(null, pMessage);
	}

	private static boolean isInteger(String s) {
	    try {
	        Integer.parseInt(s);
	    } catch(NumberFormatException e) {
	        return false;
	    }
	    return true;
	}
}
