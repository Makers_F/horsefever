package it.polimi.provaFinale2013.graphic;

import it.polimi.provaFinale2013.horsefever.cavalli.Colour;
import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.utils.ResourcesUtils;

import java.awt.Color;
import java.awt.Container;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class GraphicsManager {

	private static final Map<Colour, Color> ENUMCOLOR_TO_JAVACOLOR_MAP = createColourColorMap();
	private static final Map<Colour, String> COLOUR_TO_SPRINTIMAGE_MAP = createColourSprintMap();

	private JFrame mJFrame;
	private Container mContainer;
	private HorseManager mHorseManager = new HorseManager();
	private QuotationsManager mQuotationManager;
	private PlayerManager mPlayerManager;
	private CardsManager mCardsManager;
	private JLabel mTurnJLabel = new JLabel("< Turn X / X >");
	private JLabel[] mPreviousLabels = new JLabel[2];
	private int mNumberOfSprints = 0;

	public GraphicsManager(JFrame pJFrame) {
		mJFrame = pJFrame;
		mContainer = mJFrame.getContentPane();
		mPlayerManager = new PlayerManager(mContainer);
		mQuotationManager = new QuotationsManager(mContainer);
		mCardsManager = new CardsManager(mContainer);
	}

	public void setHorseStartingPosition() {
		mHorseManager.setHorsePosition(mJFrame);
	}

	public void setInitialQuotations() {
		mQuotationManager.setQuotationPosition(mJFrame);
	}

	public void setPlayerName(String pPlayerName, int pPlayerCount) {
		mPlayerManager.setPlayerName(pPlayerName, pPlayerCount);
	}

	public void setPlayerMoney(String pPlayerName, int pMoney) {
		mPlayerManager.setPlayerMoney(pPlayerName, pMoney);
	}

	public void setPlayerPV(String pPlayerName, int pPV) {
		mPlayerManager.setPlayerPV(pPlayerName, pPV);
	}

	public void updateHorsePosition(Horse pHorse, int pMovement) {
		mHorseManager.updateHorsePosition(pHorse, pMovement);
	}

	public void setColorOfPlayerLabels(String pPlayerName, Stable pStable) {
		mPlayerManager.setColorOfPlayerLabels(pPlayerName, ENUMCOLOR_TO_JAVACOLOR_MAP.get(pStable.getColor()));
	}

	public void updateQuotation(Stable pStable, int pQuotation) {
		mQuotationManager.updateQuotation(pStable, pQuotation);
	}

	public void showPositionOfArrival(Horse pHorse, int pPosition) {
		mHorseManager.showPositionOfArrival(pHorse, pPosition);
	}

	public void removeAllArrivalLabels() {
		mHorseManager.removeAllArrivalLabels();
	}

	public void placeMovementCardOnTheBoard(String pImageNumbers) {
		mCardsManager.placeMovementCardOnTheBoard(pImageNumbers);
	}

	public void resetMovementCards() {
		mCardsManager.resetMovementCards();
	}

	public void placeHiddenActionCardNextToPlayer(String pPlayerName, String pCardName) {
		mCardsManager.placeHiddenActionCardNextToPlayer(pPlayerName, pCardName, mPlayerManager);
	}

	public void assignCardToHorse(String pPlayerName, String pCardName, Horse pHorseChosen) {
		mCardsManager.assignActionCardToHorse(pPlayerName, pCardName, pHorseChosen, mHorseManager);
	}

	public void resetActionCards() {
		mCardsManager.removeAllActionCardsOnTheScreen();
	}

	public void removePlayer(String pPlayerName) {
		mPlayerManager.removePlayer(pPlayerName);
	}

	public void revealActionCards(List<String> pActionCardsNames) {
		mCardsManager.revealActionCards(pActionCardsNames);
	}

	public void removeActionCards(List<String> pActionCardsNames) {
		mCardsManager.removeActionCard(pActionCardsNames);
	}

	/**
	 * Called to show sprint squares
	 * @param pHorse The horse that will sprint
	 * @param pHowMuch How many squares it will sprint
	 */
	public void showSprintColors(Horse pHorse, int pHowMuch) {
		JLabel sprint = new JLabel(ResourcesUtils.loadImageIcon(COLOUR_TO_SPRINTIMAGE_MAP.get(pHorse.getColor())));
		int y_position = mNumberOfSprints == 0 ? 30 : 100;
		sprint.setBounds(760, y_position, sprint.getPreferredSize().width, sprint.getPreferredSize().height);
		mContainer.add(sprint);
		sprint.repaint();
		mPreviousLabels[mNumberOfSprints] = sprint;
		mNumberOfSprints++;
	}

	/**
	 * Called to hide sprint squares
	 */
	public void resetSprint() {
		for(JLabel label : mPreviousLabels) {
			if(label != null) {
				label.setVisible(false);
			}
		}
		mNumberOfSprints = 0;
	}

	/**
	 * Updates the counter of turns
	 * @param pTurnCounter Which turn is this
	 * @param pNumberOfTotalTurns The totaal number of turns
	 */
	public void updateTurn(int pTurnCounter, int pNumberOfTotalTurns) {
		mTurnJLabel.setForeground(Color.LIGHT_GRAY);
		mTurnJLabel.setBounds(320, 150, mTurnJLabel.getPreferredSize().width, mTurnJLabel.getPreferredSize().height);
		mContainer.add(mTurnJLabel);
		mTurnJLabel.setText("< Turn " + pTurnCounter + " / " + pNumberOfTotalTurns + " >");
	}

	private static Map<Colour, Color> createColourColorMap() {
		Map<Colour, Color> result = new HashMap<Colour, Color>();
		result.put(Colour.BLACK, Color.BLACK);
		result.put(Colour.BLUE, Color.BLUE);
		result.put(Colour.GREEN, Color.GREEN);
		result.put(Colour.RED, Color.RED);
		result.put(Colour.WHITE, Color.WHITE);
		result.put(Colour.YELLOW, Color.YELLOW);
		return Collections.unmodifiableMap(result);
	}

	private static Map<Colour, String> createColourSprintMap() {
		Map<Colour, String> map = new HashMap<Colour, String>();
		map.put(Colour.BLACK, ResourcesUtils.SPRINT_NERO);
		map.put(Colour.BLUE, ResourcesUtils.SPRINT_BLU);
		map.put(Colour.GREEN, ResourcesUtils.SPRINT_VERDE);
		map.put(Colour.RED, ResourcesUtils.SPRINT_ROSSO);
		map.put(Colour.WHITE, ResourcesUtils.SPRINT_BIANCO);
		map.put(Colour.YELLOW, ResourcesUtils.SPRINT_GIALLO);
		return Collections.unmodifiableMap(map);
		
	}
}