package it.polimi.provaFinale2013.graphic;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;

public class PlayerManager {

	private static final int Y_PLAYER_FIRST_ELEMENT = 115;
	private static final int Y_PLAYER_ELEMENT_OFFSET = 20;
	private static final int Y_DISTANCE_FROM_PLAYER_TO_PLAYER = 80;
	private static final int X_PLAYERS_COLUMN = 15;
	private static final int X_VALUES_COLUMN = 70;

	private JLabel[] mPlayerName = {new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel()};
	private JLabel[] mPlayerMoney = {new JLabel("0"), new JLabel("0"), new JLabel("0"), new JLabel("0"), new JLabel("0"), new JLabel("0")};
	private JLabel[] mPlayerPV = {new JLabel("0"), new JLabel("0"), new JLabel("0"), new JLabel("0"), new JLabel("0"), new JLabel("0")};
	private JLabel[] mGeneralName = {new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel()};
	private JLabel[] mGeneralMoney = {new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel()};
	private JLabel[] mGeneralPV = {new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel()};
	private Map<String, Integer> mPlayerNameToPlayerCount = new HashMap<String, Integer>();
	private Container mContainer;

	public PlayerManager(Container pContainer) {
		mContainer = pContainer;
	}

	/**
	 * Sets the player name
	 * @param pName The name that will be written
	 * @param pCount The ordinal number of the order of the player out of the total number of players
	 */
	public void setPlayerName(String pName, int pCount) {
		mPlayerNameToPlayerCount.put(pName, pCount);
		setPlayerGeneralLabels(pCount, Color.LIGHT_GRAY);
		mPlayerName[pCount].setText(pName);
		mPlayerName[pCount].setForeground(Color.WHITE);
		mContainer.add(mPlayerName[pCount]);
		Dimension size = mPlayerName[pCount].getPreferredSize();
		int position = (Y_PLAYER_FIRST_ELEMENT + (Y_DISTANCE_FROM_PLAYER_TO_PLAYER * pCount));
		mPlayerName[pCount].setBounds(X_VALUES_COLUMN, position, size.width, size.height);
	}

	/**
	 * Sets the new amount of money. Use positive numbers to add money and negative numbers to subtract money
	 * @param pPlayerName The player whose money will be updated
	 * @param pAmount The amount of money
	 */
	public void setPlayerMoney(String pPlayerName , int pAmount) {
		for(Map.Entry<String, Integer> playerCount : mPlayerNameToPlayerCount.entrySet()) {
			int count = playerCount.getValue();
			if(playerCount.getKey().equals(pPlayerName)) {
				int ownedAmount = Integer.valueOf(mPlayerMoney[count].getText());
				ownedAmount += pAmount;
				mPlayerMoney[count].setText(String.valueOf(ownedAmount));
				mPlayerMoney[count].setForeground(Color.WHITE);
				mContainer.add(mPlayerMoney[playerCount.getValue()]);
				mContainer.setVisible(false);
				mContainer.setVisible(true);
				Dimension size = mPlayerMoney[count].getPreferredSize();
				int position = (Y_PLAYER_FIRST_ELEMENT + Y_PLAYER_ELEMENT_OFFSET + (Y_DISTANCE_FROM_PLAYER_TO_PLAYER * count));
				mPlayerMoney[count].setBounds(X_VALUES_COLUMN, position, size.width, size.height);
			}
		}
	}

	/**
	 * Sets the new amount of PVs. Use positive numbers to add PVs and negative numbers to subtract PVs
	 * @param pPlayerName The player whose PVs will be updated
	 * @param pAmount The amount of PVs
	 */
	public void setPlayerPV(String pPlayerName, int pAmount) {
		for(Map.Entry<String, Integer> playerCount : mPlayerNameToPlayerCount.entrySet()) {
			int count = playerCount.getValue();
			if(playerCount.getKey().equals(pPlayerName)) {
				int ownedPv = Integer.valueOf(mPlayerPV[count].getText());
				ownedPv += pAmount;
				mPlayerPV[count].setText(String.valueOf(ownedPv));
				mPlayerPV[count].setForeground(Color.WHITE);
				mContainer.add(mPlayerPV[playerCount.getValue()]);
				mContainer.setVisible(false);
				mContainer.setVisible(true);
				Dimension size = mPlayerPV[count].getPreferredSize();
				int position = (Y_PLAYER_FIRST_ELEMENT + (Y_PLAYER_ELEMENT_OFFSET * 2) + (Y_DISTANCE_FROM_PLAYER_TO_PLAYER * count));
				mPlayerPV[count].setBounds(X_VALUES_COLUMN, position, size.width, size.height);
			}
		}
	}

	/**
	 * Called to set the color of the labels "player", "money", "pv" according to the Stable related to the assigned PlayerCard
	 * @param pPlayerName The player whose labels will be colored
	 * @param pColor The color derived from the layerCard
	 */
	public void setColorOfPlayerLabels(String pPlayerName, Color pColor) {
		for(Map.Entry<String, Integer> playerCount : mPlayerNameToPlayerCount.entrySet()) {
			if(playerCount.getKey().equals(pPlayerName)) {
				setPlayerGeneralLabels(playerCount.getValue(), pColor);
			}
		}
	}

	public void removePlayer(String pPlayerName) {
		int count = mPlayerNameToPlayerCount.get(pPlayerName);
		mGeneralMoney[count].setVisible(false);
		mGeneralName[count].setVisible(false);
		mGeneralPV[count].setVisible(false);
		mPlayerMoney[count].setVisible(false);
		mPlayerName[count].setVisible(false);
		mPlayerPV[count].setVisible(false);
	}

	public Map<String, Integer> getPlayerToCounterMap() {
		return mPlayerNameToPlayerCount;
	}

	public int getXValuesColumn() {
		return X_VALUES_COLUMN;
	}

	public int getYValuesColum(int pCount) {
		return mPlayerName[pCount].getBounds().y;
	}

	private void setPlayerGeneralLabels(int pPlayerCount, Color pColor) {
		JLabel player = mGeneralName[pPlayerCount];
		JLabel money = mGeneralMoney[pPlayerCount];
		JLabel pv = mGeneralPV[pPlayerCount];
		mGeneralName[pPlayerCount].setText("Player: ");
		mGeneralMoney[pPlayerCount].setText("Money: ");
		mGeneralPV[pPlayerCount].setText("PV: ");
		player.setForeground(pColor);
		money.setForeground(pColor);
		pv.setForeground(pColor);
		mContainer.add(player);
		mContainer.add(money);
		mContainer.add(pv);
		int playerPosition = (Y_PLAYER_FIRST_ELEMENT + (Y_DISTANCE_FROM_PLAYER_TO_PLAYER * pPlayerCount));
		int moneyPosition = (Y_PLAYER_FIRST_ELEMENT + Y_PLAYER_ELEMENT_OFFSET + (Y_DISTANCE_FROM_PLAYER_TO_PLAYER * pPlayerCount));
		int pvPosition = (Y_PLAYER_FIRST_ELEMENT + (Y_PLAYER_ELEMENT_OFFSET * 2) + (Y_DISTANCE_FROM_PLAYER_TO_PLAYER * pPlayerCount));
		player.setBounds(X_PLAYERS_COLUMN, playerPosition, player.getPreferredSize().width, player.getPreferredSize().height);
		money.setBounds(X_PLAYERS_COLUMN, moneyPosition, money.getPreferredSize().width, money.getPreferredSize().height);
		pv.setBounds(X_PLAYERS_COLUMN, pvPosition, pv.getPreferredSize().width, pv.getPreferredSize().height);
	}
}
