package it.polimi.provaFinale2013.graphic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.horsefever.scommesse.Bookmaker.AlreadyPresentBetException;
import it.polimi.provaFinale2013.horsefever.scommesse.KindOfBet;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.AlreadyRegisteredPlayerNameException;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.NotEnoughPlayerSlotsException;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.PlayerDoNotOwnSpecifiedCardException;
import it.polimi.provaFinale2013.utils.PlayerCardUtils;
import it.polimi.provaFinale2013.utils.ResourcesUtils;
import it.polimi.provaFinale2013.view.IViewer;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class GUI implements IViewer{

	private static final int STARTING_LINE = 0;
	private static final int INITIAL_AMOUNT_OF_PV = 1;
	private static final int PV_REMOVED_FOR_INSUFFICIENT_MONEY = 2;

	private final ITavoloDiGioco<IViewer> mTavolo;

	private UserInteraction mUserInteraction = new UserInteraction();
	private GraphicsManager mGraphicsManager;
	private JFrame mFrame = new JFrame("Horse Fever");
	private Set<String> mPlayersRegisteredInThisGUI = new HashSet<String>();
	private int mWhichTurnIsThis = 1;
	private int mTotalNumberOfTurns;
	private Map<Stable, String> mPlayerToStableMap = new HashMap<Stable, String>();
	private Map<Stable, Integer> mStableToQuotationMap = new HashMap<Stable, Integer>();

	public GUI(ITavoloDiGioco<IViewer> pTavoloDiGioco) {
		mTavolo = pTavoloDiGioco;
		new BackgroundImageJFrame(mFrame, ResourcesUtils.TABELLONE);
		mGraphicsManager = new GraphicsManager(mFrame);
		mFrame.setResizable(false);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				mGraphicsManager.setHorseStartingPosition();
				mGraphicsManager.setInitialQuotations();
				// Metto il mazzo delle carte movimento
				mGraphicsManager.resetMovementCards();
			}
		});
	}

	@Override
	public void onSingleQuotationChanged(Stable pStable, int pOldQuotation, int pNewQuotation) {
		mGraphicsManager.updateQuotation(pStable, pNewQuotation);
	}

	@Override
	public void onFullQuotationUpdate(Map<Stable, Integer> pStableToQuotationMap) {
		mStableToQuotationMap.putAll(pStableToQuotationMap);
		for(Map.Entry<Stable, Integer> stableQuotation : pStableToQuotationMap.entrySet()) {
			mGraphicsManager.updateQuotation(stableQuotation.getKey(), stableQuotation.getValue());
		}
	}

	@Override
	public void onRaceStarted() {
	}

	@Override
	public void onMovementCardRevealed(int[] pMovementCardValues) {
		mGraphicsManager.resetSprint();
		// Creo una stringa che contiene tutti i numeri della carta movimento in successione
		StringBuilder sb = new StringBuilder();
		for(int value : pMovementCardValues) {
			sb.append(value);
		}
		mGraphicsManager.placeMovementCardOnTheBoard(sb.toString());
	}

	@Override
	public void onSprint(Horse pHorse, int pSprintNumberInThisTurn) {
		mGraphicsManager.showSprintColors(pHorse, pSprintNumberInThisTurn);
	}

	@Override
	public void onHorsePositionsChanged(Map<Horse, Integer> pHorseToPositionMap) {
		for(Map.Entry<Horse, Integer> horsePosition : pHorseToPositionMap.entrySet()) {
			mGraphicsManager.updateHorsePosition(horsePosition.getKey(), horsePosition.getValue());
		}
	}

	@Override
	public void onHorseArrived(Horse pHorse, int pHorsePosition) {
		// Mostro in che posizione � arrivato il cavallo
		mGraphicsManager.showPositionOfArrival(pHorse, pHorsePosition);
	}

	@Override
	public void onRaceFinished(Horse[] pArrivalOrderedHorses) {
		// Rimetto i cavalli alle posizioni di partenza
		for(Horse horse : pArrivalOrderedHorses) {
			mGraphicsManager.updateHorsePosition(horse, STARTING_LINE);
		}
	}

	@Override
	public void onPlacedBet(String pPlayerName, Horse pHorse, int pValue, KindOfBet pKindOfBet) {
		mGraphicsManager.setPlayerMoney(pPlayerName, -pValue);
	}

	@Override
	public void onWonBet(String pPlayerName, int pAmount, int pPVs,	KindOfBet pKindOfBet) {
		mGraphicsManager.setPlayerMoney(pPlayerName, pAmount);
		mGraphicsManager.setPlayerPV(pPlayerName, pPVs);
	}

	@Override
	public void onStablePaid(Stable pStable, int pAmount) {
		String player = mPlayerToStableMap.get(pStable);
		mGraphicsManager.setPlayerMoney(player, pAmount);
	}

	@Override
	public void onActionCardGivenToPlayer(String pPlayerName, String pCardName) {
		mGraphicsManager.placeHiddenActionCardNextToPlayer(pPlayerName, pCardName);
	}

	@Override
	public boolean onExpectBet(String pPlayerName, int pMinBet, int pMaxBet, Map<Horse, Integer> pHorseToQuotationMap, boolean isMandatory) {
		String dialogTitle;
		boolean playerWantsToBet = true;
		if(isMandatory) {
			dialogTitle = "First Bet";
		} else {
			dialogTitle = "Second Bet";
			playerWantsToBet = mUserInteraction.yesOrNoDialog(mFrame, "Do you want to bet?", "Optional second bet - " + pPlayerName);
		}
		if(playerWantsToBet) {
			int amount = mUserInteraction.amountOfTheBetDialog(mFrame, pMinBet, pMaxBet, dialogTitle + " - " + pPlayerName);
			Horse horse = mUserInteraction.horseOfTheBetDialog(mFrame, "Select which horse you want to bet on.");
			KindOfBet kindOfBet = mUserInteraction.kindOfBetDialog(mFrame);
			try {
				mTavolo.registerBet(pPlayerName, horse, amount, kindOfBet);
				return true;
			} catch (AlreadyPresentBetException e) {
				mUserInteraction.message("Bet error","This bet already exists.");
				return onExpectBet(pPlayerName, pMinBet, pMaxBet, pHorseToQuotationMap, isMandatory);
			}
		} else {
			return false;
		}
	}

	@Override
	public void onExpectPlacedCard(String pPlayerName, List<String> pAvailableCards) {
		String[] cardNames = new String[pAvailableCards.size()];
		for(String card : pAvailableCards) {
			cardNames[pAvailableCards.indexOf(card)] = card;
		}
		String cardSelected = mUserInteraction.cardSelectedDialog(mFrame, "Card selection - " + pPlayerName, "Select which card you want to use.", cardNames);
		Horse horseSelected = mUserInteraction.horseOfTheBetDialog(mFrame, "Select on which horse you want to play the card");
		try {
			mTavolo.assignCardToHorse(pPlayerName, cardSelected, horseSelected);
		} catch (PlayerDoNotOwnSpecifiedCardException e) {
			mUserInteraction.message("Card error", "Player " + pPlayerName + " doesn't own this card.");
			onExpectPlacedCard(pPlayerName, pAvailableCards);
		}
	}

	@Override
	public void onTurnFinished() {
		mGraphicsManager.updateTurn(mWhichTurnIsThis, mTotalNumberOfTurns);
		mGraphicsManager.setHorseStartingPosition();
		mGraphicsManager.resetMovementCards();
		mGraphicsManager.removeAllArrivalLabels();
		mGraphicsManager.resetActionCards();
		mGraphicsManager.resetSprint();
		mWhichTurnIsThis++;
	}

	@Override
	public void onRemovedPVInsteadOfBet(String pPlayerName) {
		mGraphicsManager.setPlayerPV(pPlayerName, -PV_REMOVED_FOR_INSUFFICIENT_MONEY);
		mUserInteraction.message("Bet info", pPlayerName + " doesn't have enough money to bet and he's been removed 2 PV.");
	}

	@Override
	public void onPlayerEliminated(String pPlayerName) {
		mUserInteraction.message("Player removed", pPlayerName + " has lost the game and it's been removed.");
		boolean wasOneOfThisViewPlayer = mPlayersRegisteredInThisGUI.contains(pPlayerName);
		mPlayersRegisteredInThisGUI.remove(pPlayerName);
		if( wasOneOfThisViewPlayer && mPlayersRegisteredInThisGUI.isEmpty()) {
			boolean beObserver = mUserInteraction.yesOrNoDialog(mFrame, "Do you want to continue watching the game as an observer?", "Player Eliminated");
			if(!beObserver) {
				System.exit(0);
			}
		}
	}

	@Override
	public void onGameStarted(Map<String, String> pPlayerNamePlayerCardMap,	int pTotalNumberOfTurns) {
		// Aggiorno e mostro il contatore dei turni
		mTotalNumberOfTurns = pTotalNumberOfTurns;
		mGraphicsManager.updateTurn(mWhichTurnIsThis, pTotalNumberOfTurns);
		mWhichTurnIsThis++;
		int playerCounter = 0;
		for(Map.Entry<String, String> playernamePlayercard : pPlayerNamePlayerCardMap.entrySet()) {
			String playerName = playernamePlayercard.getKey();
			String playerCard = playernamePlayercard.getValue();
			int quotation = PlayerCardUtils.getInitialQuotationOfAssociatedStable(playerCard);
			Stable stable = getStableFromInitialQuotations(mStableToQuotationMap, quotation);
			// Imposto il nome, i soldi e i PV iniziali
			mGraphicsManager.setPlayerName(playerName, playerCounter);
			mGraphicsManager.setPlayerMoney(playerName, PlayerCardUtils.getInitialAmountOfMoney(playerCard));
			mGraphicsManager.setPlayerPV(playerName, INITIAL_AMOUNT_OF_PV);
			// Creo la Map scuderia -> giocatore
			mPlayerToStableMap.put(stable, playerName);
			// Coloro le label dei giocatori in base alla scuderia associata
			mGraphicsManager.setColorOfPlayerLabels(playerName, stable);
			playerCounter++;
		}
	}

	@Override
	public void onEndOfGame(String pWinnerPlayerName) {
		mUserInteraction.message("End of the game", pWinnerPlayerName + " has won the game.");
		System.exit(0);
	}

	@Override
	public void startPlayerRegistration() {
		String playerName = mUserInteraction.registerPlayerDialog(mFrame);
		if(playerName.isEmpty()) {
			if(mPlayersRegisteredInThisGUI.isEmpty()) {
				boolean beObserver = mUserInteraction.yesOrNoDialog(mFrame, "No player registered. Do you want to observe the game?", "Observer");
				if(beObserver) {
					mTavolo.startMatch();
				} else {
					System.exit(0);
				}
			} else {
				mTavolo.startMatch();
			}
		} else {
			try {
				mTavolo.registerPlayer(playerName);
				mPlayersRegisteredInThisGUI.add(playerName);
				startPlayerRegistration();
				return;
			} catch (AlreadyRegisteredPlayerNameException e) {
				mUserInteraction.message("Registration error", "Player name already used. Please enter a different one.");
				startPlayerRegistration();
				return;
			} catch (NotEnoughPlayerSlotsException e) {
				if(mPlayersRegisteredInThisGUI.isEmpty()) {
					mUserInteraction.message("Registration error","Maximum number of players reached. You cannot play this game.");
					boolean wantsToWatch = mUserInteraction.yesOrNoDialog(mFrame, "Do you want to observe the game?", "Observer");
					if(!wantsToWatch) {
						System.exit(0);
					}
				} else {
					mUserInteraction.message("Registration error","Sorry, maximum number of players reached. The additional one can watch the game.");
				}
				mTavolo.startMatch();
			}
		}
	}

	@Override
	public void onActionCardAssignedToHorse(String pPlayerName,	String pCardName, Horse pHorse) {
		mGraphicsManager.assignCardToHorse(pPlayerName, pCardName, pHorse);
	}

	@Override
	public void onActionCardRevealed(Horse pHorse, List<String> pActionCardsAssigned, List<String> pActionCardsRemoved) {
		mGraphicsManager.revealActionCards(pActionCardsAssigned);
		mGraphicsManager.removeActionCards(pActionCardsRemoved);

	}

	private Stable getStableFromInitialQuotations(Map<Stable, Integer> pStableToQuotationMap, int pInitialQuotation) {
		for(Map.Entry<Stable, Integer> stableQUotation : pStableToQuotationMap.entrySet()) {
			if(stableQUotation.getValue().equals(pInitialQuotation)) {
				return stableQUotation.getKey();
			}
		}
		throw new IllegalArgumentException();
	}
}