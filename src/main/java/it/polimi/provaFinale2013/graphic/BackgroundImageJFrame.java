package it.polimi.provaFinale2013.graphic;

import it.polimi.provaFinale2013.utils.ResourcesUtils;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

class BackgroundImageJFrame {

	public BackgroundImageJFrame(JFrame pJFrame, String pFilePath) {
		pJFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		pJFrame.setIconImage(ResourcesUtils.loadImageIcon(ResourcesUtils.ICONA).getImage());
		pJFrame.setLayout(new BorderLayout());
		pJFrame.setContentPane(new JLabel(ResourcesUtils.loadImageIcon(pFilePath)));
		pJFrame.setVisible(true);
		pJFrame.pack();
		pJFrame.setLocationRelativeTo(null);
	}
}