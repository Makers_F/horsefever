package it.polimi.provaFinale2013.graphic;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.utils.ResourcesUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class HorseManager {

	private static final int X_FIRST_HORSE_ON_BOARD = 328;
	private static final int Y_FIRST_HORSE_ON_BOARD = 187;
	private static final int HORSE_VERTICAL_DISTANCE = 72;
	private static final int SQUARE_DISTANCE_TO_NEXT_SQUARE = 36;
	private static final int ONE = 1;
	private static final int TWO = 2;
	private static final int THREE = 3;
	private static final int FOUR = 4;
	private static final int FIVE = 5;
	private static final int SIX = 6;

	private static Map<JLabel, Horse> mJLabelToHorseMap = new HashMap<JLabel, Horse>();
	private static Map<JLabel, Stable> mJLabelToStableMap = new HashMap<JLabel, Stable>();

	private Map<Integer, JLabel> mPositionToJLableMap = createPositionJLabelMap();
	private JLabel mHorseWhite = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CAVALLO_BIANCO));
	private JLabel mHorseYellow = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CAVALLO_GIALLO));
	private JLabel mHorseRed = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CAVALLO_ROSSO));
	private JLabel mHorseGreen = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CAVALLO_VERDE));
	private JLabel mHorseBlue = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CAVALLO_BLU));
	private JLabel mHorseBlack = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CAVALLO_NERO));
	private JLabel mStableWhite = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CARTA_SCUDERIA_BIANCO));
	private JLabel mStableYellow = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CARTA_SCUDERIA_GIALLO));
	private JLabel mStableRed = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CARTA_SCUDERIA_ROSSO));
	private JLabel mStableGreen = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CARTA_SCUDERIA_VERDE));
	private JLabel mStableBlue = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CARTA_SCUDERIA_BLU));
	private JLabel mStableBlack = new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.CARTA_SCUDERIA_NERO));
	private JFrame mJFrame;

	public HorseManager() {
		mJLabelToHorseMap.put(mHorseWhite, Horse.HORSE_WHITE);
		mJLabelToHorseMap.put(mHorseYellow, Horse.HORSE_YELLOW);
		mJLabelToHorseMap.put(mHorseRed, Horse.HORSE_RED);
		mJLabelToHorseMap.put(mHorseGreen, Horse.HORSE_GREEN);
		mJLabelToHorseMap.put(mHorseBlue, Horse.HORSE_BLUE);
		mJLabelToHorseMap.put(mHorseBlack, Horse.HORSE_BLACK);

		mJLabelToStableMap.put(mStableWhite, Stable.STABLE_WHITE);
		mJLabelToStableMap.put(mStableYellow, Stable.STABLE_YELLOW);
		mJLabelToStableMap.put(mStableRed, Stable.STABLE_RED);
		mJLabelToStableMap.put(mStableGreen, Stable.STABLE_GREEN);
		mJLabelToStableMap.put(mStableBlue, Stable.STABLE_BLUE);
		mJLabelToStableMap.put(mStableBlack, Stable.STABLE_BLACK);
	}

	/**
	 * Called to place all horses at the starting square, in a vertical column
	 * @param pJFrame The JFrame of the application
	 */
	public void setHorsePosition(JFrame pJFrame) {
		mJFrame = pJFrame;
		List<JLabel> horse = loadHorses();
		int heightOffset = 0;
		for(JLabel actualHorse : horse) {
			actualHorse.setBounds(X_FIRST_HORSE_ON_BOARD, Y_FIRST_HORSE_ON_BOARD + heightOffset, actualHorse.getPreferredSize().width, actualHorse.getPreferredSize().height);
			pJFrame.add(actualHorse);
			heightOffset += HORSE_VERTICAL_DISTANCE;
		}
	}

	/**
	 * Called to update the position of a horse of the screen
	 * @param pHorse The horse that will be updated
	 * @param pMovement The position of the horse in number of squares after the starting line
	 */
	public void updateHorsePosition(Horse pHorse, int pMovement) {
		for(Map.Entry<JLabel, Horse> entrySetFromMap : mJLabelToHorseMap.entrySet()) {
			JLabel horseJLabel = entrySetFromMap.getKey();
			Horse horseFromMap = entrySetFromMap.getValue();
			if(horseFromMap.equals(pHorse)) {
				horseJLabel.setLocation(X_FIRST_HORSE_ON_BOARD + (SQUARE_DISTANCE_TO_NEXT_SQUARE * pMovement), horseJLabel.getBounds().y);
			}
		}
	}

	/**
	 * Called when a horse has passed over the finish line
	 * @param pHorse The horse that has finished the race
	 * @param pPosition The position of arrival
	 */
	public void showPositionOfArrival(Horse pHorse, int pPosition) {
		for(Map.Entry<JLabel, Horse> jlabelHorse : mJLabelToHorseMap.entrySet()) {
			if(jlabelHorse.getValue().equals(pHorse)) {
				JLabel label = mPositionToJLableMap.get(pPosition);
				mJFrame.add(label);
				label.setBounds(X_FIRST_HORSE_ON_BOARD, jlabelHorse.getKey().getBounds().y, label.getPreferredSize().width, label.getPreferredSize().height);
				label.setVisible(true);
			}
		}
	}

	/**
	 * Removes all the labels that show the arrival position of the horses
	 */
	public void removeAllArrivalLabels() {
		for(Map.Entry<Integer, JLabel> mapEntry : mPositionToJLableMap.entrySet()) {
			mapEntry.getValue().setVisible(false);
		}
	}

	/**
	 * @param pHorse The horse whose ordinate is wanted
	 * @return The ordinate of the JLabel of the horse
	 */
	public int getHorseOrdinate(Horse pHorse) {
		for(Map.Entry<JLabel, Horse> entrySet : mJLabelToHorseMap.entrySet()) {
			if(entrySet.getValue().equals(pHorse)) {
				return entrySet.getKey().getLocation().y;
			}
		}
		throw new IllegalAccessError("Horse not found");
	}

	private Map<Integer, JLabel> createPositionJLabelMap() {
		Map<Integer, JLabel> result = new HashMap<Integer, JLabel>();
		result.put(ONE, new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.ARRIVAL1)));
		result.put(TWO, new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.ARRIVAL2)));
		result.put(THREE, new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.ARRIVAL3)));
		result.put(FOUR, new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.ARRIVAL4)));
		result.put(FIVE, new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.ARRIVAL5)));
		result.put(SIX, new JLabel(ResourcesUtils.loadImageIcon(ResourcesUtils.ARRIVAL6)));
		return Collections.unmodifiableMap(result);
	}

	private List<JLabel> loadHorses() {
		List<JLabel> horses = new LinkedList<JLabel>();
		horses.add(mHorseWhite);
		horses.add(mHorseYellow);
		horses.add(mHorseRed);
		horses.add(mHorseGreen);
		horses.add(mHorseBlue);
		horses.add(mHorseBlack);
		return horses;
	}
}