package it.polimi.provaFinale2013.graphic;

import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco;
import it.polimi.provaFinale2013.horsefever.tavolo.TavoloDiGioco;
import it.polimi.provaFinale2013.view.IViewer;

public class MainGUI {

	public static void main(String[] args) {
		ITavoloDiGioco<IViewer> tavolo = new TavoloDiGioco();
		IViewer viewer = new GUI(tavolo);
		tavolo.registerListener(viewer);
		viewer.startPlayerRegistration();
	}
}