package it.polimi.provaFinale2013.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PlayerCardUtils {

	private static final Map<String, Integer> mPlayerCardNameToInitialAmountOfMoneyMap = createPlayerMoneyMap();
	private static final Map<String, Integer> mPlayerCardNameToInitialQuotationOfAssociatedStableMap = createPlayerPvMap();

	public static int getInitialAmountOfMoney(String pPlayerCardName) {
		Integer initialMoney = mPlayerCardNameToInitialAmountOfMoneyMap.get(pPlayerCardName);
		if(initialMoney == null) {
			throw new IllegalArgumentException(pPlayerCardName + " is not the name of a player card");
		}
		return initialMoney;
	}

	public static int getInitialQuotationOfAssociatedStable(String pPlayerCardName) {
		Integer associatedStable = mPlayerCardNameToInitialQuotationOfAssociatedStableMap.get(pPlayerCardName);
		if(associatedStable == null) {
			throw new IllegalArgumentException(pPlayerCardName + " is not the name of a player card");
		}
		return associatedStable;
	}

	private static Map<String, Integer> createPlayerMoneyMap() {
		Map<String, Integer> result = new HashMap<String, Integer>();
		result.put("Cranio Mercanti", 3400);
		result.put("Steve Mc Skull", 3600);
		result.put("Viktor Von Schadel", 3800);
		result.put("Cesan Crane", 4000);
		result.put("Craneo Cervantes", 4200);
		result.put("Sigvard Skalle", 4400);
		return Collections.unmodifiableMap(result);
	}

	private static Map<String, Integer> createPlayerPvMap() {
		Map<String, Integer> result = new HashMap<String, Integer>();
		result.put("Cranio Mercanti", 2);
		result.put("Steve Mc Skull", 3);
		result.put("Viktor Von Schadel", 4);
		result.put("Cesan Crane", 5);
		result.put("Craneo Cervantes", 6);
		result.put("Sigvard Skalle", 7);
		return Collections.unmodifiableMap(result);
	}
}
