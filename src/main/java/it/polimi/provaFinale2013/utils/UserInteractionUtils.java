package it.polimi.provaFinale2013.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

public class UserInteractionUtils {

	/**
	 * Allow to make the user choose between two possibilities
	 * @param pReader The reader from which to read
	 * @param pMessage The message to show
	 * @param pPositiveAnswer The answer that the user will give to make the method return true
	 * @param pNegativeAnswer The answer that the user will give to make the method return false
	 * @param pErrorMessage The error message
	 * @param pLowerCaseTheAnswers Whether check to ignore capital cases in answers
	 * @return The choice of the user
	 * @throws IOException
	 */
	public static boolean readChoice(BufferedReader pReader, String pMessage, String pPositiveAnswer, String pNegativeAnswer, String pErrorMessage, boolean pLowerCaseTheAnswers) throws IOException {
		Log.user(pMessage);
		String answer = pReader.readLine();
		if(answer == null) {
			Log.user(pErrorMessage);
			return readChoice(pReader, pMessage, pPositiveAnswer, pNegativeAnswer, pErrorMessage, pLowerCaseTheAnswers);
		} else {
			if(pLowerCaseTheAnswers) {
				answer = answer.trim().toLowerCase();
			}
			if(answer.equals(pPositiveAnswer)) {
				return true;
			} else if(answer.equals(pNegativeAnswer)) {
				return false;
			} else {
				Log.user(pErrorMessage);
				return readChoice(pReader, pMessage, pPositiveAnswer, pNegativeAnswer, pErrorMessage, pLowerCaseTheAnswers);
			}
		}
	}

	/**
	 * Allow to make the user choose between multiple possibilities
	 * @param pReader The reader from which to read
	 * @param pMessage The message to show
	 * @param pChoices The map against which are tested the answers of the user.
	 * @param pErrorMessage The error message
	 * @param pLowerCaseTheAnswers Whether check to ignore capital cases in answers
	 * @return The value in the map for which the user answer match the key
	 * @throws IOException
	 */
	public static <T> T readMultipleChoice(BufferedReader pReader, String pMessage, Map<String, T> pChoices, String pErrorMessage, boolean pLowerCaseTheAnswers) throws IOException {
		Log.user(pMessage);
		String answer = pReader.readLine();
		if(answer == null) {
			Log.user(pErrorMessage);
			return readMultipleChoice(pReader, pMessage, pChoices, pErrorMessage, pLowerCaseTheAnswers);
		} else {
			if(pLowerCaseTheAnswers) {
				answer = answer.trim().toLowerCase();
			}
			T choice = pChoices.get(answer);
			if(choice != null) {
				return choice;
			} else {
				Log.user(pErrorMessage);
				return readMultipleChoice(pReader, pMessage, pChoices, pErrorMessage, pLowerCaseTheAnswers);
			}
		}
	}

	/**
	 * Allow to make the user input an integer
	 * @param pReader The reader from which to read
	 * @param pMessage The message to show
	 * @param pMinValue The minimum value
	 * @param pMaxValue The maximum value
	 * @param pErrorMessage The error message
	 * @return The value choose by the user
	 * @throws IOException
	 */
	public static int readInt(BufferedReader pReader, String pMessage, int pMinValue, int pMaxValue, String pErrorMessage) throws IOException {
		Log.user(pMessage);
		String answer = pReader.readLine();
		if(answer == null) {
			Log.user(pErrorMessage);
			return readInt(pReader, pMessage, pMinValue, pMaxValue, pErrorMessage);
		} else {
			try {
				Log.d("Before trim: " + answer);
				String trimmed = answer.trim();
				Log.d("After trim: " + trimmed);
				int value = Integer.parseInt(trimmed);
				if(pMinValue <= value && value <= pMaxValue) {
					return value;
				} else {
					Log.user(pErrorMessage);
					return readInt(pReader, pMessage, pMinValue, pMaxValue, pErrorMessage);
				}
			} catch (NumberFormatException e) {
				Log.user(pErrorMessage);
				return readInt(pReader, pMessage, pMinValue, pMaxValue, pErrorMessage);
			}
		}
	}

	/**
	 * Allow to make the user input a string
	 * @param pReader The reader from which to read
	 * @param pMessage The message to show
	 * @param pErrorMessage The error message
	 * @param pIsNullAnswerValid Whether to return null if the answer is null
	 * @param pReturnEmptyStringOnNullAnswer Whether to return an empty string if the answer is null
	 * @return The inputted string
	 * @throws IOException
	 */
	public static String readLine(BufferedReader pReader, String pMessage, String pErrorMessage, boolean pIsNullAnswerValid, boolean pReturnEmptyStringOnNullAnswer) throws IOException {
		Log.user(pMessage);
		String answer = pReader.readLine();
		if(answer == null) {
			if(pIsNullAnswerValid) {
				return answer;
			} else {
				if(pReturnEmptyStringOnNullAnswer) {
					return "";
				} else {
					Log.user(pErrorMessage);
					return readLine(pReader, pMessage, pErrorMessage, pIsNullAnswerValid, pReturnEmptyStringOnNullAnswer);
				}
			}
		} else {
			return answer.trim();
		}
	}
}
