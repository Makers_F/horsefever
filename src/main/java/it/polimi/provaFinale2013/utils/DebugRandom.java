package it.polimi.provaFinale2013.utils;

import java.util.Random;

public class DebugRandom extends Random {
	private static final long serialVersionUID = -2679628215899572281L;
	private static final int[] integers = new int[]{0,1,2,3,4,5,0,1,    // sprint in the first debug match
													0,1,2,3,4,5,0,1,    // sprint in the second debug match
													0,1,2,3,4,5,0,1,2,3 // i let you guess..
													};
	private boolean mNextBoolean;
	private int index = 0;

	DebugRandom() {
		mNextBoolean = false;
	}

	public void reset() {
		index = 0;
		mNextBoolean = false;
	}

	@Override
	public boolean nextBoolean() {
		boolean returnBolean = mNextBoolean;
		mNextBoolean = !mNextBoolean;
		return returnBolean;
	}

	@Override
	public int nextInt(int n){
		int currentIndex = index;
		if(index + 1 < integers.length){
			index++;
		} else {
			index = 0;
		}
		return MathUtils.clamp(integers[currentIndex], 0, n - 1);
	}
}
