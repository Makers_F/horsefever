package it.polimi.provaFinale2013.utils;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;

public class EnumUtils {

	public static <T extends Enum<T>> int getIndex(T value) {
		int i = 0;
		for(Enum<?> val : value.getClass().getEnumConstants()) {
			if(val == value) {
				return i;
			} else {
				i++;
			}
		}
		throw new IllegalArgumentException("Impossibile trovare l'indice dell'enum.");
	}

	public static <T extends Enum<T>> T getEnumFromIndex(int index, Class<T> enumType) {
		return enumType.getEnumConstants()[index];
	}

	public static Horse getHorsefromStable(Stable pStable) {
		return Horse.getHorseFromColor(pStable.getColor());
	}

	public static Stable getStableFromHorse(Horse pHorse) {
		return Stable.getStableFromColor(pHorse.getColor());
	}
}
