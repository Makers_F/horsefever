package it.polimi.provaFinale2013.utils;

public class MathUtils {
	public static int clamp(int a, int min, int max) {
		return Math.max(Math.min(a, max), min);
	}
}
