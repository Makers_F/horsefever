package it.polimi.provaFinale2013.utils;

public class DebugUtils {

	private static boolean IS_DEBUG = false;

	public static boolean isDebug() {
		return IS_DEBUG;
	}

	public static void setDebug(boolean pDebug) {
		IS_DEBUG = pDebug;
	}
}
