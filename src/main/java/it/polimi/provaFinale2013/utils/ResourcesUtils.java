package it.polimi.provaFinale2013.utils;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

public class ResourcesUtils {

	public static final String TABELLONE = "tabellone";
	public static final String MOVEMENT_CARD_XML = "movementCardXml";
	public static final String ACTION_CARD_XML = "actionCardXml";
	public static final String CLESSIDRA = "clessidra";
	public static final String CAVALLO_BIANCO = "cavalloBianco";
	public static final String CAVALLO_BLU = "cavalloBlu";
	public static final String CAVALLO_GIALLO = "cavalloGiallo";
	public static final String CAVALLO_NERO = "cavalloNero";
	public static final String CAVALLO_ROSSO = "cavalloRosso";
	public static final String CAVALLO_VERDE = "cavalloVerde";
	public static final String CARTA_SCUDERIA_BIANCO = "cartaScuderiaBianco";
	public static final String CARTA_SCUDERIA_BLU = "cartaScuderiaBlu";
	public static final String CARTA_SCUDERIA_GIALLO = "cartaScuderiaGiallo";
	public static final String CARTA_SCUDERIA_NERO = "cartaScuderiaNero";
	public static final String CARTA_SCUDERIA_ROSSO = "cartaScuderiaRosso";
	public static final String CARTA_SCUDERIA_VERDE = "cartaScuderiaVerde";
	public static final String QUOTAZIONE_BIANCO = "SegnalinoQuotazioniScuderiaBianca";
	public static final String QUOTAZIONE_BLU = "SegnalinoQuotazioniScuderiaBlu";
	public static final String QUOTAZIONE_GIALLO = "SegnalinoQuotazioniScuderiaGialla";
	public static final String QUOTAZIONE_NERO = "SegnalinoQuotazioniScuderiaNera";
	public static final String QUOTAZIONE_ROSSO = "SegnalinoQuotazioniScuderiaRossa";
	public static final String QUOTAZIONE_VERDE = "SegnalinoQuotazioniScuderiaVerde";
	public static final String CARTELLO = "cartello";
	public static final String CARTA_0 = "Magna Velocitas";
	public static final String CARTA_1 = "Fortuna Benevola";
	public static final String CARTA_2 = "Flagellum Fulguris";
	public static final String CARTA_3 = "Herba Magica";
	public static final String CARTA_4 = "In Igni Veritas";
	public static final String CARTA_5 = "Fustis et Radix";
	public static final String CARTA_6 = "Vigor Ferreum";
	public static final String CARTA_7 = "Globus Obscurus";
	public static final String CARTA_8 = "Aqua Putrida";
	public static final String CARTA_9 = "Serum Maleficum";
	public static final String CARTA_10 = "Venenum Veneficum";
	public static final String CARTA_11 = "Mala Tempora";
	public static final String CARTA_12 = "XIII";
	public static final String CARTA_13 = "Felix Infernalis";
	public static final String CARTA_14 = "Alfio Allibratore";
	public static final String CARTA_15 = "Fritz Finden";
	public static final String CARTA_18 = "Steven Sting";
	public static final String CARTA_19 = "Rochelle Recherche";
	public static final String CARTA_71 = "Cranio Mercanti";
	public static final String CARTA_72 = "Steve Mc Skull";
	public static final String CARTA_73 = "Victor Von Schadel";
	public static final String CARTA_74 = "Cesar Crane";
	public static final String CARTA_75 = "Sigvard Skalle";
	public static final String CARTA_76 = "Craneo Cervantes";
	public static final String CM222210 = "Carta Movimento 222210";
	public static final String CM222211 = "Carta Movimento 222211";
	public static final String CM222233 = "Carta Movimento 222233";
	public static final String CM222323 = "Carta Movimento 222323";
	public static final String CM222332 = "Carta Movimento 222332";
	public static final String CM223122 = "Carta Movimento 223122";
	public static final String CM223223 = "Carta Movimento 223223";
	public static final String CM223232 = "Carta Movimento 223232";
	public static final String CM223322 = "Carta Movimento 223322";
	public static final String CM232223 = "Carta Movimento 232223";
	public static final String CM232232 = "Carta Movimento 232232";
	public static final String CM233222 = "Carta Movimento 233222";
	public static final String CM242202 = "Carta Movimento 242202";
	public static final String CM322221 = "Carta Movimento 322221";
	public static final String CM322223 = "Carta Movimento 322223";
	public static final String CM322232 = "Carta Movimento 322232";
	public static final String CM322322 = "Carta Movimento 322322";
	public static final String CM323222 = "Carta Movimento 323222";
	public static final String CM332222 = "Carta Movimento 332222";
	public static final String CM422220 = "Carta Movimento 422220";
	public static final String CM432222 = "Carta Movimento 432222";
	public static final String CMRETRO = "Retro mazzo movimento";
	public static final String ARRIVAL1 = "Arrivo 1";
	public static final String ARRIVAL2 = "Arrivo 2";
	public static final String ARRIVAL3 = "Arrivo 3";
	public static final String ARRIVAL4 = "Arrivo 4";
	public static final String ARRIVAL5 = "Arrivo 5";
	public static final String ARRIVAL6 = "Arrivo 6";
	public static final String RETRO_CARTA_AZIONE = "Retro Carta Azione";
	public static final String ICONA = "Icona";
	public static final String SPRINT_BIANCO = "Sprint bianco";
	public static final String SPRINT_GIALLO = "Sprint giallo";
	public static final String SPRINT_ROSSO = "Sprint rosso";
	public static final String SPRINT_VERDE = "Sprint verde";
	public static final String SPRINT_BLU = "Sprint blu";
	public static final String SPRINT_NERO = "Sprint nero";
	
	private final static Map<String, String> resourcesPaths = new HashMap<String, String>();

	static {
		resourcesPaths.put(MOVEMENT_CARD_XML, "xml/carteMovimento.xml");
		resourcesPaths.put(ACTION_CARD_XML, "xml/carteAzione.xml");

		resourcesPaths.put(TABELLONE, "images/nuovoTabellone.png");
		resourcesPaths.put(CLESSIDRA, "images/segnalino_clessidra.png");
		resourcesPaths.put(CAVALLO_BIANCO, "images/cav_bianco.png");
		resourcesPaths.put(CAVALLO_GIALLO, "images/cav_giallo.png");
		resourcesPaths.put(CAVALLO_ROSSO, "images/cav_rosso.png");
		resourcesPaths.put(CAVALLO_VERDE, "images/cav_verde.png");
		resourcesPaths.put(CAVALLO_BLU, "images/cav_blu.png");
		resourcesPaths.put(CAVALLO_NERO, "images/cav_nero.png");
		resourcesPaths.put(CARTA_SCUDERIA_BLU, "images/cartaScuderiaBlu.jpg");
		resourcesPaths.put(CARTA_SCUDERIA_BIANCO, "images/cartaScuderiaBianco.jpg");
		resourcesPaths.put(CARTA_SCUDERIA_GIALLO, "images/cartaScuderiaGiallo.jpg");
		resourcesPaths.put(CARTA_SCUDERIA_NERO, "images/cartaScuderiaNera.jpg");
		resourcesPaths.put(CARTA_SCUDERIA_ROSSO, "images/cartaScuderiaRosso.jpg");
		resourcesPaths.put(CARTA_SCUDERIA_VERDE, "images/cartaScuderiaVerde.jpg");
		resourcesPaths.put(QUOTAZIONE_BIANCO, "images/SegnalinoQuotazioniScuderiaBianca.jpg");
		resourcesPaths.put(QUOTAZIONE_BLU, "images/SegnalinoQuotazioniScuderiaBlu.jpg");
		resourcesPaths.put(QUOTAZIONE_GIALLO, "images/SegnalinoQuotazioniScuderiaGialla.jpg");
		resourcesPaths.put(QUOTAZIONE_NERO, "images/SegnalinoQuotazioniScuderiaNera.jpg");
		resourcesPaths.put(QUOTAZIONE_ROSSO, "images/SegnalinoQuotazioniScuderiaRossa.jpg");
		resourcesPaths.put(QUOTAZIONE_VERDE, "images/SegnalinoQuotazioniScuderiaVerde.jpg");
		resourcesPaths.put(CARTELLO, "images/cartello.png");
		resourcesPaths.put(CARTA_0, "images/horseFever-0.jpg");
		resourcesPaths.put(CARTA_1, "images/horseFever-1.jpg");
		resourcesPaths.put(CARTA_2, "images/horseFever-2.jpg");
		resourcesPaths.put(CARTA_3, "images/horseFever-3.jpg");
		resourcesPaths.put(CARTA_4, "images/horseFever-4.jpg");
		resourcesPaths.put(CARTA_5, "images/horseFever-5.jpg");
		resourcesPaths.put(CARTA_6, "images/horseFever-6.jpg");
		resourcesPaths.put(CARTA_7, "images/horseFever-7.jpg");
		resourcesPaths.put(CARTA_8, "images/horseFever-8.jpg");
		resourcesPaths.put(CARTA_9, "images/horseFever-9.jpg");
		resourcesPaths.put(CARTA_10, "images/horseFever-10.jpg");
		resourcesPaths.put(CARTA_11, "images/horseFever-11.jpg");
		resourcesPaths.put(CARTA_12, "images/horseFever-12.jpg");
		resourcesPaths.put(CARTA_13, "images/horseFever-13.jpg");
		resourcesPaths.put(CARTA_14, "images/horseFever-14.jpg");
		resourcesPaths.put(CARTA_15, "images/horseFever-15.jpg");
		resourcesPaths.put(CARTA_18, "images/horseFever-18.jpg");
		resourcesPaths.put(CARTA_19, "images/horseFever-19.jpg");
		resourcesPaths.put(CARTA_19, "images/horseFever-19.jpg");
		resourcesPaths.put(CARTA_19, "images/horseFever-19.jpg");
		resourcesPaths.put(CARTA_19, "images/horseFever-19.jpg");
		resourcesPaths.put(CARTA_19, "images/horseFever-19.jpg");
		resourcesPaths.put(CARTA_19, "images/horseFever-19.jpg");
		resourcesPaths.put(CARTA_19, "images/horseFever-19.jpg");
		resourcesPaths.put(CM222210, "images/222210.png");
		resourcesPaths.put(CM222211, "images/222211.png");
		resourcesPaths.put(CM222233, "images/222233.png");
		resourcesPaths.put(CM222323, "images/222323.png");
		resourcesPaths.put(CM222332, "images/222332.png");
		resourcesPaths.put(CM223122, "images/223122.png");
		resourcesPaths.put(CM223223, "images/223223.png");
		resourcesPaths.put(CM223232, "images/223232.png");
		resourcesPaths.put(CM223322, "images/223322.png");
		resourcesPaths.put(CM232223, "images/232223.png");
		resourcesPaths.put(CM232232, "images/232232.png");
		resourcesPaths.put(CM233222, "images/233222.png");
		resourcesPaths.put(CM242202, "images/242202.png");
		resourcesPaths.put(CM322221, "images/322221.png");
		resourcesPaths.put(CM322223, "images/322223.png");
		resourcesPaths.put(CM322232, "images/322232.png");
		resourcesPaths.put(CM322322, "images/322322.png");
		resourcesPaths.put(CM323222, "images/323222.png");
		resourcesPaths.put(CM332222, "images/332222.png");
		resourcesPaths.put(CM422220, "images/422220.png");
		resourcesPaths.put(CM432222, "images/432222.png");
		resourcesPaths.put(CMRETRO, "images/CM_RETRO.png");
		resourcesPaths.put(ARRIVAL1, "images/1st.png");
		resourcesPaths.put(ARRIVAL2, "images/2nd.png");
		resourcesPaths.put(ARRIVAL3, "images/3rd.png");
		resourcesPaths.put(ARRIVAL4, "images/4th.png");
		resourcesPaths.put(ARRIVAL5, "images/5th.png");
		resourcesPaths.put(ARRIVAL6, "images/6th.png");
		resourcesPaths.put(RETRO_CARTA_AZIONE, "images/retroCartaAzione.png");
		resourcesPaths.put(ICONA, "images/icona.png");
		resourcesPaths.put(SPRINT_BIANCO, "images/sprint_bianco.png");
		resourcesPaths.put(SPRINT_GIALLO, "images/sprint_giallo.png");
		resourcesPaths.put(SPRINT_ROSSO, "images/sprint_rosso.png");
		resourcesPaths.put(SPRINT_VERDE, "images/sprint_verde.png");
		resourcesPaths.put(SPRINT_NERO, "images/sprint_nero.png");
		resourcesPaths.put(SPRINT_BLU, "images/sprint_blu.png");
	}

	public static ImageIcon loadImageIcon(String pResourceName) {
		return new ImageIcon(getResourceURL(pResourceName));
	}

	public static URL getResourceURL(String pResourceName) {
		return ResourcesUtils.class.getClassLoader().getResource(resourcesPaths.get(pResourceName));
	}

	public static InputStream getResourceInputStream(String pResourceName) {
		return ResourcesUtils.class.getClassLoader().getResourceAsStream(resourcesPaths.get(pResourceName));
	}
}