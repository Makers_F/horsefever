package it.polimi.provaFinale2013.utils.parcel;

import java.io.IOException;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParcel;
import it.polimi.provaFinale2013.communication.marshalling.parcel.ISymmetricParceler;

public class IntegerParceler implements ISymmetricParceler<Integer> {

	public IntegerParceler() {
		ParcelUtils.registerParceller(this);
	}

	@Override
	public void writeToParcel(Integer contenuto, IParcel pacchetto) throws IOException {
		pacchetto.writeInt(contenuto.intValue());
	}

	@Override
	public Integer createFromParcel(IParcel pacchetto) throws IOException {
		return Integer.valueOf(pacchetto.readInt());
	}

	@Override
	public String getUniqueClassID() {
		return "Integer";
	}

}
