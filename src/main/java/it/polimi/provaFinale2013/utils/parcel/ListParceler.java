package it.polimi.provaFinale2013.utils.parcel;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParcel;
import it.polimi.provaFinale2013.communication.marshalling.parcel.ISymmetricParceler;

public class ListParceler<T> implements ISymmetricParceler<List<T>> {

	private static final Map<String, ListParceler<?>> instancedLists = new HashMap<String, ListParceler<?>>();

	private final ISymmetricParceler<T> mElementParceler;

	public static <T> ListParceler<T> getListParceller(ISymmetricParceler<T> pElementParceler) {
		String typeCode = getUniqueClassIDFromParceler(pElementParceler);
		@SuppressWarnings("unchecked")
		ListParceler<T> requestedList = (ListParceler<T>) instancedLists.get(typeCode);
		if(requestedList != null) {
			return requestedList;
		} else {
			requestedList = new ListParceler<T>(pElementParceler);
			instancedLists.put(typeCode, requestedList);
			return requestedList;
		}
	}

	private ListParceler(ISymmetricParceler<T> pElementParceler) {
		mElementParceler = pElementParceler;
		ParcelUtils.registerParceller(this);
	}

	@Override
	public void writeToParcel(List<T> contenuto, IParcel pacchetto) throws IOException {
		pacchetto.writeInt(contenuto.size());
		for(T element : contenuto) {
			pacchetto.writeObject(element, mElementParceler);
		}
	}

	@Override
	public List<T> createFromParcel(IParcel pacchetto) throws IOException {
		List<T> list = new LinkedList<T>();
		int size = pacchetto.readInt();
		for(int i = 0; i < size; i++) {
			list.add(pacchetto.readObject(mElementParceler));
		}
		return list;
	}

	@Override
	public String getUniqueClassID() {
		return getUniqueClassIDFromParceler(mElementParceler);
	}

	private static String getUniqueClassIDFromParceler(ISymmetricParceler<?> pElementParceler) {
		return "List<" + pElementParceler.getUniqueClassID() + ">";
	}
}
