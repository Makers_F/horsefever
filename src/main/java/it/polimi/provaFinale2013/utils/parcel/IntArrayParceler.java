package it.polimi.provaFinale2013.utils.parcel;

import java.io.IOException;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParcel;
import it.polimi.provaFinale2013.communication.marshalling.parcel.ISymmetricParceler;

public class IntArrayParceler implements ISymmetricParceler<int[]> {

	public IntArrayParceler() {
		ParcelUtils.registerParceller(this);
	}

	@Override
	public void writeToParcel(int[] contenuto, IParcel pacchetto)
			throws IOException {
		pacchetto.writeInt(contenuto.length);
		for(int i : contenuto) {
			pacchetto.writeInt(i);
		}
	}

	@Override
	public int[] createFromParcel(IParcel pacchetto) throws IOException {
		int length = pacchetto.readInt();
		int[] intArray = new int[length];
		for(int i = 0; i < length; i++) {
			intArray[i] = pacchetto.readInt();
		}
		return intArray;
	}

	@Override
	public String getUniqueClassID() {
		return "int[]";
	}

}
