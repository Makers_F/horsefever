package it.polimi.provaFinale2013.utils.parcel;

import java.io.IOException;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParcel;
import it.polimi.provaFinale2013.communication.marshalling.parcel.ISymmetricParceler;
import it.polimi.provaFinale2013.utils.EnumUtils;

public class EnumParceler<T extends Enum<T>> implements ISymmetricParceler<T>{

	private final Class<T> mEnum;

	public EnumParceler(Class<T> pEnum) {
		mEnum = pEnum;
		ParcelUtils.registerParceller(this);
	}

	@Override
	public void writeToParcel(T contenuto, IParcel pacchetto) throws IOException {
		pacchetto.writeInt(EnumUtils.getIndex(contenuto));
	}

	@Override
	public T createFromParcel(IParcel pacchetto) throws IOException {
		return EnumUtils.getEnumFromIndex(pacchetto.readInt(), mEnum);
	}

	@Override
	public String getUniqueClassID() {
		return mEnum.getName(); //TODO find a more concise way. Maybe keep only the class name without the package
	}

}
