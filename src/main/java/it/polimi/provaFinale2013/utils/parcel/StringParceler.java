package it.polimi.provaFinale2013.utils.parcel;

import java.io.IOException;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParcel;
import it.polimi.provaFinale2013.communication.marshalling.parcel.ISymmetricParceler;

public class StringParceler implements ISymmetricParceler<String> {

	public StringParceler() {
		ParcelUtils.registerParceller(this);
	}

	@Override
	public void writeToParcel(String contenuto, IParcel pacchetto) throws IOException {
		pacchetto.writeUTF(contenuto);
	}

	@Override
	public String createFromParcel(IParcel pacchetto) throws IOException {
		return pacchetto.readUTF();
	}

	@Override
	public String getUniqueClassID() {
		return "String";
	}

}
