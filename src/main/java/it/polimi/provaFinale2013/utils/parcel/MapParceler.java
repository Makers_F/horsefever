package it.polimi.provaFinale2013.utils.parcel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParcel;
import it.polimi.provaFinale2013.communication.marshalling.parcel.ISymmetricParceler;

public class MapParceler<K, V> implements ISymmetricParceler<Map<K, V>>{

	private static final Map<String, MapParceler<?, ?>> instancedMaps = new HashMap<String, MapParceler<?,?>>();

	private final ISymmetricParceler<K> mKeyParceler;
	private final ISymmetricParceler<V> mValueParceler;

	public static <K, V> MapParceler<K, V> getMapParceller(ISymmetricParceler<K> pKeyParceler, ISymmetricParceler<V> pValueParceler) {
		String typeCode = getUniqueClassIDFromParcelers(pKeyParceler, pValueParceler);
		@SuppressWarnings("unchecked")
		MapParceler<K, V> requestedMap = (MapParceler<K, V>) instancedMaps.get(typeCode);
		if(requestedMap != null) {
			return requestedMap;
		} else {
			requestedMap = new MapParceler<K, V>(pKeyParceler, pValueParceler);
			instancedMaps.put(typeCode, requestedMap);
			return requestedMap;
		}
	}

	private MapParceler(ISymmetricParceler<K> pKeyParceler, ISymmetricParceler<V> pValueParceler) {
		mKeyParceler = pKeyParceler;
		mValueParceler = pValueParceler;
		ParcelUtils.registerParceller(this);
	}

	@Override
	public void writeToParcel(Map<K, V> contenuto, IParcel pacchetto) throws IOException {
		pacchetto.writeInt(contenuto.size());
		for(Map.Entry<K, V> entry : contenuto.entrySet()) {
			pacchetto.writeObject(entry.getKey(), mKeyParceler);
			pacchetto.writeObject(entry.getValue(), mValueParceler);
		}
	}

	@Override
	public Map<K, V> createFromParcel(IParcel pacchetto) throws IOException {
		Map<K, V> readMap = new HashMap<K, V>();
		int size = pacchetto.readInt();
		for(int i = 0; i < size; i++) {
			K key = pacchetto.readObject(mKeyParceler);
			V value = pacchetto.readObject(mValueParceler);
			readMap.put(key, value);
		}
		return readMap;
	}

	@Override
	public String getUniqueClassID() {
		return getUniqueClassIDFromParcelers(mKeyParceler, mValueParceler);
	}

	private static String getUniqueClassIDFromParcelers(ISymmetricParceler<?> pKeyParceler, ISymmetricParceler<?> pValueParceler) {
		return "Map<" + pKeyParceler.getUniqueClassID() + "," + pValueParceler.getUniqueClassID() + ">";
	}

}
