package it.polimi.provaFinale2013.utils.parcel;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParceler;

import java.util.HashMap;
import java.util.Map;

public class ParcelUtils {

	private static final Map<String, IParceler<?, ?>> parcellers = new HashMap<String, IParceler<?,?>>();
	public static final StringParceler STRING_PARCELER = new StringParceler();
	public static final IntegerParceler INTEGER_PARCELER = new IntegerParceler();
	public static final MapParceler<String, Integer> STRING_INTEGER_MAP_PARCELER = MapParceler.getMapParceller(STRING_PARCELER, INTEGER_PARCELER);
	public static final IntArrayParceler INT_ARRAY_PARCELER = new IntArrayParceler();
	private static final String NULL_UNIQUE_CLASS_ID = "null";
	private static final String ARRAY_PREFIX = "[]";

	public synchronized static void registerParceller(IParceler<?, ?> pParceler) {
		String ID = pParceler.getUniqueClassID();
		if(ParcelUtils.isArrayCode(ID)) {
			throw new IllegalArgumentException("A parceller can not use an array code.");
		}
		if(ParcelUtils.isNullCode(ID)) {
			throw new IllegalArgumentException("A parceller can not use a null code.");
		}
		IParceler<?, ?> previous = parcellers.put(ID, pParceler);
		if(previous != null) {
			throw new IllegalArgumentException("Already exists a parceller with the same ID code.");
		}
	}

	public static boolean isNullCode(String pCode) {
		return pCode.equals(NULL_UNIQUE_CLASS_ID);
	}

	public static String getNullCode() {
		return NULL_UNIQUE_CLASS_ID;
	}

	public static String makeArrayCode(String pCode) {
		return ARRAY_PREFIX + pCode;
	}

	public static boolean isArrayCode(String pCode) {
		return pCode.startsWith(ARRAY_PREFIX);
	}

	public static String getClassCodeFromArrayCode(String pCode) {
		if(isArrayCode(pCode)) {
			return pCode.substring(ARRAY_PREFIX.length());
		} else {
			throw new IllegalArgumentException("The code " + pCode + " is not an array code.");
		}
	}

	public static String makeSureNotArrayCode(String pCode) {
		if(isArrayCode(pCode)) {
			return getClassCodeFromArrayCode(pCode);
		} else {
			return pCode;
		}
	}
}