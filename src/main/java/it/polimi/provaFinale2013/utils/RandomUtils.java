package it.polimi.provaFinale2013.utils;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomUtils {

	private static Random RND;

	public static synchronized Random getRandom() {
		if(RND == null) {
			RND = DebugUtils.isDebug() ? new DebugRandom() : new Random();
		}
		return RND;
	}

	public static void shuffle(List<?> pList) {
		if(DebugUtils.isDebug()) {
			return;
		}
		Collections.shuffle(pList);
	}
}
