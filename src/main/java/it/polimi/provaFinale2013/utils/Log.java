package it.polimi.provaFinale2013.utils;

public class Log {

	private static Log log = new Log(DebugLevel.LOG_DEBUG, false);

	/**
	 * Sets the DebugLevel and the options of the Log
	 * @param pDebugLevel the debug level
	 * @param pWarningAsError true if warnings should raise errors
	 */
	public synchronized static void setLogOptions(DebugLevel pDebugLevel, boolean pWarningAsError) {
		log = new Log(pDebugLevel, pWarningAsError);
	}

	private final DebugLevel dl;
	private final boolean warningsAsErrors;

	private Log(DebugLevel pDL, boolean pWarningsAsErrors) {
		dl = pDL;
		warningsAsErrors = pWarningsAsErrors;
	}

	/**
	 * Throws an error if the DebugLevel of the Log is lower than LOG_ERROR
	 * @param error The error message
	 */
	public static void e(String error) {
		if(log.dl.isErrorLevel()) {
			throw new RuntimeException("ERROR: " + error);
		}
	}

	/**
	 * Throws an error if the DebugLevel of the Log is lower than LOG_ERROR
	 * @param error The throwable that will be bundled with the thrown error
	 */
	public static void e(Throwable error) {
		if(log.dl.isErrorLevel()) {
			throw new RuntimeException(error);
		}
	}

	/**
	 * Throws an error if the DebugLevel of the Log is lower than LOG_ERROR
	 * @param errorMessage The error message
	 * @param error The throwable that will be bundled with the thrown error
	 */
	public static void e(String errorMessage, Throwable error) {
		if(log.dl.isErrorLevel()) {
			throw new RuntimeException(errorMessage, error);
		}
	}

	/**
	 * Prints a warning if the DebugLevel of the Log is lower than LOG_WARNING.
	 * If the option WarningAsError is set, it throws an exception instead
	 * @param warning The warning message
	 */
	public static void w(String warning) {
		if(log.dl.isWarningLevel()) {
			if(log.warningsAsErrors) {
				e(warning);
			} else {
				System.out.println("WARNING: " + warning);
			}
		}
	}

	/**
	 * Prints a debug message if the DebugLevel of the Log is lower than LOG_DEBUG.
	 * @param debug The debug message
	 */
	public static void d(String debug) {
		if(log.dl.isDebugLevel()) {
			System.out.println("DEBUG: " + debug);
		}
	}

	/**
	 * Prints an information message if the DebugLevel of the Log is lower than LOG_INFO.
	 * @param info The info message
	 */
	public static void i(String info) {
		if(log.dl.isInfoLevel()) {
			System.out.println("INFO: " + info);
		}
	}

	/**
	 * Prints an information message if the DebugLevel of the Log is lower than LOG_INFO.
	 * @param module The module that is responsible for the message
	 * @param info The info message
	 */
	public static void i(String module, String info) {
		if(log.dl.isInfoLevel()) {
			System.out.println("INFO by " + module + ": " + info);
		}
	}

	/**
	 * Prints a verbose message if the DebugLevel of the Log is lower than LOG_VERBOSE.
	 * @param verbose The verbose message
	 */
	public static void v(String verbose) {
		if(log.dl.isVerboseLevel()) {
			System.out.println("VERBOSE: " + verbose);
		}
	}

	/**
	 * Always print the message to show to the user
	 * @param user The message to print
	 */
	public static void user(String user) {
		if(log.dl.isUserLevel()) {
			System.out.println(user);
		}
	}

	public static enum DebugLevel {
		LOG_USER(Integer.MAX_VALUE),
		LOG_ERROR(1000),
		LOG_WARNING(900),
		LOG_DEBUG(800),
		LOG_INFO(700),
		LOG_VERBOSE(500),
		LOG_NONE(Integer.MAX_VALUE - 1);

		private final int level;
		private DebugLevel(int pLevel) {
			level = pLevel;
		}

		/**
		 * @return True if the DebugLevel il lower than LOG_ERROR, false otherwise
		 */
		public boolean isErrorLevel() {
			return level <= LOG_ERROR.level;
		}

		/**
		 * @return True if the DebugLevel il lower than LOG_WARNING, false otherwise
		 */
		public boolean isWarningLevel() {
			return level <= LOG_WARNING.level;
		}

		/**
		 * @return True if the DebugLevel il lower than LOG_DEBUG, false otherwise
		 */
		public boolean isDebugLevel() {
			return level <= LOG_DEBUG.level;
		}

		/**
		 * @return True if the DebugLevel il lower than LOG_info, false otherwise
		 */
		public boolean isInfoLevel() {
			return level <= LOG_INFO.level;
		}

		/**
		 * @return True if the DebugLevel il lower than LOG_VERBOSE, false otherwise
		 */
		public boolean isVerboseLevel() {
			return level <= LOG_VERBOSE.level;
		}

		/**
		 * @return Always returns true
		 */
		public boolean isUserLevel() {
			return true;
		}
	}
}
