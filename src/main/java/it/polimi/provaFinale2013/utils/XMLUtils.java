package it.polimi.provaFinale2013.utils;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLUtils {

	public static Element parseAndGetRoot(InputStream pFileToParse) {
		Document doc = null;
		Element root = null;
		try {
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(pFileToParse, "UTF-8");
		} catch (SAXException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (FactoryConfigurationError e) {
			throw new RuntimeException(e);
		}
		root = doc.getDocumentElement();
		root.normalize();
		return root;
	}

	/**
	 * 
	 * @param parent Un nodo.
	 * @return Tutti gli elementi figli del nodo. Se il nodo non ha figli l'array avr� lunghezza 0.
	 */
	public static Element[] elements(Node parent) {
		Element[] result = new Element[parent.getChildNodes().getLength()];
		NodeList nl = parent.getChildNodes();
		for (int i = 0; i < nl.getLength(); i++) {
			if (nl.item(i).getNodeType() == Node.ELEMENT_NODE){
				result[i] = (Element) nl.item(i);
			}
		}
		return result;
	}

	/**
	 * 
	 * @param parent Il nodo parent
	 * @param tag Il nome dei figli
	 * @return Tutti gli elementi figli del nodo con nome tag
	 */
	public static Element[] elements(Node parent, String tag) {
		NodeList nodeList = ((Element) parent).getElementsByTagName(tag);
		Element[] result = new Element[nodeList.getLength()];
		for(int i = 0; i < nodeList.getLength(); i++) {
			result[i] = (Element) nodeList.item(i);
		}
		return result;
	}

	public static Node getFirstNodeInstance(Node parent, String pTagName) {
		return getFirstNodeInstance((Element) parent, pTagName);
	}

	public static Node getFirstNodeInstance(Element parent, String pTagName) {
		return parent.getElementsByTagName(pTagName).item(0);
	}
}
