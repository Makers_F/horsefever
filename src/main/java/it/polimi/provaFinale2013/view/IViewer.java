package it.polimi.provaFinale2013.view;

import java.util.List;
import java.util.Map;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.tavolo.IActionCardListener;
import it.polimi.provaFinale2013.horsefever.tavolo.IBetListener;
import it.polimi.provaFinale2013.horsefever.tavolo.IQuotationListener;
import it.polimi.provaFinale2013.horsefever.tavolo.IRaceListener;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco;

public interface IViewer extends IQuotationListener, IRaceListener, IBetListener, IActionCardListener {

	/**
	 * Called when a player has the opportunity to bet
	 * @param pPlayerName The player that has to bet
	 * @param pMinBet The minimum amount of money the player can bet
	 * @param pMaxBet The maximum amount of money the player can bet
	 * @param pHorseToQuotationMap A map that associates each horse to its stable's quotation
	 * @param isMandatory True if the bet is mandatory, otherwise false
	 * @return True if the user placed a bet, false otherwise
	 */
	boolean onExpectBet(String pPlayerName, int pMinBet, int pMaxBet, Map<Horse, Integer> pHorseToQuotationMap, boolean isMandatory);

	/**
	 * Called when the game expects a player to assign a card to a horse
	 * @param pPlayerName The player that should play the card
	 * @param pAvailableCards The available cards to the player.
	 */
	void onExpectPlacedCard(String pPlayerName, List<String> pAvailableCards);

	/**
	 * Called when a turn is finished
	 */
	void onTurnFinished();

	/**
	 * Called when a player doesn't own enough money to bet, so its PV are removed
	 * @param pPlayerName The player whose PV are removed
	 */
	void onRemovedPVInsteadOfBet(String pPlayerName);

	/**
	 * Called when a player has lost the game
	 * @param pPlayerName The player that has lost
	 */
	void onPlayerEliminated(String pPlayerName);

	/**
	 * Called when the preparation of the game is over, all the players are ready to go and the game can start.
	 * @param pPlayerNamePlayerCardMap
	 * @param pTotalNumberOfTurns
	 */
	void onGameStarted(Map<String, String> pPlayerNamePlayerCardMap, int pTotalNumberOfTurns);

	/**
	 * Called when the game is over
	 * @param pWinnerPlayerName The name of the winner
	 */
	void onEndOfGame(String pWinnerPlayerName);

	/**
	 * Called to signal that the players whould start registering for the game.
	 * It is expected that this methods calls {@link ITavoloDiGioco#startMatch()} before returning
	 */
	void startPlayerRegistration();
}
