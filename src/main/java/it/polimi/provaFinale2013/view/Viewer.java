package it.polimi.provaFinale2013.view;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.horsefever.corse.Races;
import it.polimi.provaFinale2013.horsefever.scommesse.Bookmaker.AlreadyPresentBetException;
import it.polimi.provaFinale2013.horsefever.scommesse.KindOfBet;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.AlreadyRegisteredPlayerNameException;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.NotEnoughPlayerSlotsException;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.PlayerDoNotOwnSpecifiedCardException;
import it.polimi.provaFinale2013.utils.EnumUtils;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.utils.PlayerCardUtils;
import it.polimi.provaFinale2013.utils.UserInteractionUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class Viewer implements IViewer {

	private static final int BASE_SHOW_INDEX_OFFSET = 1;
	private static final int MONEY_FOR_POSITION_REACHED = 200;

	private final BufferedReader mReader;
	private final ITavoloDiGioco<IViewer> mTavolo;

	private PhaseOfTheTurn mActualPhase;
	private Set<String> mPlayersRegisteredInThisViewer = new HashSet<String>();
	private SortedMap<Stable, Integer> mLastQuotations;

	private List<Horse> mHorseArrived = new LinkedList<Horse>();
	private boolean mGameHasStarted = false;

	public Viewer(ITavoloDiGioco<IViewer> pTavoloDiGioco) {
		mReader = new BufferedReader(new InputStreamReader(System.in));
		mTavolo = pTavoloDiGioco;
	}

	@Override
	public void onFullQuotationUpdate(Map<Stable, Integer> pHorseToQuotationMap) {
		clearScreen();
		mLastQuotations = new TreeMap<Stable, Integer>(new QuotationComparator<Stable>(pHorseToQuotationMap));
		mLastQuotations.putAll(pHorseToQuotationMap);
		if(mGameHasStarted) {
			updatePhase(PhaseOfTheTurn.UPDATE_QUOTATIONS);
			printOrderedQuotations(pHorseToQuotationMap);
		}
	}

	@Override
	public void onSingleQuotationChanged(Stable pStable, int pOldQuotation, int pNewQuotation) {
		if(pOldQuotation != pNewQuotation) {
			updatePhase(PhaseOfTheTurn.UPDATE_QUOTATIONS);
			Log.user("The quotation of " + pStable + " is updated from 1:" + pOldQuotation + " to 1:" + pNewQuotation);
		}
	}

	@Override
	public void onRaceStarted() {
		clearScreen();
		updatePhase(PhaseOfTheTurn.RACE);
		Log.user("Race has started");
	}

	@Override
	public void onMovementCardRevealed(int[] pMovementCardValues) {
		clearScreen();
		updatePhase(PhaseOfTheTurn.RACE);
		String cardStringed = Arrays.toString(pMovementCardValues);
		Log.user("The values of the Movement Card are : " + cardStringed.substring(1, cardStringed.length()-1) + ".");
	}

	@Override
	public void onSprint(Horse pHorse, int pSprintAmountInThisTurn) {
		updatePhase(PhaseOfTheTurn.RACE);
		Log.user(pHorse + " is sprinting " + pSprintAmountInThisTurn + " square" + (pSprintAmountInThisTurn == 1 ? "." : "s."));
	}

	@Override
	public void onHorsePositionsChanged(Map<Horse, Integer> pHorseToPositionMap) {
		updatePhase(PhaseOfTheTurn.RACE);
		Log.user("____________________________________________________");
		for (Map.Entry<Stable, Integer> horseQuotation : mLastQuotations.entrySet()) {
			Horse horse = EnumUtils.getHorsefromStable(horseQuotation.getKey());
			int quotation = horseQuotation.getValue();
			int position = pHorseToPositionMap.get(horse);
			if (!mHorseArrived.contains(horse)) {
				StringBuilder asterisks = new StringBuilder();
				// correcting the offset in order to have all the asterisisks start at the same location
				for(int i = horse.toString().length(); i < Horse.HORSE_YELLOW.toString().length(); i++) {
					asterisks.append(" ");
				}
				for (int i = 0; i < position && i < Races.RACE_LENGHT; i++) {
					asterisks.append("*");
				}
				// adding an indicator to shows how much to go
				for(int i = position; i < Races.RACE_LENGHT; i++) {
					asterisks.append("-");
				}
				asterisks.append("|");
				for (int i = Races.RACE_LENGHT; i < position; i++) {
					asterisks.append("*");
				}
				Log.user("[1:" + quotation + "] " + horse + " " + asterisks);
			} else {
				Log.user(horse + " arrived " + (mHorseArrived.indexOf(horse) + 1)  + "�");;
			}
		}
		Log.user("____________________________________________________");
	}

	@Override
	public void onHorseArrived(Horse pHorse, int pHorsePosition) {
		updatePhase(PhaseOfTheTurn.RACE);
		String position = null;
		switch (pHorsePosition) {
			case 1: position = "1st";
					break;
			case 2: position = "2nd";
					break;
			case 3: position = "3rd";
					break;
			default: position = String.valueOf(pHorsePosition).concat("th");
					break;
		}
		Log.user(pHorse + " has arrived " + position);
		mHorseArrived.add(pHorse);
	}

	@Override
	public void onRaceFinished(Horse[] pArrivalOrderedHorses) {
		clearScreen();
		updatePhase(PhaseOfTheTurn.END_OF_THE_RACE);
		StringBuilder sb = new StringBuilder();
		sb.append("Race is finished:\n");
		for(int i = 0; i < pArrivalOrderedHorses.length; i++) {
			sb.append(1).append("� " + pArrivalOrderedHorses[i]);
		}
		mHorseArrived.clear();
	}

	@Override
	public void onPlacedBet(String pPlayerName, Horse pHorse, int pValue, KindOfBet pKindOfBet) {
		Log.user(pPlayerName + " bet " + pValue + " money on " + pHorse + " " + pKindOfBet);
	}

	@Override
	public void onWonBet(String pPlayerName, int pAmount, int pPVs, KindOfBet pKindOfBet) {
		updatePhase(PhaseOfTheTurn.BET_PAYMENTS);
		Log.user(pPlayerName + " won " + pAmount + " money and " + pPVs + " PVs for bet " + pKindOfBet);
	}

	@Override
	public void onStablePaid(Stable pStable, int pAmount) {
		updatePhase(PhaseOfTheTurn.BET_PAYMENTS);
		int arrival = 4 - pAmount / MONEY_FOR_POSITION_REACHED;
		Log.user(pStable + " receives " + pAmount + " because his horse arrived " + arrival + "�");
	}

	@Override
	public void onActionCardGivenToPlayer(String pPlayerName, String pCardName) {
		updatePhase(PhaseOfTheTurn.CARDS_DEALING);
		if(mPlayersRegisteredInThisViewer.contains(pPlayerName)) {
			Log.user(pPlayerName + " receives " + pCardName);
		} else {
			Log.user(pPlayerName + " received an action card. ");
		}
	}

	@Override
	public void onActionCardAssignedToHorse(String pPlayerName, String pCardName, Horse pHorse) {
		if(mPlayersRegisteredInThisViewer.contains(pPlayerName)) {
			Log.user(pPlayerName + " has assigned the card " + pCardName + " to "+ pHorse);
		} else {
			Log.user(pPlayerName + " has assigned a card to " + pHorse);
		}
	}

	@Override
	public void onExpectPlacedCard(String pPlayerName, List<String> pAvailableCards) {
		clearScreen();
		updatePhase(PhaseOfTheTurn.MODIFY_RACE);
		Log.user(pPlayerName + " has to place a card.");
		String cardName = readCardFromConsole(pAvailableCards);
		Log.user("Write the number of the horse you want to place the card on.");
		Horse horse = readHorseFromConsole();
		try {
			mTavolo.assignCardToHorse(pPlayerName, cardName, horse);
		} catch (PlayerDoNotOwnSpecifiedCardException e) {
			Log.user(pPlayerName + " doesn't own this card.");
			onExpectPlacedCard(pPlayerName, pAvailableCards);
		}
	}

	@Override
	public void onTurnFinished() {
		updatePhase(PhaseOfTheTurn.END_OF_THE_TURN);
		Log.user("Turn is over.");
		clearScreen();
	}

	@Override
	public void onPlayerEliminated(String pPlayerName) {
		updatePhase(PhaseOfTheTurn.END_OF_THE_TURN);
		Log.user(pPlayerName + " has been removed from the game.");
		boolean wasOneOfThisViewPlayer = mPlayersRegisteredInThisViewer.contains(pPlayerName);
		mPlayersRegisteredInThisViewer.remove(pPlayerName);
		if( wasOneOfThisViewPlayer && mPlayersRegisteredInThisViewer.isEmpty()) {
			boolean beObserver = readYesOrNoFromConsole("Do you want to continue as an observer? [y|n]");
			if(!beObserver) {
				System.exit(0);
			}
		}
	}

	@Override
	public boolean onExpectBet(String pPlayerName, int pMinBet, int pMaxBet, Map<Horse, Integer> pHorseToQuotationMap, boolean isMandatory) {
		clearScreen();
		if(isMandatory) {
			updatePhase(PhaseOfTheTurn.FIRST_BET);
		} else {
			updatePhase(PhaseOfTheTurn.SECOND_BET);
		}

		boolean playerWantsToBet = true;
		if(isMandatory) {
			Log.user(pPlayerName + " has to bet.");
		} else {
			Log.user(pPlayerName + " may bet.");
			playerWantsToBet = readYesOrNoFromConsole("Type 'y' is you want to bet, type 'n' if you don't want to bet.");
		}
		if(playerWantsToBet) {
			printOrderedQuotations(pHorseToQuotationMap);
			Log.user("Write the number of the horse you want to bet on.");
			Horse horse = readHorseFromConsole();
			KindOfBet kindOfBet = readKindOfBetFromConsole();
			int amount = readIntFromConsole("Write how much money you want to bet [" + pMinBet + "," + pMaxBet + "]. It must be a value divisible by 100.", pMinBet, pMaxBet, true);
			try {
				mTavolo.registerBet(pPlayerName, horse, amount, kindOfBet);
				return true;
			} catch (AlreadyPresentBetException e) {
				Log.user("This bet already exists.");
				return onExpectBet(pPlayerName, pMinBet, pMaxBet, pHorseToQuotationMap, isMandatory);
			}
		} else {
			return false;
		}
	}

	@Override
	public void onRemovedPVInsteadOfBet(String pPlayerName) {
		Log.user(pPlayerName + "'s PVs have been removed because he had not enough money to bet.");
	}

	@Override
	public void onGameStarted(Map<String, String> pPlayerNamePlayerCardMap, int pNumberOfTurns) {
		clearScreen();
		mGameHasStarted = true;
		Log.user("The game  has started. You will play " + pNumberOfTurns + " turns.");
		StringBuilder sb = new StringBuilder();
		sb.append("The players are:\n");
		for(Map.Entry<String, String> entry : pPlayerNamePlayerCardMap.entrySet()) {
			String playerName = entry.getKey();
			String cardName = entry.getValue();
			int initialMoney = PlayerCardUtils.getInitialAmountOfMoney(cardName);
			int quotation = PlayerCardUtils.getInitialQuotationOfAssociatedStable(cardName);
			Stable stable = getStableFromInitialQuoations(mLastQuotations, quotation);
			sb.append(playerName).append(" with the player card \"" + cardName + "\"\n");
			sb.append("He will have ").append(initialMoney).append(" and is the owner of the ").append(stable.toString()).append("\n");
		}
		sb.append("Have fun!!");
		Log.user(sb.toString());
	}

	@Override
	public void onEndOfGame(String pWinningPlayerName) {
		clearScreen();
		updatePhase(PhaseOfTheTurn.END_OF_THE_TURN);
		Log.user(pWinningPlayerName + " has won the game!");
		System.exit(0);
	}

	@Override
	public void startPlayerRegistration() {
		clearScreen();
		updatePhase(PhaseOfTheTurn.BEGINNING_OF_THE_TURN);
		String playerName = readStringFromConsole("Write the player name. Leave blank and press 'Enter' if you don't want to add another player.");
		if(playerName.isEmpty()) {
			if(mPlayersRegisteredInThisViewer.isEmpty()) {
				boolean beObserver = readYesOrNoFromConsole("No player registered. Press 'y' if you want to observe the game or press 'n' if you want to quit.");
				if(beObserver) {
					mTavolo.startMatch();
				} else {
					System.exit(0);
				}
			} else {
				mTavolo.startMatch();
			}
		} else {
			try {
				mTavolo.registerPlayer(playerName);
				mPlayersRegisteredInThisViewer.add(playerName);
				startPlayerRegistration();
				return;
			} catch (AlreadyRegisteredPlayerNameException e) {
				Log.user("Player name already used. Please enter a different one.");
				startPlayerRegistration();
				return;
			} catch (NotEnoughPlayerSlotsException e) {
				if(mPlayersRegisteredInThisViewer.isEmpty()) {
					Log.user("Maximum number of players reached. You cannot play this game.");
					boolean wantsToWatch = readYesOrNoFromConsole("Press 'y' if you still want to observe the game or press 'n' if you want to quit.");
					if(!wantsToWatch) {
						System.exit(0);
					}
				} else {
					Log.user("Sorry, maximum number of players reached. The additional one can watch the game.");
				}
				mTavolo.startMatch();
			}
		}
	}

	@Override
	public void onActionCardRevealed(Horse pHorse,
			List<String> pActionCardsAssigned, List<String> pActionCardsRemoved) {
		if(!pActionCardsAssigned.isEmpty()) {
			Log.user("Revealed the cards assigned to " + pHorse);
			StringBuilder sb = new StringBuilder();
			sb.append("The assigned cards are: ");
			for(String card : pActionCardsAssigned) {
				sb.append(card + ", ");
			}
			sb.deleteCharAt(sb.length()-2);
			if(!pActionCardsRemoved.isEmpty()) {
				sb.append("Unluckly these cards were removed: ");
				for(String card : pActionCardsRemoved) {
					sb.append(card + ", ");
				}
				sb.deleteCharAt(sb.length()-2);
			}
			Log.user(sb.toString());
		}
	}

	private String readStringFromConsole(String pMessage) {
		try {
			return UserInteractionUtils.readLine(mReader, pMessage, "The input is not valid", false, true);
		} catch (IOException e) {
			Log.e("Errore nella lettura di una String dalla console.", e);
			throw new IllegalStateException("This should never be reached", e);
		}
	}

	private Horse readHorseFromConsole() {
		try {
			StringBuilder message = new StringBuilder();
			message.append("\n");
			Map<String, Horse> horseMap = new HashMap<String, Horse>();
			int horseIndex = BASE_SHOW_INDEX_OFFSET;
			for(Horse horse : Horse.values()) {
				horseMap.put("" + horseIndex, horse);
				message.append("(" + horseIndex + ") " + horse + ", ");
				horseIndex++;
			}
			message.deleteCharAt(message.length() -2);
			message.append("\n");
			Log.user(message.toString());
			return UserInteractionUtils.readMultipleChoice(mReader, "Please choose a horse", horseMap, "The input is not valid.", true);
		} catch (IOException e) {
			Log.e("An error occoured while accessing the console.", e);
			throw new IllegalStateException("This should never be reached", e);
		}
	}

	private int readIntFromConsole(String pMessageToShow, int pMin, int pMax, boolean mustBeDivisibleBy100) {
		try {
			int value = UserInteractionUtils.readInt(mReader, pMessageToShow, pMin, pMax, "The value is not a valid one.");
			if(mustBeDivisibleBy100) {
				if(value % 100 == 0) {
					return value;
				} else {
					return readIntFromConsole(pMessageToShow, pMin, pMax, mustBeDivisibleBy100);
				}
			} else {
				return value;
			}
		}
		catch (IOException e) {
			Log.e("Errore nella lettura di un int dalla console.", e);
			throw new IllegalStateException("This should never be reached", e);
		}
	}

	private KindOfBet readKindOfBetFromConsole() {
		try {
			Map<String, KindOfBet> choiceMap = new HashMap<String, KindOfBet>();
			choiceMap.put("w", KindOfBet.WINNER);
			choiceMap.put("p", KindOfBet.PLACED);
			return UserInteractionUtils.readMultipleChoice(mReader, "Type 'w' if you want to place a bet on the winner horse or 'p' if you want to place a bet on a placed horse.",
					choiceMap, "Not supported answer", true);
		} catch (IOException e) {
			Log.e("Errore nella lettura di un KindOfBet dalla console.", e);
			throw new IllegalStateException("This should never be reached", e);
		}
	}

	private String readCardFromConsole(List<String> pAvailableCards) {
		try {
			Map<String, String> choiceMap = new HashMap<String, String>();
			for(int i = 0; i < pAvailableCards.size() ; i++){
				choiceMap.put("" + (i+1), pAvailableCards.get(i));
			}
			StringBuilder sb = new StringBuilder();
			sb.append("These are the available cards:\n");
			for(Map.Entry<String, String> entry : choiceMap.entrySet()) {
				sb.append("(" + entry.getKey() + ") " + entry.getValue() + ", ");
			}
			sb.deleteCharAt(sb.length()-2);
			sb.append("\n");
			Log.user(sb.toString());
			return UserInteractionUtils.readMultipleChoice(mReader, "Write the number of the card you want to place", choiceMap, "This is not a valid input.", true);
		} catch (IOException e) {
			Log.e("Errore nella lettura di una Carta dalla console.", e);
			throw new IllegalStateException("This should never be reached", e);
		}
	}

	private boolean readYesOrNoFromConsole(String pMessage) {
		try {
			return UserInteractionUtils.readChoice(mReader, pMessage,
					"y", "n", "Not supported answer", true);
		} catch (IOException e) {
			Log.e("Errore nella lettura di una String dalla console.", e);
			throw new IllegalStateException("This should never be reached", e);
		}
	}

	private void updatePhase(PhaseOfTheTurn pPhase) {
		if(mActualPhase != pPhase) {
			Log.user(">> Phase " + pPhase.name());
			mActualPhase = pPhase;
		}
	}

	private <K> void printOrderedQuotations(Map<K, Integer> pQuotations) {
		SortedMap<K, Integer> orderedQuotations = new TreeMap<K, Integer>(new QuotationComparator<K>(pQuotations));
		orderedQuotations.putAll(pQuotations);
		StringBuilder sb = new StringBuilder();
		sb.append("The quotations are:\n");
		for(Map.Entry<K, Integer> entry : orderedQuotations.entrySet()) {
			sb.append(entry.getKey().toString() + " 1:" + entry.getValue() + "\n");
		}
		Log.user(sb.toString());
	}

	private Stable getStableFromInitialQuoations(Map<Stable, Integer> pQuotations, int quotationValue) {
		for(Map.Entry<Stable, Integer> quotation : pQuotations.entrySet()) {
			if(quotation.getValue().equals(quotationValue)) {
				return quotation.getKey();
			}
		}
		Log.e("No such player exists.");
		throw new IllegalArgumentException();
	}

	private void clearScreen() {
		for(int i = 0; i < 50; i++) {
			Log.user("");
		}
	}
	private static enum PhaseOfTheTurn {
		BEGINNING_OF_THE_TURN,
		CARDS_DEALING,
		FIRST_BET,
		MODIFY_RACE,
		SECOND_BET,
		RACE,
		END_OF_THE_RACE,
		BET_PAYMENTS,
		UPDATE_QUOTATIONS,
		END_OF_THE_TURN;
	}

	private static class QuotationComparator<K> implements Comparator<K> {
		private Map<K, Integer> map;

		QuotationComparator(Map<K, Integer> pMap) {
			map = pMap;
		}

		@Override
		public int compare(K o1, K o2) {
			if(map.get(o1) > map.get(o2)) {
				return +1;
			} else if(map.get(o1) < map.get(o2)){
				return -1;
			} else {
				return -1; //can't return 0 otherwise the map will delete the key
			}
		}
	}
}
