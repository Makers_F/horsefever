package it.polimi.provaFinale2013.horsefever.cavalli;

public enum Colour {

	YELLOW(),
	BLUE(),
	BLACK(),
	GREEN(),
	WHITE(),
	RED();

	Colour() {
	}
}