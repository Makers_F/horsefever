package it.polimi.provaFinale2013.horsefever.cavalli;

import it.polimi.provaFinale2013.utils.RandomUtils;

public class ColoredDice {

	private static final Colour[] mFaces = new Colour[]{
		Colour.YELLOW, Colour.BLUE, Colour.BLACK, Colour.GREEN, Colour.WHITE, Colour.RED
	};

	public static Colour rollDice() {
		int index = RandomUtils.getRandom().nextInt(mFaces.length);
		return mFaces[index];
	}
}
