package it.polimi.provaFinale2013.horsefever.cavalli;

/**
 * This enum rappresent the stables with they associated colors
 * @author Francesco
 *
 */
public enum Stable {

	STABLE_YELLOW(Colour.YELLOW),
	STABLE_BLUE(Colour.BLUE),
	STABLE_BLACK(Colour.BLACK),
	STABLE_GREEN(Colour.GREEN),
	STABLE_WHITE(Colour.WHITE),
	STABLE_RED(Colour.RED);

	private final Colour mColor;

	Stable(Colour pColor) {
		mColor = pColor;
	}

	public Colour getColor() {
		return mColor;
	}

	@Override
	public String toString() {
		return mColor.toString().toLowerCase() + " stable";
	}
	/**
	 *
	 * @return La scuderia associata al colore
	 */
	public static Stable getStableFromColor(Colour pColor) {
		for(Stable stable : Stable.values()) {
			if(stable.getColor() == pColor){
				return stable;
			}
		}
		throw new IllegalArgumentException("Il colore " + pColor + " non e' associato a nessuna scuderia.");
	}
}
