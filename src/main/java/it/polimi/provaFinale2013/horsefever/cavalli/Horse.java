package it.polimi.provaFinale2013.horsefever.cavalli;

/**
 * This enum rappresent the different horses that plays in this game
 * @author Francesco
 *
 */
public enum Horse {

	HORSE_WHITE(Colour.WHITE),
	HORSE_YELLOW(Colour.YELLOW),
	HORSE_RED(Colour.RED),
	HORSE_GREEN(Colour.GREEN),
	HORSE_BLUE(Colour.BLUE),
	HORSE_BLACK(Colour.BLACK);

	private final Colour mColor;

	Horse (Colour pColor) {
		mColor = pColor;
	}

	public Colour getColor() {
		return mColor;
	}

	@Override
	public String toString() {
		return mColor.toString().toLowerCase() + " horse";
	}

	/**
	 *
	 * @return Il cavallo associato al colore
	 */
	public static Horse getHorseFromColor(Colour pColor) {
		for(Horse horse : Horse.values()) {
			if(horse.getColor() == pColor){
				return horse;
			}
		}
		throw new IllegalArgumentException("Non esiste un cavallo con color: " + pColor);
	}
}