package it.polimi.provaFinale2013.horsefever.scommesse;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.horsefever.corse.RaceResults;
import it.polimi.provaFinale2013.horsefever.giocatore.Player;
import it.polimi.provaFinale2013.horsefever.tavolo.IBetListener;
import it.polimi.provaFinale2013.utils.EnumUtils;
import it.polimi.provaFinale2013.utils.Log;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
/**
 * This class is the bookmaker of the game. It takes the bet and pays the correct bets and the winning stables
 * @author Francesco
 *
 */
public class Bookmaker {

	private static final int PV_PER_WIN = 3;
	private static final int PV_PER_PLACED = 1;
	private static final int MONEY_MULTIPLIER_PER_PLACED = 2;

	// Ogni stable guadagna 200 per ogni posizione oltre la 4a. Il terzo prende 200, il secondo 400, il primo 600.
	private static final int FIRST_STABLE_MONEY_PRIZE = 600;
	private static final int LESS_MONEY_FOR_EACH_POSITION_AFTER_FIRST = 200;

	public static final int MONEY_INDEX = 0;
	public static final int PV_INDEX = 1;
	private final Multimap<Player, Bet> mBets = ArrayListMultimap.create();
	private final Multimap<Player, Payment> mPayments = ArrayListMultimap.create();
	private final Quotations mQuotation;
	private final List<IBetListener> mBetListeners = new LinkedList<IBetListener>();

	public Bookmaker() {
		mQuotation = new Quotations();
	}

	/**
	 * @return True se c'e' una scommessa non risolta, false altrimenti
	 */
	public boolean isBetPending() {
		return !mBets.isEmpty();
	}

	/**
	 * @return True se c'e' un pagamento non risolto, false altrimenti
	 */
	public boolean isPaymentPending() {
		return !mPayments.isEmpty();
	}

	/**
	 * 
	 * @param pPlayer Il giocatore che effetta la scommessa
	 * @param pHorse Il cavallo su cui scommette il giocatore
	 * @param pAmount L'importo scommesso
	 * @param pKindOfBet La scommessa e' di tipo vincente o piazzato
	 * @throws AlreadyPresentBetException Se esiste gia' una scommessa dello stesso giocatore sullo stesso cavallo e dello stesso tipo
	 */
	public void registerBet(Player pPlayer, Horse pHorse, int pAmount, KindOfBet pKindOfBet) throws AlreadyPresentBetException {
		Bet scommessa = new Bet(pHorse, pAmount, pKindOfBet);
		if(mBets.get(pPlayer).contains(scommessa)) {
			throw new AlreadyPresentBetException(pPlayer.getName(), pHorse, pKindOfBet);
		}
		mBets.put(pPlayer, scommessa);
		for(IBetListener listener : mBetListeners) {
			listener.onPlacedBet(pPlayer.getName(), pHorse, pAmount, pKindOfBet);
		}
		Log.i("Bookmaker", pPlayer.getName() + " ha scommesso " + pAmount + " su " + pHorse + " " + pKindOfBet);
	}

	/**
	 * Dai dati di una gara, estrapola le informazioni riguardanti i soldi e i PV da pagare ai singoli giocatori
	 * @param pRaceResults I risultati della gara
	 */
	public void judgeResults(RaceResults pRaceResults, List<Player> pPlayers) {
		// Aggiunge un pagamento per ogni scommessa giusta
		for(Player player : pPlayers) {
			// registra i pagamenti delle scommesse
			Collection<Bet> iterateCollection = mBets.containsKey(player) ? mBets.get(player) : new LinkedList<Bet>(); // prevent nullpointerex
			for(Bet bet : iterateCollection) {
				int pv = getBetWonPV(bet, pRaceResults);
				int money = getBetWonMoney(bet, pRaceResults);
				if(pv != 0 || money != 0) {
					mPayments.put(player, new Payment(money, pv));
					for(IBetListener listener : mBetListeners) {
						listener.onWonBet(player.getName(), money, pv, bet.getKindOfBet());
					}
					Log.i("Bookmaker", player.getName() + " deve ricevere " + money + " danari e " + pv + " pv per una scommessa azzeccata.");
				}
			}
			// registra i pagamenti per la scuderia vincente
			Horse playerHorse = player.getHorse();
			if(pRaceResults.isPlaced(playerHorse)) {
				int position = pRaceResults.getArrivalPosition(playerHorse);
				int amount = FIRST_STABLE_MONEY_PRIZE - position * LESS_MONEY_FOR_EACH_POSITION_AFTER_FIRST;
				mPayments.put(player, new Payment(amount, 0));
				for(IBetListener listener : mBetListeners) {
					listener.onStablePaid(EnumUtils.getStableFromHorse(playerHorse), amount);
				}
				Log.i(player + " deve ricevere " + amount + " danari perche' proprietario di una scuderia piazzata.");
			}
		}
		mBets.clear();
		mQuotation.update(pRaceResults);
	}

	/**
	 * @param pPlayer Il nome del giocatore a cui devono essere pagati i soldi e i pv
	 * @return Nella cella 0 dell'array ci sono i soldi, nella cella 1 ci sono i pv
	 */
	public int[] giveMoneyAndPv(Player pPlayer) {
		int[] payments = {0,0};
		for(Payment payment : mPayments.get(pPlayer)) {
			payments[MONEY_INDEX] += payment.amount;
			payments[PV_INDEX] += payment.pv;
		}
		mPayments.removeAll(pPlayer);
		Log.i("Bookmaker", "Pagati a " + pPlayer.getName() + " " + payments[MONEY_INDEX] + " danari e " + payments[PV_INDEX] + " pv.");
		return payments;
	}

	/**
	 *
	 * @param pStable La scuderia la cui quotazione deve essere modificata
	 * @param pDelta Il range di spostamento della quotazione del cavallo
	 */
	public void modifyQuotation(Stable pStable, int pDelta) {
		mQuotation.modifyQuotation(pStable, pDelta);
	}

	/**
	 * @return The current quotations
	 */
	public Quotations getQuotations() {
		return mQuotation;
	}

	/**
	 * Register a new listener to be notified of changes
	 * @param pListener The listener to be registered
	 */
	public void registerListener(IBetListener pListener) {
		mBetListeners.add(pListener);
		mQuotation.registerListener(pListener);
	}

	/**
	 * Removes a registered listener
	 * @param pListener The listener to remove
	 * @return True if the listener existed, false otherwise
	 */
	public boolean removeListener(IBetListener pListener) {
		return mBetListeners.remove(pListener) &&
				mQuotation.removeListener(pListener);
	}
	/**
	 *
	 * @param pBet La scommessa che si vuole riscuotere
	 * @param pRaceResults I risultati della corsa relativa alla scommessa
	 * @return L'ammontare di danari dovuti
	 */
	private int getBetWonMoney(Bet pBet, RaceResults pRaceResults) {
		Horse horse = pBet.getHorse();
		if (pBet.getKindOfBet() == KindOfBet.WINNER && pRaceResults.isWinner(horse)) {
			return pBet.getAmount() * mQuotation.getQuotation(horse);
		} else if (pBet.getKindOfBet() == KindOfBet.PLACED && pRaceResults.isPlaced(horse)) {
			return pBet.getAmount() * MONEY_MULTIPLIER_PER_PLACED;
		} else {
			return 0;
		}
	}

	/**
	 *
	 * @param pBet La scommessa che si vuole riscuotere
	 * @param pRaceResults I risultati della corsa relativa alla scommessa
	 * @return L'ammontare di pv dovuti
	 */
	private int getBetWonPV(Bet pBet, RaceResults pRaceResults) {
		Horse horse = pBet.getHorse();
		if (pBet.getKindOfBet() == KindOfBet.WINNER && pRaceResults.isWinner(horse)) {
			return PV_PER_WIN;
		} else if (pBet.getKindOfBet() == KindOfBet.PLACED && pRaceResults.isPlaced(horse)) {
			return PV_PER_PLACED;
		} else {
			return 0;
		}
	}

	private static class Payment {
		private int amount;
		private int pv;
		
		public Payment(int pImporto, int pPV) {
			amount = pImporto;
			pv = pPV;
		}
	}

	@Override
	public String toString() {
		return "Scommesse: " + mBets.toString() + ", Pagamenti: " + mPayments.toString() + ", Quotazioni: " + mQuotation.toString();
	}

	/**
	 * This means that the same player already made a bet on the same horse of the same type
	 * @author Francesco
	 *
	 */
	public static class AlreadyPresentBetException extends Exception {
		private static final long serialVersionUID = 2369668721867539509L;

		public AlreadyPresentBetException(String pPlayerName, Horse pHorse, KindOfBet pKind) {
			super("The player " + pPlayerName + " has already a bet on " + pHorse + ", " + pKind);
		}
	}
}