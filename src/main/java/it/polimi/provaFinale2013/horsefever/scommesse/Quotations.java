package it.polimi.provaFinale2013.horsefever.scommesse;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.horsefever.corse.RaceResults;
import it.polimi.provaFinale2013.horsefever.tavolo.IQuotationListener;
import it.polimi.provaFinale2013.utils.EnumUtils;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.utils.MathUtils;
import it.polimi.provaFinale2013.utils.RandomUtils;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;

/**
 * This class is a rappresentation of the quotations of the horses
 * @author Francesco
 *
 */
public class Quotations {

	private static final int QUOTATION_MODIFIER = 1;
	private static final int NORMALIZER_OFFSET = 2;
	private static final int MAX_QUOTATION = 2;
	private static final int NUMBER_OF_HORSES = 6;
	private static final int MIN_QUOTATION = 7;

	private final List<IQuotationListener> mQuotationListener = new LinkedList<IQuotationListener>();
	/*
	 * La quotazione di ogni cavallo e' sempre salvata nello stesso posto. Per
	 * accederci basta fare quotazioni[Horse.getIndex()]
	 */
	private EnumMap<Stable, Integer> mQuotations = new EnumMap<Stable, Integer>(Stable.class);

	public Quotations() {
		// populate the quotation with random values
		@SuppressWarnings("serial")
		List<Integer> initialQuotations = new ArrayList<Integer>() {{
			add(2); add(3); add(4); add(5); add(6); add(7);
		}};
		RandomUtils.shuffle(initialQuotations);
		int i = 0;
		for(Stable stable : Stable.values()) {
			mQuotations.put(stable, initialQuotations.get(i++));
		}
		Log.i("Quotations", "Le quotazioni iniziali sono " + mQuotations.toString());
		for(IQuotationListener listener : mQuotationListener) {
			listener.onFullQuotationUpdate(mQuotations);
		}
	}

	/**
	 * @param pRaceResults The race results used to update the quotations
	 */
	public void update(RaceResults pRaceResults) {
		for(int arrivalPosition = 0 ; arrivalPosition < NUMBER_OF_HORSES ; arrivalPosition++) {
			Stable stable = pRaceResults.getStableArrivedAt(arrivalPosition);
			int normalizedQuotation = (mQuotations.get(stable) - NORMALIZER_OFFSET);
			if (arrivalPosition > normalizedQuotation) {
				modifyQuotation(stable, QUOTATION_MODIFIER, false);
			}
			if (arrivalPosition < normalizedQuotation) {
				modifyQuotation(stable, -QUOTATION_MODIFIER, false);
			}
		}
		for(IQuotationListener listener : mQuotationListener) {
			listener.onFullQuotationUpdate(mQuotations);
		}
	}

	/**
	 * @param pStable La scuderia di cui si vuole la quotazione
	 * @return La quotazione della scuderia
	 */
	public int getQuotation(Stable pStable) {
		return mQuotations.get(pStable);
	}

	/**
	 * @param pHorse Il cavallo di cui si vuole la quotazione
	 * @return La quotazione del cavallo
	 */
	public int getQuotation(Horse pHorse) {
		return getQuotation(EnumUtils.getStableFromHorse(pHorse));
	}

	/**
	 * @param pStable La scuderia la cui quotazione deve essere modificata
	 * @param pDelta Di quanto viene abbassata la quotazione
	 */
	public void modifyQuotation(Stable pStable, int pDelta) {
		modifyQuotation(pStable, pDelta, true);
	}

	/**
	 * @param pQuotation La quotazione di cui si vogliono sapere i cavalli presenti
	 * @return La lista dei cavalli con la quotazione richiesta
	 */
	public List<Horse> getHorseFromQuotation(int pQuotation) {
		if(pQuotation < 2 || pQuotation > 7) {
			Log.e("The quotation should be in the [2,7] range.");
		}
		List<Horse> quotationN = new LinkedList<Horse>();
		for (Stable stable : getStableFromQuotation(pQuotation)) {
			quotationN.add(EnumUtils.getHorsefromStable(stable));
		}
		return quotationN;
	}

	/**
	 * @param pQuotation La quotazione di cui si vogliono sapere le scuderie associate
	 * @return La lista delle scuderie con la quotazione richiesta
	 */
	public List<Stable> getStableFromQuotation(int pQuotation) {
		if(pQuotation < 2 || pQuotation > 7) {
			Log.e("The quotation should be in the [2,7] range.");
		}
		List<Stable> quotationN = new LinkedList<Stable>();
		for (Stable stable : Stable.values()) {
			if (mQuotations.get(stable) == pQuotation && !quotationN.contains(stable)) {
				quotationN.add(stable);
			}
		}
		return quotationN;
	}

	/**
	 * Notify all the registered listeners of the current quotations
	 */
	public void notifyQuotations() {
		for(IQuotationListener listener : mQuotationListener) {
			listener.onFullQuotationUpdate(mQuotations);
		}
	}

	/**
	 * Register a new listener to be notified of changes
	 * @param pListener The listener to be registered
	 */
	public void registerListener(IQuotationListener pListener) {
		mQuotationListener.add(pListener);
	}

	/**
	 * Removes a registered listener
	 * @param pListener The listener to remove
	 * @return True if the listener existed, false otherwise
	 */
	public boolean removeListener(IQuotationListener pListener) {
		return mQuotationListener.remove(pListener);
	}

	@Override
	public String toString() {
		return "Quotazioni: " + mQuotations.toString();
	}

	private void modifyQuotation(Stable pStable, int pDelta, boolean pNotify) {
		int oldValue = mQuotations.get(pStable);
		int realNewValue = MathUtils.clamp(mQuotations.get(pStable) + pDelta, MAX_QUOTATION, MIN_QUOTATION);
		mQuotations.put(pStable, realNewValue);
		Log.i("Quotations", "Modificata la quotazione di " + pStable.toString() + " da " + oldValue + " a " + realNewValue);
		if(pNotify) {
			for(IQuotationListener listener : mQuotationListener) {
				listener.onSingleQuotationChanged(pStable, oldValue, realNewValue);
			}
		}
	}
}