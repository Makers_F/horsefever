package it.polimi.provaFinale2013.horsefever.scommesse;

public enum KindOfBet {
	PLACED(),
	WINNER();
}
