package it.polimi.provaFinale2013.horsefever.scommesse;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.utils.EnumUtils;

/**
 * This class is the rappresentation of a bet
 * @author Francesco
 *
 */
class Bet {

	private Horse mHorse;
	private int mAmount;
	private KindOfBet mKindOfBet;

	public Bet(Horse pHorse, int pAmount, KindOfBet pKindOfBet) {
		mHorse = pHorse;
		mAmount = pAmount;
		mKindOfBet = pKindOfBet;
	}

	public Horse getHorse() {
		return mHorse;
	}

	public int getAmount() {
		return mAmount;
	}

	public KindOfBet getKindOfBet() {
		return mKindOfBet;
	}

	@Override
	public int hashCode() {
		return EnumUtils.getIndex(mHorse) << 1 | EnumUtils.getIndex(mKindOfBet);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Bet) {
			Bet bet = (Bet) o;
			return mHorse == bet.getHorse() && mKindOfBet == bet.getKindOfBet();
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return (mHorse.name() + " , $ " + mAmount + " , " + mKindOfBet.name());
	}
}