package it.polimi.provaFinale2013.horsefever.corse;

import it.polimi.provaFinale2013.horsefever.carte.ActionCard;
import it.polimi.provaFinale2013.horsefever.carte.ActionCard.Effect;
import it.polimi.provaFinale2013.horsefever.carte.ActionCard.Trigger_Effect;
import it.polimi.provaFinale2013.horsefever.carte.IActionCard;
import it.polimi.provaFinale2013.horsefever.carte.SpecialActionCard;
import it.polimi.provaFinale2013.horsefever.carte.SpecialActionCard.SpecialEffectAction;
import it.polimi.provaFinale2013.utils.Log;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Francesco
 * This class takes into account all the effect of the cards assigned to a specific horse.
 * It will then be able to tell what effect happens.
 */
class CumulativeCardEffect {
	private static final int DEFAULT_SPRINT_VALUE = 1;
	private static final int DEFAULT_ADVANCEMENT_ON_FINISH_LINE = 0;
	public static final int STOP_AT_FINISH_LINE = -1;

	private final EnumMap<Trigger_Effect, ExecutableEffect> effects;
	private final List<SpecialActionCard> mModifyQuotationCards;

	CumulativeCardEffect() {
		effects = new EnumMap<ActionCard.Trigger_Effect, CumulativeCardEffect.ExecutableEffect>(Trigger_Effect.class);
		mModifyQuotationCards = new LinkedList<SpecialActionCard>();
	}

	/**
	 * This method evaluate all the passed cards, remove the incompatibilies and set the state of the class
	 * in order to reppresent all the effects the cards whould have applied to the horse
	 * @param pActionCardsCollection The cards assigned to a specific horse.
	 */
	public List<ActionCard> evaluateAssignedCards(Collection<IActionCard<?,?>> pActionCardsCollection) {
		// dividi le carte speciali da quelle normali.
		// nel caso ce ne sono che influenzano le carte normali
		// tienine in conto subito in modo da poter ignorare la carte dopo
		List<ActionCard> actionCards = new LinkedList<ActionCard>();
		boolean ignoreMalus = false;
		boolean ignoreBonus = false;
		for(IActionCard<?,?> ac : pActionCardsCollection) {
			if(ac instanceof SpecialActionCard) {
				SpecialActionCard specialCard = (SpecialActionCard) ac;
				if (specialCard.getEffect().getEffect() == SpecialEffectAction.MODIFY_QUOTATIONS) {
					mModifyQuotationCards.add(specialCard);
				} else {
					if (ac.isMalus()) {
						ignoreBonus = true;
					} else {
						ignoreMalus = true;
					}
				}
			} else {
				actionCards.add((ActionCard) ac);
			}
		}

		List<ActionCard> toBeRemoved = new LinkedList<ActionCard>();
		for(int i = 0 ; i < actionCards.size(); i++) {
			ActionCard ca1 = actionCards.get(i);
			if((ca1.isMalus() && ignoreMalus) || (!ca1.isMalus() && ignoreBonus)) {
				toBeRemoved.add(ca1);
				// la carta e' stata tolta, non bisogna eliminare quelle con la stessa lettera
				continue;
			}
			for(int j = i + 1; j < actionCards.size(); j++) {
				ActionCard ca2 = actionCards.get(j);
				if(ca1.getLetter() == ca2.getLetter()) {
					toBeRemoved.add(ca1);
					toBeRemoved.add(ca2);
				}
			}
		}
		actionCards.removeAll(toBeRemoved);
		if(!toBeRemoved.isEmpty()) {
			Log.i("CumulativeCardEffect", "Rimosse le seguenti carte con conflitti: " + toBeRemoved);
		}

		// NOTA: Dopo l'ordinamento verranno sempre prima analizzati i valore fissi e poi gli offset!
		Collections.sort(actionCards, new EffectComparator());

		for(ActionCard ca : actionCards) {
			Effect effect = ca.getEffect();

			Trigger_Effect trigger = effect.getEffect();
			boolean wasEffectPresent = effects.containsKey(trigger);
			ExecutableEffect oldEff = effects.get(trigger);
			switch (trigger) {
			case START:
				// Se c'e' una carta con valore fisso, bisogna usare quella come base per tutti gli offset.
				// Inoltre puo' esserci una sola carta con valore fisso, dato che le lettere uguali si elidono.
				if(wasEffectPresent) {
					// le lettere delle carte impediscono che ci siano 2 carte con valore non offset, per cui se c'e' gia' una carta
					// per forza quella corrente sara' un offset. Inoltre se la prima era un valore fisso, allora il valore andar� sempre considerato
					// come fisso e non come offset, ma se la prima era un offset, allora significa che non ci sono carte con valore fisso, e quindi
					// sara' sempre un offset. Per cui l'offset o no lo decide la prima carta analizzata, e le altre non cambiano la situazione.
					oldEff.intensity += effect.getIntensity();
				} else {
					ExecutableEffect newEff = new ExecutableEffect(effect.isOffset(), effect.getIntensity());
					effects.put(trigger, newEff);
				}
				break;
			case SPRINT:
				if(wasEffectPresent) {
					boolean wasOffset = oldEff.isOffset;
					boolean isOffset = effect.isOffset();
					if(!isOffset) {
						// ci sono 2 carte compatibili, una che ha valore fisso = 2, l'altra che impedisce lo sprint. Dato che sembra da come sono scritte le carte
						// che quella che fa stare fermi abbia il sopravvento, se la carta corrente e' a valore fisso, allora di sicuro la carta precedente era a valore
						// fisso poiche' la lista e' ordinata. Da cui, di sicuro una delle 2 e' quella che fa stare fermi, per cui si puo' mettere il valore direttamente a 0
						Log.i("EFFETTI", "Due carte con valore fisso sullo sprint. Sprint a 0");
						oldEff.intensity = 0;
					} else if (wasOffset) {
						// se era un offset, vuol dire che non c'e' mai stata una carta a valore fisso. Somma semplicemente gli offset.
						oldEff.intensity += effect.getIntensity() ;
					} else {
						// c'e' stata una sola carta non offset.
						// se era quella che faceva stare fermi, allora non aggiungi niente, altrimenti aggiungi l'offset
						oldEff.intensity += oldEff.intensity == 0 ? 0 : effect.getIntensity();
					}
				} else {
					ExecutableEffect newEff = new ExecutableEffect(effect.isOffset(), effect.getIntensity());
					effects.put(trigger, newEff);
				}
				break;
			case FINISHLINE:{
				ExecutableEffect newEff = new ExecutableEffect(effect.isOffset(), effect.getIntensity());
				effects.put(trigger, newEff);
				break;}
			case FOTOFINISH:{
				FotoFinishExecutableEffect newEff = new FotoFinishExecutableEffect(!ca.isMalus());
				effects.put(trigger, newEff);
				break;}
			case MOVEMENT:{
				MovementExecutableEffect newEff = new MovementExecutableEffect(!ca.isMalus(), effect.getIntensity());
				effects.put(trigger, newEff);
				break;}
			}
		}
		return toBeRemoved;
	}

	/**
	 * To be called before the race start.
	 * @return How much the quotation of the horse must be modified.
	 */
	public int onInit() {
		int totalOffset = 0;
		for(SpecialActionCard specialCard : mModifyQuotationCards) {
			totalOffset += specialCard.getEffect().getIntensity();
		}
		return totalOffset;
	}

	/**
	 * To be called on the start of the race.
	 * @param pTheoricValue How much the horse should advance if it moved following the movement card.
	 * @return How much the horse should move taking into account the effect of the cards.
	 */
	public int onStart(int pTheoricValue) {
		if(!effects.containsKey(Trigger_Effect.START)) {
			Log.v("No effect on start");
			return pTheoricValue;
		}
		ExecutableEffect effect = effects.get(Trigger_Effect.START);
		Log.v("<Effetto onStart> = " + effect);
		return Math.max(effect.isOffset ? pTheoricValue + effect.intensity : effect.intensity, 0);
	}

	/**
	 * To be called each time the horse sprints.
	 * @return How much the horse should advance taking into account the effects of the cards.
	 */
	public int onSprint() {
		if(!effects.containsKey(Trigger_Effect.SPRINT)) {
			Log.v("No effect on sprint");
			return DEFAULT_SPRINT_VALUE;
		}
		ExecutableEffect effect = effects.get(Trigger_Effect.SPRINT);
		int effectiveValue = Math.max(effect.isOffset ? DEFAULT_SPRINT_VALUE + effect.intensity : effect.intensity, 0);
		Log.v("<Effetto onSprint> = " + effect +". Sprints by " + effectiveValue);
		return effectiveValue;
	}

	/**
	 * To be called each time the horse advance due a movement card.
	 * Not to be called on the start of the race.
	 * @param pTheoricValue How much the horse should advance due the movement card.
	 * @param isFirst True if the horse is first, even if it is not the only one. False otherwise.
	 * @param isLast True if the horse is last, even if it is not the only one. False otherwise.
	 * @return How much the horse should move taking into account the effect cards.
	 */
	public int onMovement(int pTheoricValue, boolean isFirst, boolean isLast) {
		if(!effects.containsKey(Trigger_Effect.MOVEMENT)) {
			Log.v("No effect on movement");
			return pTheoricValue;
		}
		//value speciale di offset: true --> se ultimo allora scatta, false --> se primo allora sta fermo
		MovementExecutableEffect effect = (MovementExecutableEffect) effects.get(Trigger_Effect.MOVEMENT);
		if(effect.getDoesItFastAdvanceIfLast() && isLast) {
			Log.v("<Effetto onMovement> = " + effect);
			return Math.max(effect.getHowMuchFastDoesItRuns(), 0);// dovrebbe essere 4
		}
		if(effect.getDoesItStayStillIfFirst() && isFirst) {
			Log.v("<Effetto onMovement> = " + effect);
			return Math.max(effect.getHowMuchFastDoesItRuns(), 0);// dovrebbe essere 0
		}
		Log.v("<Effetto onMovement> = ne' primo ne' ultimo, avanza di " + pTheoricValue);
		return pTheoricValue;
	}

	/**
	 * To be called when the horse reaches the finish line.
	 * @return How much the horse should advance over the finish line. If STOP_AT_FINISH_LINE is returned, then the horse should stop on the finish line.
	 */
	public int onFinishLine() {
		if(!effects.containsKey(Trigger_Effect.FINISHLINE)) {
			Log.v("No effect on finish line");
			return DEFAULT_ADVANCEMENT_ON_FINISH_LINE;
		}
		ExecutableEffect effect = effects.get(Trigger_Effect.FINISHLINE);
		Log.v("<Effetto onFinishLine> = " + effect);
		return effect.isOffset ? Math.max(effect.intensity, 0) : STOP_AT_FINISH_LINE;
	}

	/**
	 * To be called on fotofinish.
	 * @return True is the horse should always win, false if the horse should always lose. Null it the effect is not specified
	 */
	public Boolean onFotofinish() {
		if(!effects.containsKey(Trigger_Effect.FOTOFINISH)) {
			Log.v("No effect on fotofinish");
			return null;
		}
		// Valore speciale di offset. true --> vince sempre, false --> perde sempre
		FotoFinishExecutableEffect effect = (FotoFinishExecutableEffect) effects.get(Trigger_Effect.FOTOFINISH);
		boolean doHeAlwaysWin = effect.getDoesItAlwaysWin();
		Log.v("<Effetto onFotoFinish> = " + effect);
		return Boolean.valueOf(doHeAlwaysWin);
	}

	@Override
	public String toString() {
		return "Eff: " + effects + " ModQuot: " + mModifyQuotationCards;
	}

	private static class EffectComparator implements Comparator<ActionCard> {

		@Override
		public int compare(ActionCard o1, ActionCard o2) {
			if(o1.getEffect().isOffset() == o1.getEffect().isOffset()) {
				return 0;
			} else {
				return o1.getEffect().isOffset() ? +1 : -1;
			}
		}
	}

	/**
	 * This class rappresents the sum of all the effects which would have been triggered on the same moment
	 * @author Francesco
	 *
	 */
	private static class ExecutableEffect {
		protected boolean isOffset;
		private int intensity;

		public ExecutableEffect(boolean isEffOffset, int effValue) {
			isOffset = isEffOffset;
			intensity = effValue;
		}

		@Override
		public String toString() {
			return "(" + (isOffset ? "offs, " : "no offs, ") + intensity + ")";
		}
	}

	private static class FotoFinishExecutableEffect extends ExecutableEffect {

		public FotoFinishExecutableEffect(boolean pDoItAlwaysWin) {
			super(pDoItAlwaysWin, 0);
		}

		public boolean getDoesItAlwaysWin() { return super.isOffset;}
		@Override
		public String toString() {
			return "( Always " + (isOffset ? "win" : "lose") + ")";
		}
	}

	private static class MovementExecutableEffect extends ExecutableEffect {

		public MovementExecutableEffect(boolean pDoItFastAdvanceIfLast, int pHowMuchItRunsFast) {
			super(pDoItFastAdvanceIfLast, pHowMuchItRunsFast);
		}

		public boolean getDoesItFastAdvanceIfLast() { return super.isOffset;}
		public boolean getDoesItStayStillIfFirst() { return !super.isOffset;}
		public int getHowMuchFastDoesItRuns() {return super.intensity;}
		@Override
		public String toString() {
			return "( Advance of " + super.intensity + (getDoesItFastAdvanceIfLast() ? " if last" : " if first") + ")";
		}
	}
}