package it.polimi.provaFinale2013.horsefever.corse;

import java.util.Arrays;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.utils.EnumUtils;
import it.polimi.provaFinale2013.utils.Log;

/**
 * This class is the rappresentation of the arrival order of a race
 * @author Francesco
 *
 */
public class RaceResults {

	private static final int FIRST = 0;
	private static final int SECOND = 1;
	private static final int THIRD = 2;
	private static final int FOURTH = 3;
	private static final int FIFTH = 4;
	private static final int SIXTH = 5;

	private final Horse horseArrival[] = new Horse[6];

	public RaceResults(Horse[] horsesArrival) {
		horseArrival[FIRST] = horsesArrival[FIRST];
		horseArrival[SECOND] = horsesArrival[SECOND];
		horseArrival[THIRD] = horsesArrival[THIRD];
		horseArrival[FOURTH] = horsesArrival[FOURTH];
		horseArrival[FIFTH] = horsesArrival[FIFTH];
		horseArrival[SIXTH] = horsesArrival[SIXTH];
	}

	/**
	 * @return The order of arrival of the horses in a race
	 */
	public Horse[] getHorseOrderOfArrival() {
		return horseArrival.clone();
	}

	/**
	 * 
	 * @return The order of arrival of the stables in a race
	 */
	public Stable[] getStableOrderOfArrival() {
		Stable[] stableArrival = new Stable[horseArrival.length];
		for (int i = 0 ; i <= horseArrival.length ; i++) {
			stableArrival[i] = EnumUtils.getStableFromHorse(horseArrival[i]);
		}
		return stableArrival;
	}

	/**
	 * 
	 * @param pPosition The position of the stables you are interested in
	 * @return The stables arrived at pPosition position
	 */
	public Stable getStableArrivedAt(int pPosition) {
		return EnumUtils.getStableFromHorse(horseArrival[pPosition]);
	}

	/**
	 * 
	 * @param horse The horse you are interested in
	 * @return The position at which horse is arrived
	 */
	public int getArrivalPosition(Horse horse) {
		for(int i = 0; i < horseArrival.length; i++) {
			if(horse == horseArrival[i]){
				return i;
			}
		}
		Log.e("Il cavallo non e' arrivato.. :S");
		return SIXTH;
	}

	/**
	 * @param horse The horse you are interested in
	 * @return True if horse arrived first, false otherwise
	 */
	public boolean isWinner(Horse horse) {
		return horse == horseArrival[FIRST];
	}

	/**
	 * @param horse The horse you are interested in
	 * @return True if the horse arrived in the first three positions, false otherwise
	 */
	public boolean isPlaced(Horse horse) {
		return horse == horseArrival[FIRST] || horse == horseArrival[SECOND] || horse == horseArrival[THIRD];
	}

	@Override
	public String toString() {
		return "Risultati: " + Arrays.toString(horseArrival);
	}
}