package it.polimi.provaFinale2013.horsefever.corse;

import java.util.LinkedList;
import java.util.List;

import it.polimi.provaFinale2013.horsefever.carte.Deck;
import it.polimi.provaFinale2013.horsefever.carte.DeckFactory;
import it.polimi.provaFinale2013.horsefever.carte.IActionCard;
import it.polimi.provaFinale2013.horsefever.carte.MovementCard;
import it.polimi.provaFinale2013.horsefever.cavalli.Colour;
import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.horsefever.giocatore.Player;
import it.polimi.provaFinale2013.horsefever.scommesse.Bookmaker;
import it.polimi.provaFinale2013.horsefever.scommesse.Bookmaker.AlreadyPresentBetException;
import it.polimi.provaFinale2013.horsefever.scommesse.KindOfBet;
import it.polimi.provaFinale2013.horsefever.scommesse.Quotations;
import it.polimi.provaFinale2013.horsefever.tavolo.IActionCardListener;
import it.polimi.provaFinale2013.horsefever.tavolo.IBetListener;
import it.polimi.provaFinale2013.horsefever.tavolo.IRaceListener;
import it.polimi.provaFinale2013.utils.Log;

/**
 * 
 * @author Francesco
 * This class handles all the interaction needed for the races.
 * @param <T> The type of the listener to the events.
 */
public class Races<T extends IRaceListener & IBetListener & IActionCardListener> {

	public static final int RACE_LENGHT = 12;
	private final Deck<MovementCard> mFaceDownMovementCards = DeckFactory.getMovementCardsDeck(true);
	private final Deck<IActionCard<?,?>> mFaceDownActionCards = DeckFactory.getActionCardsDeck(true);

	private final Bookmaker mBookMaker;
	private final RaceBoard mRaceboard;
	private int turn = 0;
	private boolean areTheActionCardsResolved = false;
	private final List<T> mListeners = new LinkedList<T>();

	public Races() {
		mRaceboard = new RaceBoard();
		mBookMaker = new Bookmaker();
		reset();
	}

	/**
	 * Register a bet from a player.
	 * @param pPlayer The player
	 * @param pHorse The horse
	 * @param pAmount The ammout of money betted
	 * @param pKindOfBet The kind of bet.
	 * @throws AlreadyPresentBetException It is thown if a bet from the same player, on the same horse and of the same type already have been made
	 */
	public void registerBet(Player pPlayer, Horse pHorse, int pAmount, KindOfBet pKindOfBet) throws AlreadyPresentBetException {
		mBookMaker.registerBet(pPlayer, pHorse, pAmount, pKindOfBet);
	}

	/**
	 * To be called when a pleyer wants to cash in from its bets and stable wins.
	 * @param pPlayer
	 */
	public void payBet(Player pPlayer) {
		int[] payment = mBookMaker.giveMoneyAndPv(pPlayer);
		int amountMoney = payment[Bookmaker.MONEY_INDEX];
		int amountPV = payment[Bookmaker.PV_INDEX];
		pPlayer.giveMoney(amountMoney);
		pPlayer.givePv(amountPV);
	}

	/**
	 * Draw an {@link IActionCard} from the deck for a player
	 * @return The drawn Action Card
	 */
	public IActionCard<?,?> getActionCardForPlayer() {
		return mFaceDownActionCards.draw();
	}

	/**
	 * Assign an action card to an horse.
	 * @param pActionCard The card to be assigned
	 * @param pHorse The horse to which assign the card
	 */
	public void assignCardToHorse(IActionCard<?,?> pActionCard, Horse pHorse) {
		mRaceboard.assignActionCardToHorse(pActionCard, pHorse);
	}

	/**
	 * To be called to start a new race.
	 */
	public void startRace() {
		reset();
		mRaceboard.resolveCardEffects(mBookMaker, mListeners);
		areTheActionCardsResolved = true;
		turn = 0;
		for(T listener : mListeners) {
			listener.onRaceStarted();
		}
		Log.i("La corsa e' iniziata.");
	}

	/**
	 * To be called to make the horse run a round of a race.
	 * This means turning up a movement card and make the horses sprint
	 * @return The colors of the horses which sprinted
	 */
	public Colour[] runARoundOfRace() {
		Log.i("Races", "Iniziato un turno della gara.");
		if(!areTheActionCardsResolved){
			throw new IllegalStateException("Non hai inziato la gare. Inziala prima di fare un round!.");
		}

		MovementCard nextMovementCard = mFaceDownMovementCards.draw();
		Colour[] drawnColors = mRaceboard.raceRound(nextMovementCard, mBookMaker.getQuotations(), turn == 0);
		turn++;
		return drawnColors;
	}

	/**
	 * To be called when the race is finished.
	 * @param pPlayers The list of the players of this game
	 */
	public void endOfRace(List<Player> pPlayers) {
		RaceResults results = getLastRaceResults();
		mBookMaker.judgeResults(results, pPlayers);
		for(T listener : mListeners) {
			listener.onRaceFinished(results.getHorseOrderOfArrival());
		}
		Log.i("La gara e' finita.");
	}

	/**
	 * 
	 * @return True if the race has finished, false otherwise
	 */
	public boolean isRaceFinished() {
		return mRaceboard.isRaceFinished();
	}

	/**
	 * 
	 * @return The results of the last race
	 */
	public RaceResults getLastRaceResults() {
		return mRaceboard.getLastRaceResults();
	}

	/**
	 * 
	 * @return The actual quotations of the {@link Stable}
	 */
	public Quotations getQuotations() {
		return mBookMaker.getQuotations();
	}

	/**
	 * Register a listener to be notified of changes to the game
	 * @param pListener The listener to register
	 */
	public void registerListener(T pListener) {
		mListeners.add(pListener);
		mRaceboard.registerListener(pListener);
		mBookMaker.registerListener(pListener);
	}

	/**
	 * Removes a registered listener.
	 * @param pListener The listener to remove
	 * @return True if the listener existeted, false otherwise
	 */
	public boolean removeListener(T pListener) {
		return mListeners.remove(pListener) &&
				mRaceboard.removeListener(pListener) &&
				mBookMaker.removeListener(pListener);
	}

	private void reset() {
		Log.i("Races", "E' stato resettato lo stato della board.");
		mFaceDownMovementCards.reset();
		mFaceDownActionCards.reset();
		mRaceboard.reset();
		areTheActionCardsResolved = false;
		if(mBookMaker.isPaymentPending()){
			throw new IllegalStateException("L'allibratore non ha pagato tutto!!");
		}
	}

	@Override
	public String toString() {
		return "Turno: " + turn + mBookMaker.toString() + " , " + mRaceboard.toString() + " , " + mFaceDownActionCards.toString() + " , " + mFaceDownMovementCards.toString();
	}
}