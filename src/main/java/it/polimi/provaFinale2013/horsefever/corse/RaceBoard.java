package it.polimi.provaFinale2013.horsefever.corse;

import it.polimi.provaFinale2013.horsefever.carte.ActionCard;
import it.polimi.provaFinale2013.horsefever.carte.IActionCard;
import it.polimi.provaFinale2013.horsefever.carte.MovementCard;
import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Colour;
import it.polimi.provaFinale2013.horsefever.cavalli.ColoredDice;
import it.polimi.provaFinale2013.horsefever.scommesse.Bookmaker;
import it.polimi.provaFinale2013.horsefever.scommesse.Quotations;
import it.polimi.provaFinale2013.horsefever.tavolo.IActionCardListener;
import it.polimi.provaFinale2013.horsefever.tavolo.IRaceListener;
import it.polimi.provaFinale2013.utils.DebugUtils;
import it.polimi.provaFinale2013.utils.EnumUtils;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.utils.RandomUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * This class is the rappresentation of the running board for the horses
 * @author Francesco
 *
 */
class RaceBoard {

	private static final int HORSE_NUMBER = 6;
	static final int FIRST = 1;
	static final int SIXTH = 6;

	private EnumMap<Horse, Integer> mHorsesPositions = new EnumMap<Horse, Integer>(Horse.class);
	private final List<Horse> mArrivalOrder = new LinkedList<Horse>();

	private final EnumMap<Horse, CumulativeCardEffect> mEffects = new EnumMap<Horse, CumulativeCardEffect>(Horse.class);
	private final Multimap<Horse, IActionCard<?,?>> mAssignedCards = ArrayListMultimap.create();

	private final List<IRaceListener> mRaceListeners = new LinkedList<IRaceListener>();

	public RaceBoard() {
		reset();
	}

	/**
	 * Resets the board to its original state, putting the horses on the starting line, removing the cards assigned to them
	 */
	public void reset() {
		mHorsesPositions.clear();
		mEffects.clear();
		mArrivalOrder.clear();
	}

	/**
	 * Assigns a card to an horse
	 * @param pActionCard the card to be assigned
	 * @param pHorse The horse which will receive the card
	 */
	public void assignActionCardToHorse(IActionCard<?, ?> pActionCard, Horse pHorse) {
		mAssignedCards.put(pHorse, pActionCard);
		Log.i("Raceboard", "La carta " + pActionCard.getName() + " e' stata posizionata sul cavallo " + pHorse);
	}

	/**
	 * It runs a round of the current race
	 * @param cartaMovimento The {@link MovementCard} revealed
	 * @param quotazione The {@link Quotations} of the horse
	 * @param isFirstTurn True if it is the firs round of the race
	 * @return The colors of the sprinting horses
	 */
	public Colour[] raceRound(MovementCard cartaMovimento, Quotations quotazione, boolean isFirstTurn) {
		for(IRaceListener listener : mRaceListeners) {
			listener.onMovementCardRevealed(cartaMovimento.getMovements());
		}
		sleepFor(300);
		advance(cartaMovimento, quotazione, isFirstTurn);
		for(IRaceListener listener : mRaceListeners) {
			listener.onHorsePositionsChanged(mHorsesPositions);
		}
		sleepFor(800);
		Colour[] coloriSprint = sprint();
		for(IRaceListener listener : mRaceListeners) {
			listener.onHorsePositionsChanged(mHorsesPositions);
		}
		sleepFor(800);
		checkArrival(quotazione);
		sleepFor(1000);
		return coloriSprint;
	}

	/**
	 * @return True if the race finished, false otherwise
	 */
	public boolean isRaceFinished() {
		return mArrivalOrder.size() == HORSE_NUMBER;
	}

	/**
	 * @return The {@link RaceResults} of the last race
	 */
	public RaceResults getLastRaceResults() {
		if (isRaceFinished()) {
			return new RaceResults(mArrivalOrder.toArray(new Horse[HORSE_NUMBER]));
		} else {
			throw new IllegalStateException("La gara non e' ancora finita!");
		}
	}

	private void advance(MovementCard cartaMovimento, Quotations quotazione, boolean isFirstTurn) {
		int[] advancement = cartaMovimento.getMovements();
		EnumMap<Horse, Integer> newHorsePosition = deepCopyMap(mHorsesPositions);
		for (int i = 0; i < HORSE_NUMBER; i++) {
			int theoricalAdvancement = advancement[i];
			// l'incremento di 2 e' necessario perche' la prima quotazione e' 2 e l'ultima e' 7
			List<Horse> horses = quotazione.getHorseFromQuotation(i + 2);
			for (Horse cv : horses) {
				if (!mArrivalOrder.contains(cv)) {// se e' gia' arrivato non incrementi
					Log.v("Avanzando " + cv);
					int realAdvancement;
					if (isFirstTurn) {
						realAdvancement = mEffects.get(cv).onStart(theoricalAdvancement);
					} else {
						realAdvancement = mEffects.get(cv).onMovement(theoricalAdvancement, isFirst(cv), isLast(cv));
					}
					increaseValue(newHorsePosition, cv, realAdvancement);
				}
			}
		}
		mHorsesPositions = newHorsePosition;
	}

	private Colour[] sprint() {
		Colour[] drawnColors = new Colour[2];
		drawnColors[0] = ColoredDice.rollDice();
		drawnColors[1] = ColoredDice.rollDice();
		Horse sprinter;

		//repat up to 2 times, and only if the drawnColor is different from the previous one
		int i = 0;
		do {
			Log.i("RaceBoard","Dovrebbe sprintare il cavallo " + drawnColors[i]);
			sprinter = Horse.getHorseFromColor(drawnColors[i]);
			if(mArrivalOrder.contains(sprinter)) {
				continue;
			}
			int modification = mEffects.get(sprinter).onSprint();
			increaseValue(mHorsesPositions, sprinter, modification);
			for(IRaceListener listener : mRaceListeners) {
				listener.onSprint(sprinter, modification);
			}
		} while (++i < 2 && drawnColors[0] != drawnColors[1]);
		return drawnColors;
	}

	private void checkArrival(Quotations quotazione) {
		List<Horse> arrivedThisTurn = new LinkedList<Horse>();
		boolean updateTheListenersForPositionChanged = false;
		for (Horse cv : Horse.values()) {
			if (mHorsesPositions.get(cv) >= Races.RACE_LENGHT) {
				if (!mArrivalOrder.contains(cv)) {
					arrivedThisTurn.add(cv);
					int offset = mEffects.get(cv).onFinishLine();
					if (offset == CumulativeCardEffect.STOP_AT_FINISH_LINE) {
						mHorsesPositions.put(cv, Races.RACE_LENGHT);
						updateTheListenersForPositionChanged = true;
					} else if(offset != 0) {
						increaseValue(mHorsesPositions, cv, offset);
						updateTheListenersForPositionChanged = true;
					}
				}
			}
		}
		if(updateTheListenersForPositionChanged) {
			for(IRaceListener listener : mRaceListeners) {
				listener.onHorsePositionsChanged(mHorsesPositions);
			}
		}
		Collections.sort(arrivedThisTurn, new ArriviComparator(quotazione, mHorsesPositions, mEffects));
		mArrivalOrder.addAll(arrivedThisTurn);
		for(Horse horse : arrivedThisTurn) {
			for(IRaceListener listener : mRaceListeners) {
				// +1 because the indexes start at 0, but ordinal numbers start at 1
				listener.onHorseArrived(horse, mArrivalOrder.indexOf(horse) + 1);
			}
		}
	}

	/**
	 * This methods analyzes the cards assigned to the horses.
	 * It must be called before the {@link RaceBoard#raceRound(MovementCard, Quotations, boolean)} with the boolean argument true
	 * @param pBookmaker The Bookmaker of this game
	 * @param pListeners The listener to be notified if some changes happens
	 */
	public void resolveCardEffects(Bookmaker pBookmaker, List<? extends IActionCardListener> pListeners) {
		for (Horse cv : Horse.values()) {
			CumulativeCardEffect effect = new CumulativeCardEffect();
			List<ActionCard> toBeRemoved = effect.evaluateAssignedCards(mAssignedCards.get(cv));
			notifyActionCardListeners(pListeners, cv, mAssignedCards.get(cv), toBeRemoved);
			mEffects.put(cv, effect);
			if(effect.onInit() != 0) {
				pBookmaker.modifyQuotation(EnumUtils.getStableFromHorse(cv), effect.onInit());
			}
			mAssignedCards.removeAll(cv);
		}
		mAssignedCards.clear();
	}

	/**
	 * Register a new listener to be notified of changes
	 * @param pListener The listener to be registered
	 */
	public void registerListener(IRaceListener pListener) {
		mRaceListeners.add(pListener);
	}

	/**
	 * Removes a registered listener
	 * @param pListener The listener to remove
	 * @return True if the listener existed, false otherwise
	 */
	public boolean removeListener(IRaceListener pListener) {
		return mRaceListeners.remove(pListener);
	}

	private boolean isFirst(Horse horse) {
		int thisHorsePosition = mHorsesPositions.get(horse);
		for(Integer i : mHorsesPositions.values()) {
			if(i > thisHorsePosition) {
				return false;
			}
		}
		return true;
	}

	private boolean isLast(Horse horse) {
		int thisHorsePosition = mHorsesPositions.get(horse);
		for(Integer i : mHorsesPositions.values()) {
			if(i < thisHorsePosition) {
				return false;
			}
		}
		return true;
	}

	private void notifyActionCardListeners(List<? extends IActionCardListener> pListeners, Horse pHorse, Collection<IActionCard<?, ?>> pAssignedCards, List<ActionCard> pRemovedCards) {
		List<String> assignedCardNames = new LinkedList<String>();
		for(IActionCard<?, ?> card : pAssignedCards) {
			assignedCardNames.add(card.getName());
		}
		List<String> removedCardsName = new LinkedList<String>();
		for(ActionCard card : pRemovedCards) {
			removedCardsName.add(card.getName());
		}
		for(IActionCardListener listener : pListeners) {
			listener.onActionCardRevealed(pHorse, assignedCardNames, removedCardsName);
		}
	}

	private <K> void increaseValue(Map<K, Integer> pMap, K pKey, int pValue ) {
		int oldValue = pMap.get(pKey) != null ? pMap.get(pKey) : 0;
		pMap.put(pKey, oldValue + pValue);
	}

	/**
	 * This class handles the correct ordering of the arrived horses
	 * @author Francesco
	 *
	 */
	private static class ArriviComparator implements Comparator<Horse> {

		private final Quotations qt;
		private final EnumMap<Horse, Integer> horsePositions;
		private final EnumMap<Horse, CumulativeCardEffect> effects;

		ArriviComparator(Quotations quot, EnumMap<Horse, Integer> posizioni,
				EnumMap<Horse, CumulativeCardEffect> pEffetti) {
			qt = quot;
			horsePositions = posizioni;
			effects = pEffetti;
		}

		@Override
		public int compare(Horse o1, Horse o2) {

			Log.v("Comparo " + o1 + " e " + o2);
			int o1Pos = horsePositions.get(o1);
			int o2Pos = horsePositions.get(o2);
			if ( o1Pos > o2Pos) {
				Log.v("Prima " + o1 + " per pos: " + o1Pos + " vs " + o2Pos);
				return -1;
			} else if (o1Pos < o2Pos) {
				Log.v("Prima " + o2 + " per pos: " + o2Pos + " vs " + o1Pos);
				return +1;
			} else { // posizione uguale

				Boolean o1FF = effects.get(o1).onFotofinish();
				Boolean o2FF = effects.get(o2).onFotofinish();
				if ( o1FF != null) {
					boolean result = o1FF.booleanValue();
					Log.v("Prima " + (result ? o1 : o2) + " per eff: " + (o1FF ? "vince sempre" : "perde sempre"));
					return result ? -1 : +1;
				} else if ( o2FF != null) {
					boolean result = o2FF.booleanValue();
					Log.v("Prima " + (result ? o2 : o1) + " per eff: " + (o2FF ? "vince sempre" : "perde sempre"));
					return result ? +1 : -1;
				}

				int o1Quot = qt.getQuotation(o1);
				int o2Quot = qt.getQuotation(o2);
				if ( o1Quot > o2Quot) {
					Log.v("Prima " + o2 + " per quot: " + o2Quot + " vs " + o1Quot);
					return +1;
				} else if (qt.getQuotation(o1) < qt.getQuotation(o2)) {
					Log.v("Prima " + o1 + " per quot: " + o1Quot + " vs " + o2Quot);
					return -1;
				} else { // stessa quotazione: RANDOM RESULT!!
					Log.v("Random");
					return RandomUtils.getRandom().nextBoolean() ? -1 : +1;
				}
			}
		}
	}

	private EnumMap<Horse, Integer> deepCopyMap(Map<Horse, Integer> pOldMap) {
		EnumMap<Horse, Integer> returnMap = new EnumMap<Horse, Integer>(Horse.class);
		for(Map.Entry<Horse, Integer> entry : pOldMap.entrySet()) {
			returnMap.put(entry.getKey(), entry.getValue().intValue());
		}
		return returnMap;
	}

	private void sleepFor(int milliseconds) {
		if(!DebugUtils.isDebug()) {
			try {
				Thread.sleep(milliseconds);
			} catch (InterruptedException e) {
				Log.w("Should never happen. " + e.getMessage());
			}
		}
	}
	@Override
	public String toString() {
		return "Posizione cavalli: " + mHorsesPositions.toString() + ", Ordine di arrivo:" + mArrivalOrder.toString() + ", Carte: " + mAssignedCards.toString();
	}
}