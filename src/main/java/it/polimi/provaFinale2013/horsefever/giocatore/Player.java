package it.polimi.provaFinale2013.horsefever.giocatore;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import it.polimi.provaFinale2013.horsefever.carte.IActionCard;
import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.utils.EnumUtils;
import it.polimi.provaFinale2013.utils.Log;

/**
 * This class rappresent a player, with its moneys, pv and cards.
 * @author Francesco
 *
 */
public class Player {

	private int mMoney;
	private int mPv;
	private String mName;
	private final PlayerCard mPlayerCard;
	private final List<IActionCard<?,?>> mCardsAdviable;

	public Player(String pName, PlayerCard pPlayerCard) {
		mName = pName;
		mPlayerCard = pPlayerCard;
		mMoney = pPlayerCard.getMoney();
		mPv = 1;
		mCardsAdviable = new LinkedList<IActionCard<?,?>>();
	}

	/**
	 * @return The name of the player
	 */
	public String getName() {
		return mName;
	}

	/**
	 * @return The money owned by the player
	 */
	public int getMoney() {
		return mMoney;
	}

	/**
	 * Adds pMoney to the player total moneys.
	 * @param pMoney The money to give
	 */
	public void giveMoney(int pMoney) {
		if(pMoney < 0) {
			Log.w("You want to give money to the player, but you are taking them.");
		}
		mMoney += pMoney;
	}

	/**
	 * Takes pMoney to the player total moneys.
	 * @param pMoney The money to take
	 */
	public void takeMoney(int pMoney) {
		if(pMoney < 0) {
			Log.w("You want to take money from the player, but you are giving them");
		}
		mMoney -= pMoney;
	}

	/**
	 * @return How much PV the player has
	 */
	public int getPv() {
		return mPv;
	}

	/**
	 * Gives some pv to the player
	 * @param pPv The PV to give
	 */
	public void givePv(int pPv) {
		if(pPv < 0) {
			Log.w("You want to give pv to the player, but you are taking them");
		}
		mPv += pPv;
	}

	/**
	 * Takes some pv to the player
	 * @param pPv The PV to take
	 */
	public void takePv(int pPv) {
		if(pPv < 0) {
			Log.w("You want to take pv from the player, but you are giving them");
		}
		mPv -= pPv;
	}

	/**
	 * Give an {@link IActionCard} to the player, to place on an horse later on
	 * @param pActionCard The given card
	 */
	public void giveCard(IActionCard<?,?> pActionCard) {
		mCardsAdviable.add(pActionCard);
	}

	/**
	 * Gets the list of the available cards
	 * @return The list of the available cards
	 */
	public List<IActionCard<?,?>> getAvailableCards() {
		return Collections.unmodifiableList(mCardsAdviable);
	}

	/**
	 * Takes a card from the cards of the player
	 * @param pActionCard The action card to take
	 * @return True if the player owned the card, false otherwise
	 */
	public boolean takeCard(IActionCard<?,?> pActionCard) {
		return mCardsAdviable.remove(pActionCard);
	}

	/**
	 * @return The stabel owned by the player
	 */
	public Stable getStable() {
		return mPlayerCard.getAssociatedStable();
	}

	/**
	 * @return The horse corresponding to the owned stable
	 */
	public Horse getHorse() {
		return EnumUtils.getHorsefromStable(getStable());
	}

	/**
	 * @return The player card owned by the player
	 */
	public PlayerCard getPlayerCard() {
		return mPlayerCard;
	}

	@Override
	public String toString() {
		return (mName + " , " + getStable() + ", $ " + mMoney + " , PV " + mPv);
	}
}