package it.polimi.provaFinale2013.horsefever.giocatore;

import it.polimi.provaFinale2013.horsefever.carte.ICard;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.horsefever.scommesse.Quotations;

public class PlayerCard implements ICard {

	private final int mMoney;
	private final int mStableQuotation;
	private final Stable mStable;
	private final String mName;

	PlayerCard(String pName, int pMoney, int pStableQuotation, Quotations pQuotations) {
		mName = pName;
		mMoney = pMoney;
		mStableQuotation = pStableQuotation;
		mStable = pQuotations.getStableFromQuotation(mStableQuotation).get(0);
	}

	public int getMoney() {
		return mMoney;
	}

	public int getStableQuotation() {
		return mStableQuotation;
	}

	public Stable getAssociatedStable() {
		return mStable;
	}

	@Override
	public String getName() {
		return mName;
	}

	public static PlayerCard[] getPlayerCards(Quotations pQuotations) {
		PlayerCard[] playerCards = new PlayerCard[6];
		playerCards[0] = new PlayerCard("Cranio Mercanti", 3400, 2, pQuotations);
		playerCards[1] = new PlayerCard("Steve Mc Skull", 3600, 3, pQuotations);
		playerCards[2] = new PlayerCard("Viktor Von Schadel", 3800, 4, pQuotations);
		playerCards[3] = new PlayerCard("Cesan Crane", 4000, 5, pQuotations);
		playerCards[4] = new PlayerCard("Craneo Cervantes", 4200, 6, pQuotations);
		playerCards[5] = new PlayerCard("Sigvard Skalle", 4400, 7, pQuotations);

		return playerCards;
	}

	@Override
	public String toString() {
		return (mName + " , " + mStable.toString() + " , 1 : " + mStableQuotation + " , $ " + mMoney);
	}
}