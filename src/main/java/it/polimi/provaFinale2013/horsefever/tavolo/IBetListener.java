package it.polimi.provaFinale2013.horsefever.tavolo;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.horsefever.scommesse.KindOfBet;

public interface IBetListener extends IQuotationListener {
	/**
	 * This method is called to notify that some user placed a bet
	 * @param pPlayerName The player how placed the beet
	 * @param pHorse The horse on which the player bet
	 * @param pValue The ammount of money betted
	 * @param pKindOfBet The kind of bet
	 */
	void onPlacedBet(String pPlayerName, Horse pHorse, int pValue, KindOfBet pKindOfBet);
	/**
	 * This method is called to notify that a player won a bet
	 * @param pPlayerName The player who won
	 * @param pAmount The ammount of money won
	 * @param pPVs The pv won
	 * @param pKindOfBet The kind of bet guessed
	 */
	void onWonBet(String pPlayerName, int pAmount, int pPVs, KindOfBet pKindOfBet);
	/**
	 * This method is called to notify that a stable have been payed due to its resultes in the last race
	 * @param pStable The paid stable
	 * @param pAmount The ammount of money paied
	 */
	void onStablePaid(Stable pStable, int pAmount);
}
