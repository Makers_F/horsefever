package it.polimi.provaFinale2013.horsefever.tavolo;

import it.polimi.provaFinale2013.horsefever.carte.IActionCard;
import it.polimi.provaFinale2013.horsefever.carte.ICard;
import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.corse.Races;
import it.polimi.provaFinale2013.horsefever.giocatore.Player;
import it.polimi.provaFinale2013.horsefever.giocatore.PlayerCard;
import it.polimi.provaFinale2013.horsefever.scommesse.Bookmaker.AlreadyPresentBetException;
import it.polimi.provaFinale2013.horsefever.scommesse.KindOfBet;
import it.polimi.provaFinale2013.horsefever.scommesse.Quotations;
import it.polimi.provaFinale2013.utils.DebugUtils;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.utils.RandomUtils;
import it.polimi.provaFinale2013.view.IViewer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class TavoloDiGioco implements ITavoloDiGioco<IViewer> {
	private static final int MAX_NUMBER_OF_PLAYERS = 6;
	private static final int PV_TAKEN_ON_MISSED_BET = 2;
	private static final int MINIMUM_REQUIRED_PLAYERS = 1;
	private static final int MINIMUM_BET_PER_PV = 100;

	private final Map<String, Player> mPlayerNamesMap = new HashMap<String, Player>();
	private final LinkedList<Player> mSortedPlayers = new LinkedList<Player>();
	private final Races<IViewer> mRaces;
	private final List<PlayerCard> mPlayerCards;
	private int mNumberOfPlayers = 0;
	private final List<IViewer> mListeners = new LinkedList<IViewer>();

	public TavoloDiGioco() {
		mRaces = new Races<IViewer>();
		mPlayerCards = new ArrayList<PlayerCard>(Arrays.asList(PlayerCard.getPlayerCards(mRaces.getQuotations())));
		RandomUtils.shuffle(mPlayerCards);
	}

	@Override
	public void startMatch() {
		mRaces.getQuotations().notifyQuotations();
		RandomUtils.shuffle(mSortedPlayers);
		int numberOfTurns = setNumberOfTurns();
		Map<String, String> playerNameCardMap = new HashMap<String, String>();
		for(Map.Entry<String, Player> entry: mPlayerNamesMap.entrySet()) {
			playerNameCardMap.put(entry.getKey(), entry.getValue().getPlayerCard().getName());
		}
		for(IViewer listener : mListeners) {
			listener.onGameStarted(playerNameCardMap, numberOfTurns);
		}
		mainloop:
		for(int turno = 0 ; turno < numberOfTurns && mSortedPlayers.size() > MINIMUM_REQUIRED_PLAYERS; turno++) {
			// Assegna due carte a ogni giocatore
			assignCardsToPlayers();
			// Prepara le quotazioni
			Quotations quotations = mRaces.getQuotations();
			Map<Horse, Integer> horseQuotations = new HashMap<Horse, Integer>();
			for(Horse horse : Horse.values()) {
				horseQuotations.put(horse, quotations.getQuotation(horse));
			}
			// Prima scommessa
			for(Iterator<Player> it = mSortedPlayers.listIterator(); it.hasNext() ; ) {
				Player player = it.next();
				boolean removePlayer = makePlayerBet(player, horseQuotations, true);
				if(removePlayer){
					it.remove();
				}
				if(mSortedPlayers.size()==1){
					break mainloop;
				}
			}
			// Piazza Carte
			for(int n = 0; n < 2; n++) {
				for(Player player : mSortedPlayers) {
					List<String> cards = new LinkedList<String>();
					for(ICard card : player.getAvailableCards()) {
						cards.add(card.getName());
					}
					for(IViewer view : mListeners) {
						view.onExpectPlacedCard(player.getName(), cards);
					}
				}
			}
			// Seconda scommessa
			ListIterator<Player> it = mSortedPlayers.listIterator(mSortedPlayers.size());
			while(it.hasPrevious()) {
				Player player = it.previous();
				boolean removePlayer = makePlayerBet(player, horseQuotations, false);
				if(removePlayer){
					it.remove();
				}
				if(mSortedPlayers.size()==1){
					break mainloop;
				}
			}
			runRace();
			payPlayers();
			for(IViewer view : mListeners) {
				view.onTurnFinished();
			}
			advanceOrderOfPlayers();
		}
		// trova il vincitore
		checkWinner();
	}

	@Override
	public void registerPlayer(String pName) throws AlreadyRegisteredPlayerNameException, NotEnoughPlayerSlotsException {
		if(mNumberOfPlayers >= MAX_NUMBER_OF_PLAYERS) {
			throw new NotEnoughPlayerSlotsException();
		}
		if(mPlayerNamesMap.containsKey(pName)) {
			throw new AlreadyRegisteredPlayerNameException(pName);
		}
		PlayerCard playerCard = mPlayerCards.get(mNumberOfPlayers);
		Player newPlayer = new Player(pName, playerCard);
		mPlayerNamesMap.put(pName, newPlayer);
		mSortedPlayers.add(newPlayer);
		mNumberOfPlayers++;
	}

	@Override
	public void registerBet(String pPlayerName, Horse pHorse, int pAmount, KindOfBet pKindOfBet) throws AlreadyPresentBetException {
		Player player = getPlayerFromName(pPlayerName);
		if(pAmount < player.getPv() * MINIMUM_BET_PER_PV || pAmount > player.getMoney()) {
			Log.e("You should respect the limits of the rules. The bet is out of range.");
		}
		mRaces.registerBet(player, pHorse, pAmount, pKindOfBet);
		player.takeMoney(pAmount);
	}

	@Override
	public void assignCardToHorse(String pPlayerName, String pCardName, Horse pHorse) throws PlayerDoNotOwnSpecifiedCardException {
		Player player = getPlayerFromName(pPlayerName);
		List<IActionCard<?,?>> playerActionCards = player.getAvailableCards();
		for(IActionCard<?,?> actionCard : playerActionCards) {
			if(actionCard.getName().equals(pCardName)) {
				player.takeCard(actionCard);
				mRaces.assignCardToHorse(actionCard, pHorse);
				for(IViewer listener : mListeners) {
					listener.onActionCardAssignedToHorse(pPlayerName, pCardName, pHorse);
				}
				return;
			}
		}
		throw new PlayerDoNotOwnSpecifiedCardException(pPlayerName, pCardName);
	}

	@Override
	public void registerListener(IViewer pListener) {
		mListeners.add(pListener);
		mRaces.registerListener(pListener);
	}

	@Override
	public boolean removeListener(IViewer pListener) {
		return mListeners.remove(pListener) &&
				mRaces.removeListener(pListener);
	}

	/**
	 * Creato per potere fare i test
	 * @return the number of players registered
	 */
	public int getNumberOfPlayers() {
		return mSortedPlayers.size();
	}

	/**
	 * Since next turn the first player is the last one, and the second one becoomes the first one, make the player circulate around the list
	 */
	private void advanceOrderOfPlayers() {
		Player oldFirst = mSortedPlayers.removeFirst();
		mSortedPlayers.addLast(oldFirst);
	}

	private void checkWinner() {
		int maxPv = 0;
		for(Player player : mSortedPlayers) {
			maxPv = Math.max(maxPv, player.getPv());
		}
		List<Player> winners = new LinkedList<Player>();
		for(Player player :  mSortedPlayers) {
			if(player.getPv() >= maxPv) {
				winners.add(player);
			}
		}
		RandomUtils.shuffle(winners);
		for(IViewer listener : mListeners) {
			if(winners.isEmpty()) {
				listener.onEndOfGame("No one");
			} else {
				listener.onEndOfGame(winners.get(0).getName());
			}
		}
	}

	private boolean makePlayerBet(Player pPlayer, Map<Horse, Integer> pHorseQuotations, boolean pIsMandatory) {
		String playerName = pPlayer.getName();
		int minBet = pPlayer.getPv() * 100;
		int maxBet = pPlayer.getMoney();
		if(pIsMandatory && minBet > maxBet) {
			pPlayer.takePv(PV_TAKEN_ON_MISSED_BET);
			if(pPlayer.getPv() <= 0 ) {
				for(IViewer view : mListeners) {
					view.onPlayerEliminated(playerName);
					mNumberOfPlayers--;
				}
				return true;
			} else {
				for(IViewer view : mListeners) {
					view.onRemovedPVInsteadOfBet(playerName);
				}
			}
		} else {
			if(minBet < maxBet) {
				for(IViewer view : mListeners) {
					boolean hasPlaced = view.onExpectBet(pPlayer.getName(), minBet, maxBet, pHorseQuotations, pIsMandatory);
					if(!hasPlaced) {
						Log.i("Tavolo", pPlayer.getName() + " did not bet.");
					}
				}
			} // non e' obbligatoria, non chiedere niente
		}
		return false;
	}

	/**
	 * Assigns one {@link ActionCard} to each {@link Player}
	 */
	private void assignCardsToPlayers() {
		for(Player player : mSortedPlayers) {
			for(int n = 0; n < 2; n++) {
				IActionCard<?,?> actionCard = mRaces.getActionCardForPlayer();
				player.giveCard(actionCard);
				Log.i("Tavolo", player.getName() + " received " + actionCard.getName());
				for(IViewer listener : mListeners) {
					listener.onActionCardGivenToPlayer(player.getName(), actionCard.getName());
				}
			}
		}
	}

	private void runRace() {
		mRaces.startRace();
		while(!mRaces.isRaceFinished()) {
			mRaces.runARoundOfRace();
		}
		mRaces.endOfRace(mSortedPlayers);
	}

	private void payPlayers() {
		if(!mRaces.isRaceFinished()) {
			throw new IllegalStateException("La gara non e' ancora finita!");
		}
		for(Player player : mSortedPlayers) {
			mRaces.payBet(player);
		}
	}

	private Player getPlayerFromName(String pPlayerName) {
		Player player = mPlayerNamesMap.get(pPlayerName);
		if(player == null) {
			throw new IllegalArgumentException("Non esiste un giocatore con nome \"" + pPlayerName + "\".");
		}
		return player;
	}

	private int setNumberOfTurns() {
		if (mNumberOfPlayers == 2 || mNumberOfPlayers == 3) {
			if(DebugUtils.isDebug()) {
				return 3;
			}
			return 6;
		} else {
			return mNumberOfPlayers;
		}
	}

	@Override
	public String toString() {
		return "Num. giocatori: " + mNumberOfPlayers + ", " + mPlayerNamesMap.toString() + ", " + mPlayerCards + ", " + mRaces.toString();
	}
}
