package it.polimi.provaFinale2013.horsefever.tavolo;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.scommesse.Bookmaker.AlreadyPresentBetException;
import it.polimi.provaFinale2013.horsefever.scommesse.KindOfBet;

/**
 * This interface reppresent the full state of the game.
 * The player must interface with the classes which implent this interface only,
 * an all the input must be hadled by it.
 * @author Francesco
 *
 * @param <T> The type of the listener to be notified when registerd
 */
public interface ITavoloDiGioco<T extends IActionCardListener & IBetListener & IRaceListener> {

	/**
	 * Register a player to play the game
	 * @param pName The name of the player
	 * @throws AlreadyRegisteredPlayerNameException Thrown if already exists a player with the same name
	 * @throws NotEnoughPlayerSlotsException Thrown if the player limit has been reached
	 */
	void registerPlayer(String pName) throws AlreadyRegisteredPlayerNameException, NotEnoughPlayerSlotsException;
	/**
	 * Starts the match
	 */
	void startMatch();
	/**
	 * Register a bet
	 * @param pPlayerName The player betting.
	 * @param pHorse The horse on which the player betted.
	 * @param pAmount The amoutn on mony betted.
	 * @param pKindOfBet The kind of bet placed.
	 * @throws AlreadyPresentBetException Thrown if the player already made a bet of the same kinf on the same horse.
	 */
	void registerBet(String pPlayerName, Horse pHorse, int pAmount, KindOfBet pKindOfBet) throws AlreadyPresentBetException;
	/**
	 * Assign a card owned by a player to a horse.
	 * @param pPlayerName The assigning player.
	 * @param pCardName The name of the card.
	 * @param pHorse The horse to which it will be assigned.
	 * @throws PlayerDoNotOwnSpecifiedCardException Thrown if the player do not own the card he tried to assign. 
	 */
	void assignCardToHorse(String pPlayerName, String pCardName, Horse pHorse) throws PlayerDoNotOwnSpecifiedCardException;
	/**
	 * Registers a listener which will be notified of game events.
	 * @param pListener The listener to register
	 */
	void registerListener(T pListener);
	/**
	 * Remove a registered listener.
	 * @param pListener The listener to remove.
	 * @return True if the listener was registered, false otherwise
	 */
	boolean removeListener(T pListener);

	/**
	 * It indicates that a player has already been registered with the name someone just tried to use
	 * @author Francesco
	 *
	 */
	public static class AlreadyRegisteredPlayerNameException extends Exception {
		private static final long serialVersionUID = -3361276915685485430L;
		public AlreadyRegisteredPlayerNameException(String pAlreadyUsedName) {super("Il nome \"" + pAlreadyUsedName + "\" e' gia' stato usato.");}
	}

	/**
	 * Indicates that the game already reached the maximum number of players allowed to play
	 * @author Francesco
	 *
	 */
	public static class NotEnoughPlayerSlotsException extends Exception {
		private static final long serialVersionUID = -4089361820166812329L;
		public NotEnoughPlayerSlotsException() {super("Already reached the max number of players.");}
	}

	/**
	 * Indicates that a player tried to play a card which s/he do not own
	 * @author Francesco
	 *
	 */
	public static class PlayerDoNotOwnSpecifiedCardException extends Exception {
		private static final long serialVersionUID = -7149980212375605583L;
		public PlayerDoNotOwnSpecifiedCardException(String pPlayerName, String pCardName) {super("Il giocatore " + pPlayerName + " non possiede la carta " + pCardName + ".");}
	}
}
