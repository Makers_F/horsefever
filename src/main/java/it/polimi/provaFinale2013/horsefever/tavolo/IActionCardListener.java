package it.polimi.provaFinale2013.horsefever.tavolo;

import java.util.List;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;

public interface IActionCardListener {
	/**
	 * This method is called to notify that a player received a card
	 * @param pPlayerName The player who received the card
	 * @param pCardName The card received
	 */
	void onActionCardGivenToPlayer(String pPlayerName, String pCardName);
	/**
	 * This method is called to notify that a card has been assigned to an horse
	 * @param pPlayerName The player assigning the card
	 * @param pCardName The card name
	 * @param pHorse The horse which receives the card
	 */
	void onActionCardAssignedToHorse(String pPlayerName, String pCardName, Horse pHorse);
	/**
	 * This method is called to notify that the cards assigned to a specific horse has been revealed
	 * @param pHorse The horse to which the cards have been assigned
	 * @param pActionCardsAssigned The assigned cards
	 * @param pActionCardsRemoved All the removed cards due to incompatibilities or other cards effects
	 */
	void onActionCardRevealed(Horse pHorse, List<String> pActionCardsAssigned, List<String> pActionCardsRemoved);
}
