package it.polimi.provaFinale2013.horsefever.tavolo;

import it.polimi.provaFinale2013.horsefever.cavalli.Stable;

import java.util.Map;

public interface IQuotationListener {
	/**
	 * This method is called to notify that the quotation of a single stable has changed
	 * @param pStable The stable which changed its quotation
	 * @param pOldQuotation The old quotation
	 * @param pNewQuotation The new Quotation
	 */
	void onSingleQuotationChanged(Stable pStable, int pOldQuotation, int pNewQuotation);
	/**
	 * This method notify that all the stables quotations have been updated
	 * @param pHorseToQuotationMap The map that maps from the stable to its new quotation
	 */
	void onFullQuotationUpdate(Map<Stable, Integer> pHorseToQuotationMap);
}
