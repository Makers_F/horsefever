package it.polimi.provaFinale2013.horsefever.tavolo;

import it.polimi.provaFinale2013.horsefever.cavalli.Horse;

import java.util.Map;

public interface IRaceListener {
	/**
	 * This method is called to notify that a race has started
	 */
	void onRaceStarted();
	/**
	 * This method is called to notify that a new movement card have been revealed
	 * @param pMovementCardValues The values of how much the horses will have to move due to the movement card
	 */
	void onMovementCardRevealed(int[] pMovementCardValues);
	/**
	 * This method is called to notify that an horse sprinted
	 * @param pHorse The horse which sprinteed
	 * @param pHowMuchTheHorseSprinted How much the sprint was
	 */
	void onSprint(Horse pHorse, int pHowMuchTheHorseSprinted);
	/**
	 * This method is called to notify that at least an horse chaned its position
	 * @param pHorseToPositionMap The map whici�h maps the horse to its current position
	 */
	void onHorsePositionsChanged(Map<Horse, Integer> pHorseToPositionMap);
	/**
	 * This method is called to notify that a horse finished the race
	 * @param pHorse The horse which arrived
	 * @param pHorsePosition the position in the order of arrival of the horses
	 */
	void onHorseArrived(Horse pHorse, int pHorsePosition);
	/**
	 * This method is called to notify that a race finished
	 * @param pArrivalOrderedHorses The arrival order of the horses
	 */
	void onRaceFinished(Horse[] pArrivalOrderedHorses);
}
