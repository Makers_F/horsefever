package it.polimi.provaFinale2013.horsefever.carte;

/**
 * An effect of an action card
 * @author Francesco
 *
 * @param <T> The trigger of the card
 */
public interface IActionEffect<T extends Enum<T>> {
	/**
	 * @return Return a description of what and when the card trigger
	 */
	T getEffect();
	/**
	 * @return True if the intensity is to be summed to a default value, or it is a fixed value
	 */
	boolean isOffset();
	/**
	 * @return The intensity of the effect
	 */
	int getIntensity();
}
