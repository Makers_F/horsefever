package it.polimi.provaFinale2013.horsefever.carte;


import it.polimi.provaFinale2013.utils.RandomUtils;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Reppresent a deck of cards
 * @author Francesco
 *
 * @param <T> The kind of card contained on the deck
 */
public class Deck<T extends ICard> {

	private final LinkedList<T> mDeck = new LinkedList<T>();

	Deck() {
	}

	/**
	 * reset the deck to its original state
	 */
	public void reset() {
		shuffle();
	}

	/**
	 * Shuffles the deck
	 */
	public void shuffle() {
		RandomUtils.shuffle(mDeck);
	}

	/**
	 * Mischia il mazzo passato con quello attuale. NOTA: Il mazzo passato viene svuotato
	 * @param pDeck Il mazzo da mischiare
	 */
	public void shuffle(Deck<T> pDeck) {
		mDeck.addAll(pDeck.mDeck);
		pDeck.mDeck.clear();
		shuffle();
	}

	/**
	 * Mischia le carte passate dentro il mazzo
	 * @param pDeck La collezione di nuove carte da mischiare
	 */
	public void shuffle(Collection<T> pDeck) {
		mDeck.addAll(pDeck);
		shuffle();
	}

	/**
	 * Draws a card from the top of the deck
	 * @return The drawn card
	 */
	public T draw() {
		return mDeck.poll();
	}

	/**
	 * Puts a card on top of the deck
	 * @param card The card to put on top
	 */
	public void putOnTheTop(T card) {
		mDeck.addLast(card);
	}

	/**
	 * Puts a card on the bottom of the deck
	 * @param card The card to put at the bottom
	 */
	public void putOnTheBottom(T card) {
		mDeck.addFirst(card);
	}

	@Override
	public String toString() {
		return "Deck: " + mDeck.toString()  + " , " + mDeck.size();
	}
}
