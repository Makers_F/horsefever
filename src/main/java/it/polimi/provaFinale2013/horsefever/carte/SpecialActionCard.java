package it.polimi.provaFinale2013.horsefever.carte;


/**
 * This class reppresent a special card.
 * @author Francesco
 *
 */
public class SpecialActionCard implements IActionCard<SpecialActionCard.SpecialEffect, SpecialActionCard.SpecialEffectAction> {

	private final SpecialEffect mSpecialEffect;
	private final String mName;
	private final boolean mIsMalus;

	public SpecialActionCard(String pName, boolean pIsMalus,
			SpecialEffect pEffect) {
		mName = pName;
		mIsMalus = pIsMalus;
		mSpecialEffect = pEffect;
	}


	@Override
	public String getName() {
		return mName;
	}

	@Override
	public boolean isMalus() {
		return mIsMalus;
	}

	@Override
	public char getLetter() {
		return 'Z';
	}

	@Override
	public SpecialEffect getEffect() {
		return mSpecialEffect;
	}

	@Override
	public String toString() {
		return "[Spec] " + mName + ": " + (isMalus() ? "malus" : "bonus") +  " , " + mSpecialEffect.toString();
	}

	/**
	 * This class reppresent the effect of one of the special cards of HorseFever
	 * @author Francesco
	 *
	 */
	public static class SpecialEffect implements IActionEffect<SpecialActionCard.SpecialEffectAction> {

		private final SpecialEffectAction mSpecialEffectAction;
		private final boolean mIsOffset;
		private final int mIntensity;

		public SpecialEffect(SpecialEffectAction pSpecialEffectAction, boolean pIsOffset, int pIntensity) {
			mSpecialEffectAction = pSpecialEffectAction;
			mIsOffset = pIsOffset;
			mIntensity = pIntensity;
		}

		@Override
		public SpecialEffectAction getEffect() {
			return mSpecialEffectAction;
		}

		@Override
		public boolean isOffset() {
			return mIsOffset;
		}

		@Override
		public int getIntensity() {
			return mIntensity;
		}

		@Override
		public String toString() {
			return mSpecialEffectAction.name() + " , " + (mIsOffset ? (mIntensity >= 0 ? "+" : "-") : "") + Math.abs(mIntensity);
		}
	}
	
	public static enum SpecialEffectAction{
		MODIFY_QUOTATIONS,
		MODIFY_ACTION_CARDS;
	}
}
