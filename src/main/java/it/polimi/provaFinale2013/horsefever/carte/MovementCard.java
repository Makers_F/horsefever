package it.polimi.provaFinale2013.horsefever.carte;

import java.util.Arrays;

/**
 * This class rappresent a movement card, with its movement values
 * @author Francesco
 *
 */
public class MovementCard implements ICard {

	private final int[] mMovements;

	public MovementCard(int pFirst, int pSecond, int pThird, int pFourth, int pFifth, int pSixth) {
		mMovements = new int[] {pFirst, pSecond, pThird, pFourth, pFifth, pSixth};
	}

	/**
	 * @return The value of how much each horse should move based on its quotations, from the quotation 1:2 to 1:7
	 */
	public int[] getMovements() {
		return mMovements;
	}

	@Override
	public String getName() {
		return "Movimento: " + Arrays.toString(mMovements);
	}

	@Override
	public String toString() {
		return getName();
	}
}