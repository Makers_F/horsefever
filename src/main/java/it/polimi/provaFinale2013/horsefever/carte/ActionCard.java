package it.polimi.provaFinale2013.horsefever.carte;

/**
 * This class rappresent a normal action card
 * @author Francesco
 *
 */
public class ActionCard implements IActionCard<ActionCard.Effect, ActionCard.Trigger_Effect>{

	private final Effect mEffect;
	private final char mLetter;
	private final String mName;
	private final boolean mIsMalus;

	public ActionCard(String pName, boolean pIsMalus, char pLetter, Effect pEffect) {
		mName = pName;
		mLetter = pLetter;
		mIsMalus = pIsMalus;
		mEffect = pEffect;
	}

	@Override
	public String getName() {
		return mName;
	}

	@Override
	public boolean isMalus() {
		return mIsMalus;
	}

	@Override
	public Effect getEffect() {
		return mEffect;
	}

	@Override
	public char getLetter() {
		return mLetter;
	}

	@Override
	public int hashCode() {
		return this.getName().hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof ActionCard) {
			ActionCard ac = (ActionCard) o;
			return this.getName().equals(ac.getName());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return mName + ": " + (isMalus() ? "malus" : "bonus") + " , " + mLetter + " , " + mEffect.toString();
	}

	/**
	 * This class rappresent an effect of a normal action card.
	 * @author Francesco
	 *
	 */
	public static class Effect implements IActionEffect<Trigger_Effect>{

		private final Trigger_Effect when;
		private final boolean isOffset;
		private final int intensity;

		public Effect(Trigger_Effect pWhen, boolean pIsOffset, int pHowMuch) {
			when = pWhen;
			isOffset = pIsOffset;
			intensity = pHowMuch;
		}

		@Override
		public String toString() {
			return when.name() + " , " + (isOffset ? (intensity >= 0 ? "+" : "-") : "=") + intensity;
		}

		@Override
		public Trigger_Effect getEffect() {
			return when;
		}

		@Override
		public boolean isOffset() {
			return isOffset;
		}

		@Override
		public int getIntensity() {
			return intensity;
		}
	}

	public static enum Trigger_Effect {
		START,
		MOVEMENT,
		SPRINT,
		FINISHLINE,
		FOTOFINISH;
	}
}
