package it.polimi.provaFinale2013.horsefever.carte;

import java.util.LinkedList;

public class  ConservativeDeck<T extends ICard> extends Deck<T> {

	private final LinkedList<T> mCardsOutOfDeck = new LinkedList<T>();

	@Override
	public void reset() {
		super.shuffle(mCardsOutOfDeck);
		mCardsOutOfDeck.clear();
		super.reset();
	}

	@Override
	public T draw() {
		T card = super.draw();
		mCardsOutOfDeck.push(card);
		return card;
	}
}
