package it.polimi.provaFinale2013.horsefever.carte;

/**
 * This class rappresent a generic card of the game
 * @author Francesco
 *
 */
public interface ICard {
	/**
	 * @return The name of the card
	 */
	String getName();
}
