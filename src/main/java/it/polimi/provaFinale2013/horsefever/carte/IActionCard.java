package it.polimi.provaFinale2013.horsefever.carte;

/**
 * This card rappresent an action card with an associated effect
 * @author Francesco
 *
 * @param <T> The effect of the action card
 * @param <E> The trigger of the action card
 */
public interface IActionCard<T extends IActionEffect<E>, E extends Enum<E>> extends ICard {
	/**
	 * @return True if it's a Malus ActionCard, false if it's a Bonus ActionCard
	 */
	boolean isMalus();

	/**
	 * @return The letter of the card
	 */
	char getLetter();

	/**
	 * @return The effect of the card
	 */
	T getEffect();
}
