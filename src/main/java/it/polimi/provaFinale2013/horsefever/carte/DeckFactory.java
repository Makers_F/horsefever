package it.polimi.provaFinale2013.horsefever.carte;

import it.polimi.provaFinale2013.horsefever.carte.ActionCard.Effect;
import it.polimi.provaFinale2013.horsefever.carte.SpecialActionCard.SpecialEffect;
import it.polimi.provaFinale2013.horsefever.carte.SpecialActionCard.SpecialEffectAction;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.utils.ResourcesUtils;
import it.polimi.provaFinale2013.utils.XMLUtils;

import java.io.IOException;
import java.io.InputStream;

import org.w3c.dom.Element;

public class DeckFactory {

	private static final String MOVEMENT_CARD = "MovementCard";
	private static final String SLOTS = "slots";
	private static final String[] DISPOSITION = new String[] {
		"first","second","third","fourth","fifth","sixth"
	};

	private static final String ACTION_CARD = "ActionCard";
	private static final String VALUE = "value";
	private static final String NAME = "name";
	private static final String IS_MALUS= "isMalus";
	private static final String LETTER = "letter";
	private static final String EFFECT = "effect";
	private static final String SPECIAL = "special";
	private static final String WHEN = "when";
	private static final String IS_OFFSET= "isOffset";
	private static final String INTENSITY= "intensity";
	private static final String TRUE = "true";
	private static final String ACTION = "action";

	public static <T extends ICard> Deck<T> getDeck() {
		return new Deck<T>();
	}

	public static <T extends ICard> Deck<T> getDeck(Class<T> clazz) {
		return new Deck<T>();
	}

	public static Deck<MovementCard> getMovementCardsDeck(boolean pConservative) {
		Deck<MovementCard> deck = pConservative ? new ConservativeDeck<MovementCard>() : new Deck<MovementCard>();
		InputStream xmlFile = ResourcesUtils.getResourceInputStream(ResourcesUtils.MOVEMENT_CARD_XML);

		Element root = XMLUtils.parseAndGetRoot(xmlFile);

		//carte contiene l'elenco delle carte da aggiungere
		Element[] cards = XMLUtils.elements(root, MOVEMENT_CARD);
		for(Element card : cards) {
			//per ogni carta, prima di tutti leggi le informazione per instanziarne la classe
			int[] advancement = new int[6];
			int i = 0;
			for(String order : DISPOSITION) {
				Element number = (Element) XMLUtils.getFirstNodeInstance(card, order);
				advancement[i++] = Integer.parseInt(number.getAttribute(SLOTS));
			}
			deck.putOnTheTop(new MovementCard(advancement[0], advancement[1], advancement[2],
					advancement[3], advancement[4], advancement[5]));
		}
		try {
			xmlFile.close();
		} catch (IOException e) {
			Log.e(e);
		}
		deck.shuffle();

		return deck;
	}

	public static Deck<IActionCard<?,?>> getActionCardsDeck(boolean pConservative) {
		Deck<IActionCard<?,?>> deck = pConservative ? new ConservativeDeck<IActionCard<?,?>>() : new Deck<IActionCard<?,?>>();
		InputStream xmlFile = ResourcesUtils.getResourceInputStream(ResourcesUtils.ACTION_CARD_XML);
		Element root = XMLUtils.parseAndGetRoot(xmlFile);

		Element[] cards = XMLUtils.elements(root, ACTION_CARD);
		for(Element card : cards) {
			Element nameElement = (Element) XMLUtils.getFirstNodeInstance(card, NAME);
			String name = nameElement.getAttribute(VALUE);

			Element isMalusElement = (Element) XMLUtils.getFirstNodeInstance(card, IS_MALUS);
			boolean isMalus = isMalusElement.getAttribute(VALUE).equals(TRUE);

			Element letterElement = (Element) XMLUtils.getFirstNodeInstance(card, LETTER);
			char letter = letterElement.getAttribute(VALUE).charAt(0);

			Element effectElement = (Element) XMLUtils.getFirstNodeInstance(card, EFFECT);
			boolean isSpecial = effectElement.getAttribute(SPECIAL).equals(TRUE);

			if(!isSpecial) {
				Element whenElement = (Element) XMLUtils.getFirstNodeInstance(effectElement, WHEN);
				ActionCard.Trigger_Effect when = ActionCard.Trigger_Effect.valueOf(ActionCard.Trigger_Effect.class, whenElement.getAttribute(VALUE));

				Element isOffsetElement = (Element) XMLUtils.getFirstNodeInstance(effectElement, IS_OFFSET);
				boolean isOffset = isOffsetElement.getAttribute(VALUE).equals(TRUE);

				Element intensityElement = (Element) XMLUtils.getFirstNodeInstance(effectElement, INTENSITY);
				int intesity = Integer.parseInt(intensityElement.getAttribute(VALUE));
				deck.putOnTheTop(new ActionCard(name, isMalus, letter, new Effect(when, isOffset, intesity)));
			} else {

				Element actionElement = (Element) XMLUtils.getFirstNodeInstance(effectElement, ACTION);
				SpecialActionCard.SpecialEffectAction action = SpecialActionCard.SpecialEffectAction.valueOf(SpecialEffectAction.class, actionElement.getAttribute(VALUE));

				Element isOffsetElement = (Element) XMLUtils.getFirstNodeInstance(effectElement, IS_OFFSET);
				boolean isOffset = isOffsetElement.getAttribute(VALUE).equals(TRUE);

				Element intensityElement = (Element) XMLUtils.getFirstNodeInstance(effectElement, INTENSITY);
				int intesity = Integer.parseInt(intensityElement.getAttribute(VALUE));

				deck.putOnTheTop(new SpecialActionCard(name, isMalus, new SpecialEffect(action, isOffset, intesity)));
			}
		}
		try {
			xmlFile.close();
		} catch (IOException e) {
			Log.e(e);
		}
		deck.shuffle();
		return deck;
	}
}