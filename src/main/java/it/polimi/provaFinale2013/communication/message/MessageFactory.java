package it.polimi.provaFinale2013.communication.message;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParcel;
import it.polimi.provaFinale2013.utils.EnumUtils;

import java.io.DataInputStream;
import java.io.IOException;

public class MessageFactory {

	public static Message createMessage(MessageCode pMessageCode, String pPlayerString, byte[] pMessageContent) {
		return new Message(pMessageCode, pPlayerString, pMessageContent);
	}

	public static Message createMessage(MessageCode pMessageCode, String pPlayerName, IParcel pMessageParcel) {
		return new Message(pMessageCode, pPlayerName, pMessageParcel == null ? null : pMessageParcel.contenutoPacchetto());
	}

	public static Message createMessage(MessageCode pMessageCode, String pPlayerName) {
		return new Message(pMessageCode, pPlayerName, null);
	}

	public static Message createMessage(DataInputStream inputSteam) throws IOException {
		final int code = inputSteam.readInt();
		final MessageCode messageCode = EnumUtils.getEnumFromIndex(code, MessageCode.class);
		final String playerName = inputSteam.readUTF();
		final int length = inputSteam.readInt();
		if ( length == 0) {
			return new Message(messageCode, playerName, null);
		} else {
			byte[] messageContent = new byte[length];
			final int readBytes = inputSteam.read(messageContent);
			if(readBytes != length){
				throw new IOException("The steam had not enought bytes.");
			}
			return new Message(messageCode, playerName, messageContent);
		}
	}
}
