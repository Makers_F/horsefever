package it.polimi.provaFinale2013.communication.message;

import it.polimi.provaFinale2013.utils.EnumUtils;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * This class rappresent a message to be sent over the internet
 * @author Francesco
 *
 */
public class Message {

	final private MessageCode mCode;
	final private String mPlayerName;
	final private byte[] mContent;

	protected Message (MessageCode messageCode, String pPlayerName, byte[] messageContent) {
		mCode = messageCode;
		mContent = messageContent != null ? messageContent.clone() : null;
		mPlayerName = pPlayerName;
	}

	/**
	 * Sends the message
	 * @param stream The stream to use to send the message
	 * @throws IOException If an error occours
	 */
	public void send(DataOutputStream stream) throws IOException {
		stream.writeInt(EnumUtils.getIndex(mCode));
		stream.writeUTF(mPlayerName);
		int length = mContent == null ? 0 : mContent.length;
		stream.writeInt(length);
		if (mContent != null){
			stream.write(mContent);
		}
		stream.flush();
	}

	/**
	 * @return The code of the message
	 */
	public MessageCode getCode() {
		return mCode;
	}

	/**
	 * @return The content of this message
	 */
	public byte[] getContent() {
		return mContent;
	}

	/**
	 * @return The name of the player sending this message
	 */
	public String getPlayer() {
		return mPlayerName;
	}

	@Override
	public String toString() {
		return "Message: " + getCode() + ", " + getPlayer() + ", content length:" + getContent().length;
	}
}
