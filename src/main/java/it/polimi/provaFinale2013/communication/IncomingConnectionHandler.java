package it.polimi.provaFinale2013.communication;

import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.view.IViewer;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashSet;
import java.util.Set;

/**
 * This class handles the incoming connetion to the server.
 * Each time a connection is received, a new {@link SingleIViewerServer} is created.
 * Once all the connected clients are ready, the class spawns a thread in which the game will happen and terminate its own thread.
 * @author Francesco
 *
 */
public class IncomingConnectionHandler extends Thread {

	private ServerSocket mServerSocket;
	private final ITavoloDiGioco<IViewer> mTavoloDiGioco;
	private final Set<Socket> mWaitingForBeforeStart = new HashSet<Socket>();
	private boolean mClose = false;

	public IncomingConnectionHandler(ITavoloDiGioco<IViewer> pTavoloDiGioco, int pPort) throws IOException, BindException {
		mServerSocket = new ServerSocket(pPort);
		mServerSocket.setSoTimeout(5000);
		mTavoloDiGioco = pTavoloDiGioco;
	}

	@Override
	public void run() {
		while(!mClose) {
			try {
				Socket socket = mServerSocket.accept();
				SingleIViewerServer sch = new SingleIViewerServer(mTavoloDiGioco, this, socket);
				sch.start();
			} catch (SocketTimeoutException e) {
				continue;
			}
			catch (IOException e) {
				Log.e(e);
			}
		}
	}

	/**
	 * Called to signal that a socket registered at least a player in the game, so we need to wait for it to fire the
	 * start game signal.
	 * @param pCallerSocket The registered sockete
	 */
	public synchronized void registerDependencyOnSocket(Socket pCallerSocket) {
		boolean alreadyContainsSocket = mWaitingForBeforeStart.contains(pCallerSocket);
		if(!alreadyContainsSocket) {
			mWaitingForBeforeStart.add(pCallerSocket);
		}
	}

	public synchronized void startGame(Socket pCallerSocket) {
		mWaitingForBeforeStart.remove(pCallerSocket);
		if(mWaitingForBeforeStart.size() == 0) {
			close();
			// a bit ungly to see, but i choose to make it this way so that we have a new thread
			// that handles the game, and it is not the last SingleClientHandler to call this method
			// the thread were all the execution happens. This way all the SingleClientHandlers terminates
			// and we are left with just this single thread.
			Runnable gameRunnable = new Runnable() {
				@Override
				public void run() {
					mTavoloDiGioco.startMatch();
				}
			};
			new Thread(gameRunnable, "GameThread").start();
		}
	}
	public void close() {
		mClose = true;
	}
}
