package it.polimi.provaFinale2013.communication;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParcel;
import it.polimi.provaFinale2013.communication.marshalling.parcel.ParcelFactory;
import it.polimi.provaFinale2013.communication.message.Message;
import it.polimi.provaFinale2013.communication.message.MessageCode;
import it.polimi.provaFinale2013.communication.message.MessageFactory;
import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.horsefever.scommesse.Bookmaker.AlreadyPresentBetException;
import it.polimi.provaFinale2013.horsefever.scommesse.KindOfBet;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.utils.parcel.ListParceler;
import it.polimi.provaFinale2013.utils.parcel.MapParceler;
import it.polimi.provaFinale2013.utils.parcel.ParcelUtils;
import it.polimi.provaFinale2013.view.IViewer;

/**
 * This class acts as a proxy for the client.
 * This class interfaces with the game as it was the player, while in reality it is acting as a bridge between the
 * server and the client, in a total trasparent way.
 * @author Francesco
 *
 */
public class ClientSideITavolo implements ITavoloDiGioco<IViewer> {

	private static final String GENERAL_CLIENT_PLAYER = "anonymous player";
	private final String mAddress;
	private final int mPort;
	private final List<IViewer> mListeners = new LinkedList<IViewer>();
	private DataInputStream mTavoloInputStream;
	private DataOutputStream mTavoloOutputStream;
	private boolean mClose;

	public ClientSideITavolo(String pAddress, int pPort) {
		mAddress = pAddress;
		mPort = pPort;
	}

	/**
	 * This method connect the client with the server and start to wait for the updates.
	 * @throws UnknownHostException if the IP address of the host could not be determined.
	 * @throws IOException if something bad happens during the connection
	 */
	public void startListening () throws IOException, UnknownHostException {
		Socket socket = new Socket(mAddress, mPort);
		mTavoloInputStream = new DataInputStream(socket.getInputStream());
		mTavoloOutputStream = new DataOutputStream(socket.getOutputStream());
		while(!mClose) {
			waitForUpdate();
		}
		socket.close();
	}

	private void stopListening() {
		mClose = true;
	}

	@Override
	public void registerPlayer(String pName) throws AlreadyRegisteredPlayerNameException, NotEnoughPlayerSlotsException {
		try {
			Message message = MessageFactory.createMessage(MessageCode.REGISTER_PLAYER, pName);
			message.send(mTavoloOutputStream);
			Message reply = MessageFactory.createMessage(mTavoloInputStream);
			switch (reply.getCode()) {
			case ACK:
				return;
			case ALREADY_REGISTERED_PLAYER_NAME_EXCEPTION:
				throw new AlreadyRegisteredPlayerNameException(pName);
			case NOT_ENOUGH_PLAYER_SLOTS_EXCEPTION:
				throw new NotEnoughPlayerSlotsException();
			default:
				Log.w("Why am i here? Should never be reached. Reply code :" + reply.getCode());
				break;
			}
		} catch (IOException e) {
			Log.e(e);	
		}
	}

	@Override
	public void startMatch() {
		try {
			Message message = MessageFactory.createMessage(MessageCode.START_GAME, GENERAL_CLIENT_PLAYER);
			message.send(mTavoloOutputStream);
			Message reply = MessageFactory.createMessage(mTavoloInputStream);
			if(reply.getCode() != MessageCode.ACK){
				Log.e("Something when wrong... :( ");
			}
		} catch (IOException e) {
			Log.e(e);	
		}
	}

	@Override
	public void registerBet(String pPlayerName, Horse pHorse, int pAmount,
			KindOfBet pKindOfBet) throws AlreadyPresentBetException {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeObject(pHorse, SingleIViewerServer.HORSE_PARCELER);
			parcel.writeInt(pAmount);
			parcel.writeObject(pKindOfBet, SingleIViewerServer.KIND_OF_BET_PARCELER);
			Message betMessage = MessageFactory.createMessage(MessageCode.EXPECTED_BET, pPlayerName, parcel);
			betMessage.send(mTavoloOutputStream);
			Message reply = MessageFactory.createMessage(mTavoloInputStream);
			switch (reply.getCode()) {
			case PLACED_BET:
				IParcel replyParcel = ParcelFactory.getParcel(reply.getContent());
				onPlacedBet(replyParcel);
				return;
			case ALREADY_PRESENT_BET:
				throw new AlreadyPresentBetException(pPlayerName, pHorse, pKindOfBet);
			default:
				Log.w("Why am i here? Should never be reached. Reply code :" + reply.getCode());
				break;
			}
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void assignCardToHorse(String pPlayerName, String pCardName,	Horse pHorse) throws PlayerDoNotOwnSpecifiedCardException {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeUTF(pCardName);
			parcel.writeObject(pHorse, SingleIViewerServer.HORSE_PARCELER);
			Message betMessage = MessageFactory.createMessage(MessageCode.EXPECTED_PLACED_CARD, pPlayerName, parcel);
			betMessage.send(mTavoloOutputStream);
			Message reply = MessageFactory.createMessage(mTavoloInputStream);
			switch (reply.getCode()) {
			case ACTION_CARD_ASSIGNED_TO_HORSE:
				IParcel replyParcel = ParcelFactory.getParcel(reply.getContent());
				onActionCardAssignedToHorse(replyParcel);
				return;
			case PLAYER_DONT_OWN_CARD:
				throw new PlayerDoNotOwnSpecifiedCardException(pPlayerName, pCardName);
			default:
				Log.w("Why am i here? Should never be reached. Reply code :" + reply.getCode());
				break;
			}
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void registerListener(IViewer pListener) {
		mListeners.add(pListener);
	}

	@Override
	public boolean removeListener(IViewer pListener) {
		return mListeners.remove(pListener);
	}

	private void waitForUpdate() throws IOException {
		Message message = MessageFactory.createMessage(mTavoloInputStream);
		byte[] messageContent = message.getContent();
		IParcel parcel = messageContent != null ? ParcelFactory.getParcel(messageContent) : null;
		switch (message.getCode()) {
		case START_PLAYER_REGISTRATION: {
			for(IViewer listener : mListeners) {
				listener.startPlayerRegistration();
			}
			break;
		}
		case ACTION_CARD_ASSIGNED_TO_HORSE: {
			onActionCardAssignedToHorse(parcel);
			break;
		}
		case ACTION_CARD_GIVEN_TO_PLAYER: {
			String playerName = parcel.readUTF();
			String cardName = parcel.readUTF();
			for(IViewer listener : mListeners) {
				listener.onActionCardGivenToPlayer(playerName, cardName);
			}
			break;
		}
		case ACTION_CARDS_REVEALED: {
			Horse horse = parcel.readObject(SingleIViewerServer.HORSE_PARCELER);
			List<String> assignedCards = parcel.readObject(ListParceler.getListParceller(ParcelUtils.STRING_PARCELER));
			List<String> removedCards = parcel.readObject(ListParceler.getListParceller(ParcelUtils.STRING_PARCELER));
			for(IViewer listener : mListeners) {
				listener.onActionCardRevealed(horse, assignedCards, removedCards);
			}
			break;
		}
		case END_OF_GAME: {
			String playerName = parcel.readUTF();
			for(IViewer listener : mListeners) {
				listener.onEndOfGame(playerName);
			}
			stopListening();
			break;
		}
		case EXPECTED_BET: {
			String playerName = parcel.readUTF();
			int minBet = parcel.readInt();
			int maxBet = parcel.readInt();
			Map<Horse, Integer> horseToQuotationMap = parcel.readObject(MapParceler.getMapParceller(SingleIViewerServer.HORSE_PARCELER, ParcelUtils.INTEGER_PARCELER));
			boolean isMandatory = parcel.readBoolean();
			for(IViewer listener : mListeners) {
				boolean placedBet = listener.onExpectBet(playerName, minBet, maxBet, horseToQuotationMap, isMandatory);
				if(!placedBet) {
					MessageFactory.createMessage(MessageCode.CHOOSE_NOT_TO_BET, playerName).send(mTavoloOutputStream);
				}
			}
			break;
		}
		case EXPECTED_PLACED_CARD: {
			String playerName = parcel.readUTF();
			List<String> availableCards = parcel.readObject(ListParceler.getListParceller(ParcelUtils.STRING_PARCELER));
			for(IViewer listener : mListeners) {
				listener.onExpectPlacedCard(playerName, availableCards);
			}
			break;
		}
		case FULL_QUOTATION_CHANGE: {
			Map<Stable, Integer> horseToQuotationMap = parcel.readObject(MapParceler.getMapParceller(SingleIViewerServer.STABLE_PARCELER, ParcelUtils.INTEGER_PARCELER));
			for(IViewer listener : mListeners) {
				listener.onFullQuotationUpdate(horseToQuotationMap);
			}
			break;
		}
		case GAME_STARTED: {
			Map<String, String> playerNamePlayerCardMap = parcel.readObject(MapParceler.getMapParceller(ParcelUtils.STRING_PARCELER, ParcelUtils.STRING_PARCELER));
			int totalNumberOfTurns = parcel.readInt();
			for(IViewer listener : mListeners) {
				listener.onGameStarted(playerNamePlayerCardMap, totalNumberOfTurns);
			}
			break;
		}
		case HORSE_ARRIVED: {
			Horse horse = parcel.readObject(SingleIViewerServer.HORSE_PARCELER);
			int horsePosition = parcel.readInt();
			for(IViewer listener : mListeners) {
				listener.onHorseArrived(horse, horsePosition);
			}
			break;
		}
		case HORSE_POSITION_CHANGE: {
			Map<Horse, Integer> horseToPositionMap = parcel.readObject(MapParceler.getMapParceller(SingleIViewerServer.HORSE_PARCELER, ParcelUtils.INTEGER_PARCELER));
			for(IViewer listener : mListeners) {
				listener.onHorsePositionsChanged(horseToPositionMap);
			}
			break;
		}
		case MOVEMENT_CARD_REVEALED: {
			int[] movementCardValues = parcel.readObject(ParcelUtils.INT_ARRAY_PARCELER);
			for(IViewer listener : mListeners) {
				listener.onMovementCardRevealed(movementCardValues);
			}
			break;
		}
		case PLACED_BET: {
			onPlacedBet(parcel);
			break;
		}
		case PLAYER_ELIMINATED: {
			String playerName = parcel.readUTF();
			for(IViewer listener : mListeners) {
				listener.onPlayerEliminated(playerName);
			}
			break;
		}
		case RACE_FINISHED: {
			Horse[] arrivalOrderedHorses = parcel.readArray(Horse.class, SingleIViewerServer.HORSE_PARCELER);
			for(IViewer listener : mListeners) {
				listener.onRaceFinished(arrivalOrderedHorses);
			}
			break;
		}
		case RACE_STARTED: {
			for(IViewer listener : mListeners) {
				listener.onRaceStarted();
			}
			break;
		}
		case REMOVED_PV_INSTEAD_OF_BET: {
			String playerName = parcel.readUTF();
			for(IViewer listener : mListeners) {
				listener.onRemovedPVInsteadOfBet(playerName);
			}
			break;
		}
		case SINGLE_QUOTATION_CHANGE: {
			Stable stable = parcel.readObject(SingleIViewerServer.STABLE_PARCELER);
			int oldQuotation = parcel.readInt();
			int newQuotation = parcel.readInt();
			for(IViewer listener : mListeners) {
				listener.onSingleQuotationChanged(stable, oldQuotation, newQuotation);
			}
			break;
		}
		case SPRINT: {
			Horse horse = parcel.readObject(SingleIViewerServer.HORSE_PARCELER);
			int sprintNumberInThisTurn = parcel.readInt();
			for(IViewer listener : mListeners) {
				listener.onSprint(horse, sprintNumberInThisTurn);
			}
			break;
		}
		case STABLE_PAID: {
			Stable stable = parcel.readObject(SingleIViewerServer.STABLE_PARCELER);
			int amount = parcel.readInt();
			for(IViewer listener : mListeners) {
				listener.onStablePaid(stable, amount);
			}
			break;
		}
		case TURN_FINISHED: {
			for(IViewer listener : mListeners) {
				listener.onTurnFinished();
			}
			break;
		}
		case WON_BET: {
			String playerName = parcel.readUTF();
			int amount = parcel.readInt();
			int PVs = parcel.readInt();
			KindOfBet kindOfBet = parcel.readObject(SingleIViewerServer.KIND_OF_BET_PARCELER);
			for(IViewer listener : mListeners) {
				listener.onWonBet(playerName, amount, PVs, kindOfBet);
			}
			break;
		}
		case REGISTER_PLAYER:
		case PLAYER_DONT_OWN_CARD:
		case NOT_ENOUGH_PLAYER_SLOTS_EXCEPTION:
		case ALREADY_PRESENT_BET:
		case ALREADY_REGISTERED_PLAYER_NAME_EXCEPTION:
		case ACK:
		case START_GAME:
		case CHOOSE_NOT_TO_BET:
			Log.w("Should never be reached.");
			break;
		}
	}

	private void onPlacedBet(IParcel parcel) throws IOException {
		String playerName = parcel.readUTF();
		Horse horse = parcel.readObject(SingleIViewerServer.HORSE_PARCELER);
		int value = parcel.readInt();
		KindOfBet kindOfBet = parcel.readObject(SingleIViewerServer.KIND_OF_BET_PARCELER);
		for(IViewer listener : mListeners) {
			listener.onPlacedBet(playerName, horse, value, kindOfBet);
		}
	}

	private void onActionCardAssignedToHorse(IParcel parcel) throws IOException {
		String playerName = parcel.readUTF();
		String cardName = parcel.readUTF();
		Horse horse = parcel.readObject(SingleIViewerServer.HORSE_PARCELER);
		for(IViewer listener : mListeners) {
			listener.onActionCardAssignedToHorse(playerName, cardName, horse);
		}
	}
}
