package it.polimi.provaFinale2013.communication.marshalling.parcel;

import java.io.IOException;

/**
 * This class has the goal of writing and reading specific objects to an {@link IParcel}.
 * Since the read object could be simpler or just a data rappresentation of the original object, it should be able
 * to generate a different object from the one wrote with this same {@link IParceler}
 * @author Francesco
 *
 * @param <Original> The objects that will be used to write to the IParcel
 * @param <Reppresentation> The object that will be returned when this IParceler reads the objects it wrote to the IParcel 
 */
public interface IParceler<Original, Reppresentation> {
	/**
	 * Writes an object to the parcel
	 * @param contenuto The object to write
	 * @param pacchetto The IParcel to write the object to
	 * @throws IOException If some error happens while writing to the parcel
	 */
	void writeToParcel(Original contenuto, IParcel pacchetto) throws IOException;
	/**
	 * Reads an object from the parcel
	 * @param pacchetto The parcel from which read the object
	 * @return The read object
	 * @throws IOException If some error happens while reading to the parcel
	 */
	Reppresentation createFromParcel(IParcel pacchetto) throws IOException;
	/**
	 * @return An unique String rappresenting the type serialized by this IParceler
	 */
	String getUniqueClassID();
}
