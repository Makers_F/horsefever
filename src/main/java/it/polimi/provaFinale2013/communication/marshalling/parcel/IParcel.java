package it.polimi.provaFinale2013.communication.marshalling.parcel;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * This class rappresent a parcel.
 * The parcel can be used to put objects or primitive data into it.
 * After the parcel is created you can only write objects into it, once {@link IParcel#apriParcel()} is called you can only read from it.
 * @author Francesco
 *
 */
public interface IParcel extends DataOutput, DataInput {

	/**
	 * Opens the parcel to allow reading from it
	 * @throws IllegalStateException If the parcel has already been opened
	 */
	void apriParcel() throws IllegalStateException;
	/**
	 * @return The size in bytes of the current parcel
	 */
	int size();
	/**
	 * 
	 * @return The whole content of the parcel
	 * @throws IllegalStateException If the parcel has not been opened yet
	 */
	byte[] contenutoPacchetto() throws IllegalStateException;
	/**
	 * Writes an object into the parcel
	 * @param object The object to write
	 * @param factory The factory to use to write the object to the parcel
	 * @throws IOException If some error happens while writing to the parcel
	 */
	<T> void writeObject(T object, IParceler<T,?> factory) throws IOException;
	/**
	 * Reads a previously wrote object from the parcel.
	 * NOTE: this method is type safe, if you try to read the wrong object, a {@link ClassCastException} will be thrown 
	 * @param factory The factory able to read the object wrote to the parcel
	 * @return The read object 
	 * @throws IOException If some error happens while reading to the parcel
	 */
	<T> T readObject(IParceler<?,T> factory) throws IOException;
	/**
	 * Writes an array of object into the parcel
	 * @param objects The array to write
	 * @param factory The factory able to write the object contained in the array to the parcel
	 * @throws IOException If some error happens while writing to the parcel
	 */
	<T> void writeArray(T[] objects, IParceler<T,?> factory) throws IOException;
	/**
	 * Reads a previously wrote array from the parcel
	 * @param type The type of objects contained in the array
	 * @param factory The factory able to read the objects contained in the array
	 * @return The array of objects
	 * @throws IOException If some error happens while writing to the parcel
	 */
	<T> T[] readArray(Class<T> type, IParceler<?,T> factory) throws IOException;
}
