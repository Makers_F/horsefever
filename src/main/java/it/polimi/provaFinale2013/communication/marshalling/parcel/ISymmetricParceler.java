package it.polimi.provaFinale2013.communication.marshalling.parcel;

/**
 * This interface is a special {@link IParceler} which writes and read the same class of objects, thus erasing the difference between original object and rappresentation
 * @author Francesco
 *
 * @param <T> The class of objetcs it read and writes
 */
public interface ISymmetricParceler<T> extends IParceler<T, T> {

}