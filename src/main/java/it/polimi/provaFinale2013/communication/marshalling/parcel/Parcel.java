package it.polimi.provaFinale2013.communication.marshalling.parcel;

import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.utils.parcel.ParcelUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.List;


/**
 * 
 * @author Francesco
 *
 * Questa classe funziona proprio come un pacco postale. Finch? il creatore ce l'ha pu? continuare ad aggiungere
 * cose al suo interno, ma una volta spedito, e poi quindi aperto, si pu? solo togliere roba, senza pi? aggiungerla.
 * 
 * Appena creato, si pu? scrivere dentro al parcel tutti i dati che si vuole.
 * Una volta aperto, si pu? solo leggere.
 * Se si instanzia passando un array di byte, allora si pu? solo leggere.
 */
public class Parcel implements IParcel {

	private ByteArrayInputStream byteArrayInputStream;
	private ByteArrayOutputStream byteArrayOutputStream;
	private DataOutputStream dataOutputStream;
	private DataInputStream dataInputStream;
	private PARCEL_STATE stato;
	private int totalLength = -1;
	private List<Integer> recursionCheck;
	
	Parcel() {
		byteArrayOutputStream = new ByteArrayOutputStream();
		dataOutputStream = new DataOutputStream(byteArrayOutputStream);
		stato = PARCEL_STATE.APERTO_IN_SCRITTURA;
		recursionCheck = new LinkedList<Integer>();
	}

	Parcel(byte[] originalBytes) {
		apriParcel(originalBytes);
	}

	@Override
	public void apriParcel() {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new IllegalStateException("The parcel is already open.");
		}
		byte[] bytes = byteArrayOutputStream.toByteArray();
		apriParcel(bytes);
	}

	private void apriParcel(byte[] bytes) {
		stato = PARCEL_STATE.APERTO_IN_LETTURA;
		byte[] clonedBytes = bytes.clone();
		totalLength = clonedBytes.length;
		byteArrayInputStream = new ByteArrayInputStream(clonedBytes);
		dataInputStream = new DataInputStream(byteArrayInputStream);
	}

	/**
	 *
	 * @return The number of bytes that this parcel uses
	 */
	@Override
	public int size() {
		return stato == PARCEL_STATE.APERTO_IN_LETTURA ? totalLength : byteArrayOutputStream.size();
	}

	@Override
	public byte[] contenutoPacchetto() {
		if( stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			return byteArrayOutputStream.toByteArray();
		} else {
			throw new IllegalStateException("You can't get the content of an opened parcel. Use the appropriate methods to read from it.");
		}
	}

	@Override
	public <T> void writeObject(T object, IParceler<T,?> factory) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		if(object == null) {
			dataOutputStream.writeUTF(ParcelUtils.getNullCode());
		} else {
			Integer ID = Integer.valueOf(System.identityHashCode(object));
			boolean firstTimer = !recursionCheck.contains(ID);
			if(firstTimer) {
				recursionCheck.add(ID);
				dataOutputStream.writeUTF(factory.getUniqueClassID());
				factory.writeToParcel(object, this);
				recursionCheck.remove(ID);
			} else {
				dataOutputStream.writeUTF(ParcelUtils.getNullCode());
				Log.w("Recursive dependency found when adding the object: " + object.toString() +  ". Wrote null to the parcel instead.");
			}
		}
	}

	@Override
	public <T> T readObject(IParceler<?,T> factory) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		String classCode = dataInputStream.readUTF();
		if(ParcelUtils.isNullCode(classCode)) {
			return null;
		}
		if(classCode.equals(factory.getUniqueClassID())) {
			return factory.createFromParcel(this);
		} else {
			throw new ClassCastException("The parcel do not contains the type of object you tried to read. Contained type ID: " + classCode);
		}
	}

	@Override
	public <T> void writeArray(T[] objects, IParceler<T,?> factory) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		String typeCode;
		if(objects == null) {
			typeCode = ParcelUtils.makeArrayCode(ParcelUtils.getNullCode());
			dataOutputStream.writeUTF(typeCode);
		} else {
			typeCode = ParcelUtils.makeArrayCode(factory.getUniqueClassID());
			dataOutputStream.writeUTF(typeCode);
			dataOutputStream.writeInt(objects.length);
			for(T obj : objects) {
				if (obj == null) {
					dataOutputStream.writeBoolean(false);
				} else {
					dataOutputStream.writeBoolean(true);
					factory.writeToParcel(obj, this);
				}
			}
		}
	}

	@Override
	public <T> T[] readArray(Class<T> type, IParceler<?,T> factory) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		String typeCode = dataInputStream.readUTF();
		boolean isArray = ParcelUtils.isArrayCode(typeCode);
		String classCode = ParcelUtils.makeSureNotArrayCode(typeCode);

		if (!isArray) {
			throw new ClassCastException("You tried to read an array, but the parcel do not contains an array.");
		}
		if (ParcelUtils.isNullCode(classCode)) {
			return null;
		}
		if(classCode.equals(factory.getUniqueClassID())) {
			int arrayLength = dataInputStream.readInt();
			@SuppressWarnings("unchecked")
			T[] array = (T[]) Array.newInstance(type, arrayLength);
			for(int i = 0; i < arrayLength; i++) {
				boolean isNull = !dataInputStream.readBoolean();
				if(isNull) {
					array[i] = null;
				} else {
					array[i] = factory.createFromParcel(this);
				}
			}
			return array;
		} else {
			throw new ClassCastException("The parcel do not contains the type of object array you tried to read. Contained array type ID: " + classCode);
		}
	}

	@Override
	public void write(int b) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.write(b);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.write(b, off, len);
	}

	@Override
	public void writeBoolean(boolean v) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.writeBoolean(v);
	}

	@Override
	public void writeByte(int v) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.writeByte(v);
	}

	@Override
	public void writeBytes(String s) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.writeBytes(s);
	}

	@Override
	public void writeChar(int v) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.writeChar(v);
	}

	@Override
	public void writeChars(String s) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.writeChars(s);
	}

	@Override
	public void writeDouble(double v) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.writeDouble(v);
	}

	@Override
	public void writeFloat(float v) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.writeFloat(v);
	}

	@Override
	public void writeInt(int v) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.writeInt(v);
	}

	@Override
	public void writeLong(long v) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.writeLong(v);
	}

	@Override
	public void writeShort(int v) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		dataOutputStream.writeShort(v);
	}

	@Override
	public void writeUTF(String s) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_LETTURA) {
			throw new WrongParcelWriteAccess();
		}
		if(s != null) {
			dataOutputStream.writeBoolean(true);
			dataOutputStream.writeUTF(s);
		} else {
			dataOutputStream.writeBoolean(false);
		}
	}

	@Override
	public boolean readBoolean() throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		return dataInputStream.readBoolean();
	}

	@Override
	public byte readByte() throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		return dataInputStream.readByte();
	}

	@Override
	public char readChar() throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		return dataInputStream.readChar();
	}

	@Override
	public double readDouble() throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		return dataInputStream.readDouble();
	}

	@Override
	public float readFloat() throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		return dataInputStream.readFloat();
	}

	@Override
	public void readFully(byte[] b) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		dataInputStream.readFully(b);
	}

	@Override
	public void readFully(byte[] b, int off, int len) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		dataInputStream.readFully(b, off, len);
	}

	@Override
	public int readInt() throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		return dataInputStream.readInt();
	}

	@Override
	public String readLine() throws IOException {
		throw new IllegalAccessError("The method is deprecated");
	}

	@Override
	public long readLong() throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		return dataInputStream.readLong();
	}

	@Override
	public short readShort() throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		return dataInputStream.readShort();
	}

	@Override
	public String readUTF() throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		boolean isValid = dataInputStream.readBoolean();
		if (isValid) {
			return dataInputStream.readUTF();
		} else {
			return null;
		}
	}

	@Override
	public int readUnsignedByte() throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		return dataInputStream.readUnsignedByte();
	}

	@Override
	public int readUnsignedShort() throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		return dataInputStream.readUnsignedShort();
	}

	@Override
	public int skipBytes(int n) throws IOException {
		if(stato == PARCEL_STATE.APERTO_IN_SCRITTURA) {
			throw new WrongParcelReadAccess();
		}
		return dataInputStream.skipBytes(n);
	}

	private enum PARCEL_STATE {
		APERTO_IN_SCRITTURA(),
		APERTO_IN_LETTURA();
	}

	public static class WrongParcelReadAccess extends IOException {
		private static final long serialVersionUID = 9125235538376010752L;

		public WrongParcelReadAccess() {
			super("You can not read from a Parcel that has not been opened yet.");
		}
	}

	public static class WrongParcelWriteAccess extends IOException {
		private static final long serialVersionUID = -2264525443714777140L;

		public WrongParcelWriteAccess() {
			super("You can not write to a Parcel that has already been opened.");
		}
	}
}
