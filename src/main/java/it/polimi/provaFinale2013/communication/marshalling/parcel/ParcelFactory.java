package it.polimi.provaFinale2013.communication.marshalling.parcel;

public class ParcelFactory {

	public static IParcel getParcel() {
		return new Parcel();
	}

	public static IParcel getParcel(byte[] bytes) {
		return new Parcel(bytes);
	}
}
