package it.polimi.provaFinale2013.communication;

import it.polimi.provaFinale2013.communication.marshalling.parcel.IParcel;
import it.polimi.provaFinale2013.communication.marshalling.parcel.ISymmetricParceler;
import it.polimi.provaFinale2013.communication.marshalling.parcel.ParcelFactory;
import it.polimi.provaFinale2013.communication.message.Message;
import it.polimi.provaFinale2013.communication.message.MessageCode;
import it.polimi.provaFinale2013.communication.message.MessageFactory;
import it.polimi.provaFinale2013.horsefever.cavalli.Horse;
import it.polimi.provaFinale2013.horsefever.cavalli.Stable;
import it.polimi.provaFinale2013.horsefever.scommesse.KindOfBet;
import it.polimi.provaFinale2013.horsefever.scommesse.Bookmaker.AlreadyPresentBetException;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.AlreadyRegisteredPlayerNameException;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.NotEnoughPlayerSlotsException;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco.PlayerDoNotOwnSpecifiedCardException;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.utils.parcel.EnumParceler;
import it.polimi.provaFinale2013.utils.parcel.ListParceler;
import it.polimi.provaFinale2013.utils.parcel.MapParceler;
import it.polimi.provaFinale2013.utils.parcel.ParcelUtils;
import it.polimi.provaFinale2013.view.IViewer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class acts as a listener for the game, but all the notifications are sent over internet to {@link ClientSideITavolo}.
 * @author Francesco
 *
 */
public class SingleIViewerServer extends Thread implements IViewer {

	static final Message ACK_MESSAGE = MessageFactory.createMessage(MessageCode.ACK, SingleIViewerServer.GOD_PLAYER_NAME);
	static final ISymmetricParceler<Horse> HORSE_PARCELER = new EnumParceler<Horse>(Horse.class);
	static final ISymmetricParceler<Stable> STABLE_PARCELER = new EnumParceler<Stable>(Stable.class);
	static final ISymmetricParceler<KindOfBet> KIND_OF_BET_PARCELER =new EnumParceler<KindOfBet>(KindOfBet.class);

	public static final String GOD_PLAYER_NAME = "'m God and i know it";
	private final ITavoloDiGioco<IViewer> mTavoloDiGioco;
	private final Set<String> mRegisteredPlayers = new HashSet<String>();
	private final IncomingConnectionHandler mConntectionHandler;
	private final Socket mSocket;
	private final DataOutputStream mPlayerOutputStream;
	private final DataInputStream mPlayerInputStream;

	public SingleIViewerServer(ITavoloDiGioco<IViewer> pTavoloDiGioco, IncomingConnectionHandler pConntectionHandler, Socket pSocket) throws IOException {
		super("SingleServer");
		mTavoloDiGioco = pTavoloDiGioco;
		mConntectionHandler = pConntectionHandler;
		mSocket = pSocket;
		mPlayerOutputStream = new DataOutputStream(pSocket.getOutputStream());
		mPlayerInputStream = new DataInputStream(pSocket.getInputStream());
		mTavoloDiGioco.registerListener(this);
	}

	/**
	 * This methods signals to the client that it should start to register the players 
	 */
	@Override
	public void run() {
		try {
			boolean close = false;
			Message startRegistration = MessageFactory.createMessage(MessageCode.START_PLAYER_REGISTRATION, SingleIViewerServer.GOD_PLAYER_NAME);
			startRegistration.send(mPlayerOutputStream);
			while(!close) {
				try {
					Message message = MessageFactory.createMessage(mPlayerInputStream);
					switch (message.getCode()) {
						case REGISTER_PLAYER:
							try {
								mTavoloDiGioco.registerPlayer(message.getPlayer());
								mConntectionHandler.registerDependencyOnSocket(mSocket);
								mRegisteredPlayers.add(message.getPlayer());
								ACK_MESSAGE.send(mPlayerOutputStream);
							} catch (AlreadyRegisteredPlayerNameException e) {
								Message exception = MessageFactory.createMessage(MessageCode.ALREADY_REGISTERED_PLAYER_NAME_EXCEPTION, SingleIViewerServer.GOD_PLAYER_NAME);
								exception.send(mPlayerOutputStream);
							} catch (NotEnoughPlayerSlotsException e) {
								Message exception = MessageFactory.createMessage(MessageCode.NOT_ENOUGH_PLAYER_SLOTS_EXCEPTION, SingleIViewerServer.GOD_PLAYER_NAME);
								exception.send(mPlayerOutputStream);
							}
							break;
						case START_GAME:
							mConntectionHandler.startGame(mSocket);
							SingleIViewerServer.ACK_MESSAGE.send(mPlayerOutputStream);
							close = true;
							break;
						default:
							Log.w("Why am i here??? What other messages could be sent?");
							break;
					}
				} catch (IOException e) {
					mPlayerInputStream.close();
					mPlayerOutputStream.close();
					mSocket.close();
					Log.e(e);
				}
			}
			// DO NOT CLOSE THE SOCKET!! IT WILL BE USED LATER 
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onSingleQuotationChanged(Stable pStable, int pOldQuotation,
			int pNewQuotation) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeObject(pStable, STABLE_PARCELER);
			parcel.writeInt(pOldQuotation);
			parcel.writeInt(pNewQuotation);
			Message notify = MessageFactory.createMessage(MessageCode.SINGLE_QUOTATION_CHANGE, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onFullQuotationUpdate(Map<Stable, Integer> pHorseToQuotationMap) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeObject(pHorseToQuotationMap, MapParceler.getMapParceller(STABLE_PARCELER, ParcelUtils.INTEGER_PARCELER));
			Message notify = MessageFactory.createMessage(MessageCode.FULL_QUOTATION_CHANGE, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onRaceStarted() {
		try {
			Message notify = MessageFactory.createMessage(MessageCode.RACE_STARTED, GOD_PLAYER_NAME);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onMovementCardRevealed(int[] pMovementCardValues) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeObject(pMovementCardValues, ParcelUtils.INT_ARRAY_PARCELER);
			Message notify = MessageFactory.createMessage(MessageCode.MOVEMENT_CARD_REVEALED, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onSprint(Horse pHorse, int pSprintNumberInThisTurn) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeObject(pHorse, HORSE_PARCELER);
			parcel.writeInt(pSprintNumberInThisTurn);
			Message notify = MessageFactory.createMessage(MessageCode.SPRINT, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onHorsePositionsChanged(Map<Horse, Integer> pHorseToPositionMap) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeObject(pHorseToPositionMap, MapParceler.getMapParceller(HORSE_PARCELER, ParcelUtils.INTEGER_PARCELER));
			Message notify = MessageFactory.createMessage(MessageCode.HORSE_POSITION_CHANGE, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onHorseArrived(Horse pHorse, int pHorsePosition) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeObject(pHorse, HORSE_PARCELER);
			parcel.writeInt(pHorsePosition);
			Message notify = MessageFactory.createMessage(MessageCode.HORSE_ARRIVED, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onRaceFinished(Horse[] pArrivalOrderedHorses) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeArray(pArrivalOrderedHorses, HORSE_PARCELER);
			Message notify = MessageFactory.createMessage(MessageCode.RACE_FINISHED, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onPlacedBet(String pPlayerName, Horse pHorse, int pValue,
			KindOfBet pKindOfBet) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeUTF(pPlayerName);
			parcel.writeObject(pHorse, HORSE_PARCELER);
			parcel.writeInt(pValue);
			parcel.writeObject(pKindOfBet, KIND_OF_BET_PARCELER);
			Message notify = MessageFactory.createMessage(MessageCode.PLACED_BET, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onWonBet(String pPlayerName, int pAmount, int pPVs,
			KindOfBet pKindOfBet) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeUTF(pPlayerName);
			parcel.writeInt(pAmount);
			parcel.writeInt(pPVs);
			parcel.writeObject(pKindOfBet, KIND_OF_BET_PARCELER);
			Message notify = MessageFactory.createMessage(MessageCode.WON_BET, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onStablePaid(Stable pStable, int pAmount) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeObject(pStable, STABLE_PARCELER);
			parcel.writeInt(pAmount);
			Message notify = MessageFactory.createMessage(MessageCode.STABLE_PAID, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onActionCardGivenToPlayer(String pPlayerName, String pCardName) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeUTF(pPlayerName);
			parcel.writeUTF(pCardName);
			Message notify = MessageFactory.createMessage(MessageCode.ACTION_CARD_GIVEN_TO_PLAYER, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onActionCardAssignedToHorse(String pPlayerName, String pCardName, Horse pHorse) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeUTF(pPlayerName);
			parcel.writeUTF(pCardName);
			parcel.writeObject(pHorse, HORSE_PARCELER);
			Message notify = MessageFactory.createMessage(MessageCode.ACTION_CARD_ASSIGNED_TO_HORSE, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public boolean onExpectBet(String pPlayerName, int pMinBet, int pMaxBet,
			Map<Horse, Integer> pHorseToQuotationMap, boolean isMandatory) {
		if(mRegisteredPlayers.contains(pPlayerName)) {
			try {
				// notifica che deve scommettere
				IParcel parcel = ParcelFactory.getParcel();
				parcel.writeUTF(pPlayerName);
				parcel.writeInt(pMinBet);
				parcel.writeInt(pMaxBet);
				parcel.writeObject(pHorseToQuotationMap, MapParceler.getMapParceller(HORSE_PARCELER, ParcelUtils.INTEGER_PARCELER));
				parcel.writeBoolean(isMandatory);
				Message notify = MessageFactory.createMessage(MessageCode.EXPECTED_BET, GOD_PLAYER_NAME, parcel);
				notify.send(mPlayerOutputStream);
				//leggi la risposta
				return waitForBetReply(pPlayerName);
			} catch (IOException e) {
				Log.e(e);
				return false;
			}
		} else {
			return false;
		}
	}

	private boolean waitForBetReply(String pPlayerName) throws IOException {
		Message reply = MessageFactory.createMessage(mPlayerInputStream);
		if(!reply.getPlayer().equals(pPlayerName)){
			Log.e("The anwser is from the wrong player: " + reply.getPlayer());
		}
		switch (reply.getCode()) {
		case CHOOSE_NOT_TO_BET: {
			return false;
		}
		case EXPECTED_BET : {
			try {
				IParcel replyParcel = ParcelFactory.getParcel(reply.getContent());
				mTavoloDiGioco.registerBet(pPlayerName, replyParcel.readObject(HORSE_PARCELER),
						replyParcel.readInt(), replyParcel.readObject(KIND_OF_BET_PARCELER));
				// se tutto � andato bene, gli arriver� l'update PLACED_BET
				return true;
			} catch (AlreadyPresentBetException e) {
				// se avviene un'eccezione, segnala l'errore e riinizia la procedura per la scommessa
				Message alreadyPresent = MessageFactory.createMessage(MessageCode.ALREADY_PRESENT_BET, GOD_PLAYER_NAME);
				alreadyPresent.send(mPlayerOutputStream);
				return waitForBetReply(pPlayerName);
			}
		}
		default:
			Log.e("This is not the expected reply");
			return false;
		}
	}

	@Override
	public void onExpectPlacedCard(String pPlayerName, List<String> pAvailableCards) {
		if(mRegisteredPlayers.contains(pPlayerName)) {
			try {
				IParcel parcel = ParcelFactory.getParcel();
				parcel.writeUTF(pPlayerName);
				parcel.writeObject(pAvailableCards, ListParceler.getListParceller(ParcelUtils.STRING_PARCELER));
				Message notify = MessageFactory.createMessage(MessageCode.EXPECTED_PLACED_CARD, GOD_PLAYER_NAME, parcel);
				notify.send(mPlayerOutputStream);
				// leggi la rispsosta
				waitForPlacedCardReply(pPlayerName);
			} catch (IOException e) {
				Log.e(e);
			}
		}
	}

	private void waitForPlacedCardReply(String pPlayerName) throws IOException {
		try {
			Message reply = MessageFactory.createMessage(mPlayerInputStream);
			if(!reply.getPlayer().equals(pPlayerName)){
				Log.e("The anwser is from the wrong player");
			}
			if(reply.getCode() != MessageCode.EXPECTED_PLACED_CARD){
				Log.e("This is not the expected reply");
			}
			IParcel replyParcel = ParcelFactory.getParcel(reply.getContent());
			mTavoloDiGioco.assignCardToHorse(pPlayerName, replyParcel.readUTF(), replyParcel.readObject(HORSE_PARCELER));
			// se tutto � andato bene, gli arriver� l'update ASSIGNED_CARD_TO_HORSE
		}  catch (PlayerDoNotOwnSpecifiedCardException e) {
			// se avviene un'eccezione, segnala l'errore e riinizia la procedura per la scommessa
			Message doNotown = MessageFactory.createMessage(MessageCode.PLAYER_DONT_OWN_CARD, GOD_PLAYER_NAME);
			doNotown.send(mPlayerOutputStream);
			waitForPlacedCardReply(pPlayerName);
		}
	}

	@Override
	public void onTurnFinished() {
		try {
			Message notify = MessageFactory.createMessage(MessageCode.TURN_FINISHED, GOD_PLAYER_NAME);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onRemovedPVInsteadOfBet(String pPlayerName) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeUTF(pPlayerName);
			Message notify = MessageFactory.createMessage(MessageCode.REMOVED_PV_INSTEAD_OF_BET, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onPlayerEliminated(String pPlayerName) {
		try {
			mRegisteredPlayers.remove(pPlayerName);
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeUTF(pPlayerName);
			Message notify = MessageFactory.createMessage(MessageCode.PLAYER_ELIMINATED, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onGameStarted(Map<String, String> pPlayerNamePlayerCardMap, int pTotalNumberOfTurns) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeObject(pPlayerNamePlayerCardMap, MapParceler.getMapParceller(ParcelUtils.STRING_PARCELER, ParcelUtils.STRING_PARCELER));
			parcel.writeInt(pTotalNumberOfTurns);
			Message notify = MessageFactory.createMessage(MessageCode.GAME_STARTED, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void onEndOfGame(String pPlayerName) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeUTF(pPlayerName);
			Message notify = MessageFactory.createMessage(MessageCode.END_OF_GAME, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
		try {
			mSocket.close();
		} catch (IOException e) {
			Log.e(e);
		}
	}

	@Override
	public void startPlayerRegistration() {
		Log.w("This method should never be called since it is called in SingleClientHandler when the connection is entablished.");
	}

	@Override
	public void onActionCardRevealed(Horse pHorse,
			List<String> pActionCardsAssigned, List<String> pActionCardsRemoved) {
		try {
			IParcel parcel = ParcelFactory.getParcel();
			parcel.writeObject(pHorse, HORSE_PARCELER);
			parcel.writeObject(pActionCardsAssigned, ListParceler.getListParceller(ParcelUtils.STRING_PARCELER));
			parcel.writeObject(pActionCardsRemoved, ListParceler.getListParceller(ParcelUtils.STRING_PARCELER));
			Message notify = MessageFactory.createMessage(MessageCode.ACTION_CARDS_REVEALED, GOD_PLAYER_NAME, parcel);
			notify.send(mPlayerOutputStream);
		} catch (IOException e) {
			Log.e(e);
		}
	}
}
