package it.polimi.provaFinale2013;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.BindException;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

import it.polimi.provaFinale2013.communication.IncomingConnectionHandler;
import it.polimi.provaFinale2013.communication.ClientSideITavolo;
import it.polimi.provaFinale2013.graphic.GUI;
import it.polimi.provaFinale2013.graphic.UserInteraction;
import it.polimi.provaFinale2013.horsefever.tavolo.ITavoloDiGioco;
import it.polimi.provaFinale2013.horsefever.tavolo.TavoloDiGioco;
import it.polimi.provaFinale2013.utils.Log;
import it.polimi.provaFinale2013.utils.UserInteractionUtils;
import it.polimi.provaFinale2013.view.IViewer;
import it.polimi.provaFinale2013.view.Viewer;

public class Main {

	private static final int MINIMUM_ADDRESS_LENGHT = 8;
	private static final String NO_GUI_OPTION = "noGUI";

	public static void main(String[] args) {
		if(args.length >= 1 && args[0].contains("-h")) {
			printHelp();
			return;
		}
		if(args.length >= 1 && args[0].startsWith("-") && args[0].contains(NO_GUI_OPTION)) {
			try {
				CLIstartGame();
			} catch (IOException e) {
				Log.user("An error happened during the starting of the game.\n" + e.getMessage());
			}
		} else {
			GUIstartGame();
		}
	}

	public static void CLIstartGame() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		boolean wantToBeServer = UserInteractionUtils.readChoice(br, "Do you want to be the server? [y|n]", "y", "n", "This is not a valid answer", true);
		String address;
		int port = UserInteractionUtils.readInt(br, "Please insert the port of the connection in the range (1025 - 99999)", 1025, 99999, "This is not a valid answer");
		if(wantToBeServer) {
			ITavoloDiGioco<IViewer> tavolo = new TavoloDiGioco();
			IncomingConnectionHandler ich = null;
			try {
				ich = new IncomingConnectionHandler(tavolo, port);
			} catch (BindException e) {
				Log.user("Sorry, the port is already used by another instance of the game. Please use a different one");
				CLIstartGame();
				return;
			}
			ich.start();
			address = "localhost";
		} else {
			address = UserInteractionUtils.readLine(br, "Please insert the address of the server", "This is not a valid answer", false, true);
		}
		Thread.currentThread().setName("Client");
		connectClientCLI(br, address, port);
	}

	private static void connectClientCLI(BufferedReader pBufferedReader, String pAddress, int pPort) throws IOException {
		ClientSideITavolo client = new ClientSideITavolo(pAddress, pPort);
		IViewer viewver = new Viewer(client);
		client.registerListener(viewver);
		try {
			client.startListening();
		} catch (UnknownHostException e) {
			Log.user("Sorry, no server at the address \"" + pAddress + ":" + pPort + "\"");
			boolean retry = UserInteractionUtils.readChoice(pBufferedReader, "Do you want to try again? [y|n]", "y", "n", "Invalid answer", true);
			if(retry) {
				int port = UserInteractionUtils.readInt(pBufferedReader, "Please insert the port of the connection in the range (80 - 9999)", 80, 9999, "This is not a valid answer");
				String address = UserInteractionUtils.readLine(pBufferedReader, "Please insert the address of the server", "This is not a valid answer", false, true);
				connectClientCLI(pBufferedReader, address, port);
			} else {
				Log.user("See yoou next time!");
				System.exit(0);
			}
		} catch (IOException e1) {
			if(e1.getMessage().contains("Conncetion timed out")) {
				Log.user("The address you tried to connect tu does not exists.");
			} else {
				Log.user("Sorry, there was a connection problem." + e1.getMessage());
			}
			System.exit(0);
		}
	}

	private static void GUIstartGame() {
		String address;
		int port = 0;
		int respose = JOptionPane.showConfirmDialog(null, "Do you want to be the server", "Server Choice", JOptionPane.YES_NO_OPTION);
		if(respose == JOptionPane.YES_OPTION) {
			// get the port
			port = getGUIPort();
			// initialize the server
			ITavoloDiGioco<IViewer> tavolo = new TavoloDiGioco();
			IncomingConnectionHandler ich = null;
			try {
				ich = new IncomingConnectionHandler(tavolo, port);
			} catch (BindException e) {
				JOptionPane.showMessageDialog(null, "The port you used is already used by another instance of the game. Please change it.", "Server Error", JOptionPane.ERROR_MESSAGE);
				GUIstartGame();
				return;
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Sorry, there was an error instancing the server.", "Server Error", JOptionPane.ERROR_MESSAGE);
				GUIstartGame();
				return;
			}
			ich.start();
			address = "localhost";
		} else {
			port = getGUIPort();
			address = getGUIAddress();
		}
		Thread.currentThread().setName("Client");
		connectClientGUI(address, port);
	}

	private static void connectClientGUI(String pAddress, int pPort) {
		ClientSideITavolo client = new ClientSideITavolo(pAddress, pPort);
		IViewer viewer = new GUI(client);
		client.registerListener(viewer);
		try {
			client.startListening();
		} catch (UnknownHostException e) {
			JOptionPane.showMessageDialog(null, "Sorry, no server at the address \"" + pAddress + ":" + pPort + "\"");
			boolean retry = JOptionPane.showConfirmDialog(null, "Do you want to try again?", "Error", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION ? true : false;
			if(retry) {
				int port = getGUIPort();
				String address = getGUIAddress();
				connectClientGUI(address, port);
			} else {
				UserInteraction.generalMessage("See you next time!");
				System.exit(0);
			}
		} catch (IOException e1) {
			if(e1.getMessage().contains("Connection timed out")) {
				UserInteraction.generalMessage("The address you tried to connect to does not exists.");
			} else {
				UserInteraction.generalMessage("Sorry, there was a connection problem caused by:\n" + e1.getMessage());
			}
			System.exit(0);
		}
	}

	private static int getGUIPort() {
		while(true) {
			String portS = JOptionPane.showInputDialog(null, "Please write the port you want your server to listen to. Range [1025, 99999]", "Server Address", JOptionPane.DEFAULT_OPTION);
			if(portS != null && !portS.equals("")) {
				try {
					int port = Integer.parseInt(portS.trim());
					if(port < 1025 || port > 9999 ) {
						continue;
					} else {
						return port;
					}
				} catch (NumberFormatException e) {
					continue;
				}
			}
		}
	}

	private static String getGUIAddress() {
		while(true) {
			String input = JOptionPane.showInputDialog(null, "Please write the address of the server", "Server Address", JOptionPane.DEFAULT_OPTION);
			String address = input.trim();
			if(address != null && !address.equals("") && address.length() >= MINIMUM_ADDRESS_LENGHT) {
				return address;
			}
		}
	}

	private static void printHelp() {
		Log.user(" -h prints this message.");
		Log.user(" -" + NO_GUI_OPTION + " with this option the game is run on CLI.");
	}
}